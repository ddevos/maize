/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Blad model.
 */

#include "Blad.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"

using namespace std;
using namespace SimPT_Sim::Util;
using boost::property_tree::ptree;

namespace SimPT_Blad {
namespace CellChemistry {

Blad::Blad(const CoreData& cd)
{
	Initialize(cd);
}

void Blad::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters->get_child("auxin_transport");

        const auto& q     = m_cd.m_parameters->get_child("blad");
        m_aux1prod        = p.get<double>("aux1prod");
        m_aux_breakdown	  = p.get<double>("aux_breakdown");
        m_grid_size       = q.get<int>("grid_size");
        m_M_degradation   = q.get<double>("M_degradation");
        m_M_duration      = q.get<double>("M_duration");
        m_M_production    = q.get<double>("M_production");
}

void Blad::operator()(Cell* cell, double* dchem)
{

	double time_now 	= m_cd.m_time_data->m_sim_time;
	const double chem1      = cell->GetChemical(1);
	//const double a_area	= cell->GetArea();

	bool produces_M = false;
	if (m_grid_size == 32) {
		if(cell->GetIndex() > 15 && cell->GetIndex() < 32) {
			produces_M = true;
		}
	} else if (m_grid_size == 128) {
		if ((cell->GetIndex() > 15 && cell->GetIndex() < 32)
			|| (cell->GetIndex() > 52 && cell->GetIndex() < 61)
			|| (cell->GetIndex() > 63 && cell->GetIndex() < 72)
			|| (cell->GetIndex() > 81 && cell->GetIndex() < 86)
			|| (cell->GetIndex() > 93 && cell->GetIndex() < 98)
			|| (cell->GetIndex() > 103 && cell->GetIndex() < 128)) {
			produces_M = true;
		}
	} else if (m_grid_size == 512) {
		if ((cell->GetIndex() > 15 && cell->GetIndex() < 32)
			|| (cell->GetIndex() > 52 && cell->GetIndex() < 61)
			|| (cell->GetIndex() > 63 && cell->GetIndex() < 72)
			|| (cell->GetIndex() > 81 && cell->GetIndex() < 86)
			|| (cell->GetIndex() > 93 && cell->GetIndex() < 98)
			|| (cell->GetIndex() > 103 && cell->GetIndex() < 128)
			|| (cell->GetIndex() > 135 && cell->GetIndex() < 144)
			|| (cell->GetIndex() > 187 && cell->GetIndex() < 196)
			|| (cell->GetIndex() > 207 && cell->GetIndex() < 256)
			|| (cell->GetIndex() > 280 && cell->GetIndex() < 297)
			|| (cell->GetIndex() > 375 && cell->GetIndex() < 392)
			|| (cell->GetIndex() > 395 && cell->GetIndex() < 412)
			|| (cell->GetIndex() > 431 && cell->GetIndex() < 512)) {
			produces_M = true;
		}
	}

	if( produces_M && time_now < m_M_duration ) {
		dchem[1] = m_M_production - m_M_degradation * chem1;  //Half the production rate w.r.t. reference model
	} else {
		dchem[1] = -m_M_degradation * chem1;
	}
}

} // namespace
} // namespace
