/*
 * Copyright 2011-2018 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Maize model.
 */

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"
//#include "util/Exception.h"
//#include "util/log_debug.h"
#include <cmath> //
#include "MaizeCC.h"

using namespace std;
using namespace SimPT_Sim::Util;
using boost::property_tree::ptree;

namespace SimPT_Blad {
namespace CellChemistry {

MaizeCC::MaizeCC(const CoreData& cd)
{
	Initialize(cd);
}

double MaizeCC::Michaelis(double M1, double J1, double K1, double S1)
{
	double rate = 0;
	return rate = M1 * S1 / ( J1 + K1 );

}

double MaizeCC::Hill(double Vm, double Km, double h, double S)
{
	double rate = 0;
	return rate = Vm * pow ( S, h ) / ( pow ( S, h ) + pow ( Km, h ) ) ;
}

double MaizeCC::Goldbeter(double A1, double A2, double A3, double A4)
{
	double rate = 0;
	double BB = A2 - A1 + A3 * A2 + A4 * A1 ;
	return rate = 2.0 * A4 * A1 / ( BB + pow ( ( pow ( BB, 2.0 ) - 4.0 * ( A2 - A1 ) * A4 * A1 ), .5 ) ) ;
}

void MaizeCC::Initialize(const CoreData& cd)
{
		m_cd = cd;
		const auto& q = m_cd.m_parameters->get_child("maize");

		m_ksCK	  	  = q.get<double>("ksCK");
		m_ksCKp		  = q.get<double>("ksCKp");
		m_kdCK		  = q.get<double>("kdCK");
		m_ksAUX		  = q.get<double>("ksAUX");
		m_ksAUXp	  = q.get<double>("ksAUXp");
		m_ksAUXpp	  = q.get<double>("ksAUXpp");
		m_kdAUX	 	  = q.get<double>("kdAUX");
		m_tSUP		  = q.get<double>("tSUP");
		m_kdGA1	  	  = q.get<double>("kdGA1");
		m_kGA203OX	  = q.get<double>("kGA203OX");
		m_kGA2OX	  = q.get<double>("kGA2OX");
		m_KmGA1	      = q.get<double>("KmGA1");
		m_kdGA8       = q.get<double>("kdGA8");
		m_ksDELLA     = q.get<double>("ksDELLA");
		m_kdDELLA     = q.get<double>("kdDELLA");
		m_kdDELLAp    = q.get<double>("kdDELLAp");
		m_ksGA2OX	  = q.get<double>("ksGA2OX");
		m_ksGA2OXp	  = q.get<double>("ksGA2OXp");
		m_KiDELLA	  = q.get<double>("KiDELLA");
		m_kdGA2OX	  = q.get<double>("kdGA2OX");
		m_ksGA203OX	  = q.get<double>("ksGA203OX");
		m_kdGA203OX	  = q.get<double>("kdGA203OX");
		m_KiCK		  = q.get<double>("KiCK");
		m_ksCDK		  = q.get<double>("ksCDK");
		m_ksCDKp	  = q.get<double>("ksCDKp");
		m_KiDELLAp	  = q.get<double>("KiDELLAp");
		m_KmAUX		  = q.get<double>("KmAUX");
		m_kdCDK 	  = q.get<double>("kdCDK");
		m_KmCK		  = q.get<double>("KmCK");
		m_KiAUX		  = q.get<double>("KiAUX");
		m_KaAUX		  = q.get<double>("KaAUX");
		m_tEZ		  = q.get<double>("tEZ");
		m_KiM		  = q.get<double>("KiM");//Feedback factor M
		m_kdM		  = q.get<double>("kdM");//Feedback factor M
		m_ksM		  = q.get<double>("ksM");//Feedback factor M

}

void MaizeCC::operator()(Cell* cell, double* dchem)
{

	double time_now 	= m_cd.m_time_data->m_sim_time;

	const double a_area	= cell->GetArea();

	const double chem0      = cell->GetChemical(0);//CK
	const double chem1      = cell->GetChemical(1);//AUX
	const double chem3      = cell->GetChemical(3);//GA1
	const double chem4      = cell->GetChemical(4);//GA8
	const double chem5      = cell->GetChemical(5);//DELLA
	const double chem6      = cell->GetChemical(6);//GA203OX
	const double chem7      = cell->GetChemical(7);//GA2OX
	const double chem8      = cell->GetChemical(8);//Feedback factor 'M'
	const double chem9      = cell->GetChemical(9);//CDK

////CONSTANT PRODUCTION MODE:
	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 )
	{
 		if( time_now < m_tSUP )
 		{
	    		dchem[0] = m_ksCK - m_kdCK * chem0;
	    		dchem[1] = m_ksAUX - m_kdAUX * chem1;
 		}
 		else
 		{
 			dchem[0] = m_ksCKp - m_kdCK * chem0;
 			dchem[1] = m_ksAUXp - m_kdAUX * chem1;
 			dchem[3] = - m_kdGA1 * chem3;
 			dchem[4] = - m_kdGA8 * chem4;
    	}
	}

////LINEAR PRODUCTION MODE:
/**	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 )
	{
		if( time_now < m_tSUP )
		{
			dchem[0] = m_ksCK * (1 - time_now / m_tSUP ) - m_kdCK * chem0;//CK
			dchem[1] = m_ksAUX * (1 - time_now / m_tSUP )- m_kdAUX * chem1;//AUX
		}
		else
		{
			dchem[0] = m_ksCKp - m_kdCK * chem0;
			dchem[1] = m_ksAUXp - m_kdAUX * chem1;
			dchem[3] = - m_kdGA1 * chem3;
			dchem[4] = - m_kdGA8 * chem4;
		}
	}
*/

////SLIDE (CONSTANT+LINEAR) PRODUCTION MODE:
/**	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 )
	{
		if( time_now < (27./65.) * m_tSUP )
		{
			dchem[0] = m_ksCK * (1 - time_now / m_tSUP ) - m_kdCK * chem0;//CK
			dchem[1] = m_ksAUX * (1 - time_now / m_tSUP )- m_kdAUX * chem1;//AUX
		}
		else if( time_now >= (27./65.) * m_tSUP && time_now < m_tSUP )
		{
			dchem[0] = m_ksCK * (1 - (time_now - (27./65.)*m_tSUP) / (1* m_tSUP - (27./65.)*m_tSUP) ) - m_kdCK * chem0;//CK
			dchem[1] = m_ksAUX * (1 - (time_now - (27./65.)*m_tSUP) / (1* m_tSUP - (27./65.)*m_tSUP) ) - m_kdAUX * chem1;//AUX
		}
		else
		{
			dchem[0] = m_ksCKp - m_kdCK * chem0;
			dchem[1] = m_ksAUXp - m_kdAUX * chem1;
			dchem[3] = - m_kdGA1 * chem3;
			dchem[4] = - m_kdGA8 * chem4;
		}
	}
*/

	else
	{
		if( time_now < m_tSUP )
		{
    		dchem[0] = m_ksCKp - m_kdCK * chem0;
    		dchem[1] = m_ksAUXpp - m_kdAUX * chem1;
		}
		else
		{
			dchem[0] = m_ksCKp - m_kdCK * chem0;
			dchem[1] = m_ksAUXp - m_kdAUX * chem1;
		}
	}

///For FEEDBACK MODEL replace above lines
///i.e.	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 ){} else {}, with:
/**	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 )
	{
  		dchem[0] = (1/(1+chem8/m_KiX)) * m_ksCK + m_ksCKp - m_kdCK * chem0;//CK
  		dchem[1] = (1/(1+chem8/m_KiX)) * m_ksAUX + m_ksAUXp - m_kdAUX * chem1;//AUX
  		dchem[3] = - m_kdGA1 * chem3;//GA1
	}
	else
	{
		dchem[0] = m_ksCKp - m_kdCK * chem0;
		dchem[1] = m_ksAUXpp - m_kdAUX * chem1;
	}
*/


	if ( (cell->GetIndex() < 28 || cell->GetIndex() > 31 ) )
	{

		dchem[3] =  m_kGA203OX * chem6 +
			+ 0.00000 * a_area * Hill( chem7 / a_area , 10 , 1, chem4 / a_area )//GA8->GA1
    		- m_kGA2OX * a_area * Hill( chem7 / a_area , m_KmGA1 , 1 , chem3 / a_area )//GA1->GA8
    		- m_kdGA1 * chem3;//GA1->0

		dchem[6] = 1 * m_ksGA203OX * a_area / (1 + pow( m_KaAUX / (chem1 / a_area), 2 ) + pow( (chem0 / a_area) / m_KiCK, 2) ) - m_kdGA203OX * chem6;

		////GA8
		dchem[4] = m_kGA2OX * a_area * Hill( chem7 / a_area , m_KmGA1 , 1 , chem3 / a_area )//GA1->GA8
				- 0.00000 * a_area * Hill( chem7 / a_area , 10 , 1, chem4 / a_area )//GA8->GA1
				- m_kdGA8 * chem4;//GA8->0

		////DELLA
		dchem[5] = m_ksDELLA * a_area//0->DELLA
				 -  chem5 * ( m_kdDELLA + m_kdDELLAp * chem3 / a_area );//DELLA->0

		////GA2-OX
		dchem[7] =  0.3 * a_area * ( m_ksGA2OX + m_ksGA2OXp * ( 1 / ( 1 + pow((chem1 / a_area) / m_KiAUX, 2 ))) / ( m_KiDELLA + (chem5 / a_area) ))
							- m_kdGA2OX * chem7;

		////Un-comment for FEEDBACK 'M factor' MODEL
/**		if ((time_now - cell->GetDivisionTime()) > m_tEZ)
		{
			dchem[8] = m_ksM - m_ksM * chem8;
		}
		else
		{
			dchem[8] = - m_ksM * chem8;
		}
*/

		////CDK

		if (time_now >= 30)//avoids instability w.r.t. auxin transport ode
		{
		dchem[9] =  a_area 	* ( (chem0 / a_area) / ( m_KmCK + (chem0 / a_area)  ) )
					* ( (chem1 / a_area) / ( m_KmAUX + (chem1 / a_area)  ) )
					* ( m_ksCDK +  m_ksCDKp / ( m_KiDELLAp +  (chem5 / a_area) ) )
					- m_kdCDK * chem9;
		}

	}
}

} // namespace
} // namespace


