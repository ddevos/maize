#ifndef BLAD_CELL_CHEMISTRY_MAIZECC_H_INCLUDED
#define BLAD_CELL_CHEMISTRY_MAIZECC_H_INCLUDED
/*
 * Copyright 2011-2018 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Maize model.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim { class Cell; }

namespace SimPT_Blad {
namespace CellChemistry {

using namespace SimPT_Sim;

/**
 * CellChemistry for Maize model.
 */
class MaizeCC
{
public:
	/// Initializing constructor.
	 MaizeCC(const CoreData& cd);

	/// Initialize or re-initialization.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Cell* cell, double* dchem);

private:
	double Michaelis(double M1, double J1, double K1, double S1);

	//Calculate rate according to Hill cooperative kinetics
	double Hill(double Vm, double Km, double h, double S);

	double Goldbeter(double A1, double A2, double A3, double A4);

	CoreData    m_cd;              ///< Core data (mesh, params, sim_time,...).

	double	    m_ksCK;
	double	    m_ksCKp;
	double	    m_kdCK;
	double 	    m_ksAUX;
	double 	    m_ksAUXp;
	double 	    m_ksAUXpp;
	double 	    m_kdAUX;
	double	    m_tSUP;
	double      m_kdGA1;
	double      m_kGA203OX;
	double      m_kGA2OX;
	double 	    m_KmGA1;
	double      m_kdGA8;
	double      m_ksDELLA;
	double      m_kdDELLA;
	double      m_kdDELLAp;
	double	    m_ksGA2OX;
	double	    m_ksGA2OXp;
	double	    m_KiDELLA;
	double	    m_kdGA2OX;
	double	    m_ksGA203OX;
	double	    m_kdGA203OX;
	double	    m_KiCK;
	double	    m_ksCDK;
	double	    m_ksCDKp;
	double 	    m_KiDELLAp;
	double	    m_KmAUX;
	double	    m_kdCDK;
	double      m_KmCK;
	double      m_KiAUX;
	double      m_KaAUX;
	double      m_KiX;
	double		m_tEZ;
	double		m_KiM;//Feedback factor M
	double		m_kdM;//Feedback factor M
	double		m_ksM;//Feedback factor M
};

} // namespace
} // namespace

#endif // include guard
