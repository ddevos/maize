#ifndef BLAD_COMPONENT_FACTORY_H_INCLUDED
#define BLAD_COMPONENT_FACTORY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Component factory for the Blad model group.
 */

#include "model/ComponentFactoryBase.h"
#include "model/ComponentTraits.h"

namespace SimPT_Sim { struct CoreData; }

namespace SimPT_Blad {

using namespace SimPT_Sim;
using namespace std;
//using namespace SimPT_Sim;
using boost::property_tree::ptree;

/**
 * Component factory for the Blad model group.
 */
class ComponentFactory: public ComponentFactoryBase
{
public:
        /// @see ComponentAbstractFacory
        virtual CellChemistryComponent CreateCellChemistry(const CoreData& cd) const override final;

        /// @see ComponentAbstractFacory
        virtual CellColorComponent CreateCellColor(const string& select, const ptree&) const override final;

        /// @see ComponentAbstractFacory
        virtual CellDaughtersComponent CreateCellDaughters(const CoreData& cd) const override final;

        /// @see ComponentAbstractFacory
        virtual CellHousekeepComponent CreateCellHousekeep(const CoreData& cd) const override final;

        /// @see ComponentAbstractFacory
        virtual CellSplitComponent CreateCellSplit(const CoreData& cd) const override final;

        /// @see ComponentAbstractFacory
        virtual CellToCellTransportComponent CreateCellToCellTransport(const CoreData& cd) const override final;

public:
        /// List components for CellChemistry.
        virtual std::vector<std::string>  ListCellChemistry() const override final;

        /// List components for CellColor.
        virtual std::vector<std::string>  ListCellColor() const override final;

        /// List components for cell daughters.
        virtual std::vector<std::string>  ListCellDaughters() const override final;

        /// List components for CellHouseKeep.
        virtual std::vector<std::string>  ListCellHousekeep() const override final;

        /// List components for cell division.
        virtual std::vector<std::string>  ListCellSplit() const override final;

        /// List components for CellToCellTransport.
        virtual std::vector<std::string>  ListCellToCellTransport() const override final;
};

} // namespace

#endif // end_of_include_guard
