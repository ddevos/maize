/*
 * Copyright 2011-2018 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters for Maize model.
 */

#include "bio/Cell.h"
#include "MaizeCD.h"

namespace SimPT_Blad {
namespace CellDaughters {

MaizeCD::MaizeCD(const CoreData& cd)
{
	Initialize(cd);
}

void MaizeCD::Initialize(const CoreData& cd)
{
	m_cd = cd;
}

void MaizeCD::operator()(Cell* daughter1, Cell* daughter2)
{
	double const area1 = daughter1->GetArea();
	double const area2 = daughter2->GetArea();
	double const tot_area = area1 + area2;

	if (daughter1->HasNeighborOfTypeZero())
	{
		daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
		daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));
		daughter1->SetChemical(2, daughter1->GetChemical(2) * (area1 / tot_area));
		daughter1->SetChemical(3, daughter1->GetChemical(3) * (area1 / tot_area));
		daughter1->SetChemical(4, daughter1->GetChemical(4) * (area1 / tot_area));
		daughter1->SetChemical(5, daughter1->GetChemical(5) * (area1 / tot_area));
		daughter1->SetChemical(6,0.);
		daughter1->SetChemical(7,0.);
		daughter1->SetChemical(8, daughter1->GetChemical(8) * (area1 / tot_area));
		daughter1->SetChemical(9, daughter1->GetChemical(9) * (area1 / tot_area));

		daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
		daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));
		daughter2->SetChemical(2, daughter2->GetChemical(2) * (area2 / tot_area));
		daughter2->SetChemical(3, daughter2->GetChemical(3) * (area2 / tot_area));
		daughter2->SetChemical(4, daughter2->GetChemical(4) * (area2 / tot_area));
		daughter2->SetChemical(5, daughter2->GetChemical(5) * (area2 / tot_area));
		daughter2->SetChemical(6, daughter2->GetChemical(6) * (area2 / tot_area));
		daughter2->SetChemical(7, daughter2->GetChemical(7) * (area2 / tot_area));
		daughter2->SetChemical(8, daughter2->GetChemical(8) * (area2 / tot_area));
		daughter2->SetChemical(9, daughter2->GetChemical(9) * (area2 / tot_area));
	}
	else if (daughter2->HasNeighborOfTypeZero())
	{

		daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
		daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));
		daughter1->SetChemical(2, daughter1->GetChemical(2) * (area2 / tot_area));
		daughter1->SetChemical(3, daughter1->GetChemical(3) * (area2 / tot_area));
		daughter1->SetChemical(4, daughter1->GetChemical(4) * (area2 / tot_area));
		daughter1->SetChemical(5, daughter1->GetChemical(5) * (area2 / tot_area));
		daughter2->SetChemical(6,0.);
		daughter2->SetChemical(7,0.);
		daughter2->SetChemical(8, daughter2->GetChemical(8) * (area2 / tot_area));
		daughter2->SetChemical(9, daughter2->GetChemical(9) * (area2 / tot_area));

		daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
		daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));
		daughter1->SetChemical(2, daughter1->GetChemical(2) * (area1 / tot_area));
		daughter1->SetChemical(3, daughter1->GetChemical(3) * (area1 / tot_area));
		daughter1->SetChemical(4, daughter1->GetChemical(4) * (area1 / tot_area));
		daughter1->SetChemical(5, daughter1->GetChemical(5) * (area1 / tot_area));
		daughter1->SetChemical(6, daughter1->GetChemical(6) * (area1 / tot_area));
		daughter1->SetChemical(7, daughter1->GetChemical(7) * (area1 / tot_area));
		daughter1->SetChemical(8, daughter1->GetChemical(8) * (area1 / tot_area));
		daughter1->SetChemical(9, daughter1->GetChemical(9) * (area1 / tot_area));
	}
	else
	{
		daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));//CK
		daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
		daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));//AUX
		daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));
		daughter1->SetChemical(2, daughter1->GetChemical(2) * (area1 / tot_area));
		daughter2->SetChemical(2, daughter2->GetChemical(2) * (area2 / tot_area));
		daughter1->SetChemical(3, daughter1->GetChemical(3) * (area1 / tot_area));//GA1
		daughter2->SetChemical(3, daughter2->GetChemical(3) * (area2 / tot_area));
		daughter1->SetChemical(4, daughter1->GetChemical(4) * (area1 / tot_area));//GA8
		daughter2->SetChemical(4, daughter2->GetChemical(4) * (area2 / tot_area));
		daughter1->SetChemical(5, daughter1->GetChemical(5) * (area1 / tot_area));//DELLA
		daughter2->SetChemical(5, daughter2->GetChemical(5) * (area2 / tot_area));
		daughter1->SetChemical(6, daughter1->GetChemical(6) * (area1 / tot_area));//GA203OX
		daughter2->SetChemical(6, daughter2->GetChemical(6) * (area2 / tot_area));
		daughter1->SetChemical(7, daughter1->GetChemical(7) * (area1 / tot_area));//GA2OX
		daughter2->SetChemical(7, daughter2->GetChemical(7) * (area2 / tot_area));
		daughter1->SetChemical(8, daughter1->GetChemical(8) * (area1 / tot_area));//M factor
		daughter2->SetChemical(8, daughter2->GetChemical(8) * (area2 / tot_area));
		daughter1->SetChemical(9, daughter1->GetChemical(9) * (area1 / tot_area));//CDK
		daughter2->SetChemical(9, daughter2->GetChemical(9) * (area2 / tot_area));
	}

	daughter1->SetTransporters(1, 1., 1.);//Default: does not affect simulation
	daughter2->SetTransporters(1, 1., 1.);//Default: Does not affect simulation
}

} // namespace
} // namespace



