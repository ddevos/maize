/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep component factory map.
 */

#include "factories.h"

#include "Blad.h"
#include <boost/functional/value_factory.hpp>
#include <utility>
#include "MaizeCH.h"


namespace SimPT_Blad {
namespace CellHousekeep {

// ------------------------------------------------------------------------------
// Add new classes here (but do not forget to include the header also).
// ------------------------------------------------------------------------------
const typename ComponentTraits<CellHousekeepTag>::MapType g_component_factories
{{
	std::make_pair("Blad",        	boost::value_factory<Blad>()),
    std::make_pair("MaizeCH",        	boost::value_factory<MaizeCH>())
}};

} // namespace
} // namespace
