/*
 * Copyright 2011-2018 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for Maize model.
 */

#include "bio/Cell.h"
#include "bio/Wall.h"
#include "MaizeRT.h"

namespace SimPT_Blad {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

MaizeRT::MaizeRT(const CoreData& cd)
{
        Initialize(cd);
}

void MaizeRT::Initialize(const CoreData& cd)
{
		m_cd    = cd;
		const auto& p             = m_cd.m_parameters;

		m_chemical_count   = p->get<unsigned int>("model.cell_chemical_count");

		ptree const& arr_D = p->get_child("auxin_transport.D.value_array");
		unsigned int c = 0;
		for (auto it = arr_D.begin(); it != arr_D.end(); it++) {
			if (c == m_chemical_count) {
				break;
			}
			m_D.push_back(it->second.get_value<double>());
			c++;
		}
		// Defensive: if fewer than chemical_count parameters were specified, fill up.
		for (unsigned int i = c; i < m_chemical_count; i++) {
			m_D.push_back(0.0);
		}

}

void MaizeRT::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	if ( !(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon()) )
	{

		// CK
		double phi0 = (w->GetLength() ) * m_D[0] * ( ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) ); //LEVELS!


		dchem_c1[0] += phi0 ;
		dchem_c2[0] -= phi0 ;

		// AUX
		double phi1 = (w->GetLength() ) * m_D[1] * ( ( w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()) ) ); //LEVELS!


		dchem_c1[1] += phi1 ;
		dchem_c2[1] -= phi1 ;

		// GA1
		double phi3 = (w->GetLength() ) * m_D[3] * ( ( w->GetC2()->GetChemical(3) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(3) / (w->GetC1()->GetArea()) ) ); //LEVELS!


		dchem_c1[3] += phi3 ;
		dchem_c2[3] -= phi3 ;

		// GA8
		double phi4 = (w->GetLength() ) * m_D[4] * ( ( w->GetC2()->GetChemical(4) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(4) / (w->GetC1()->GetArea()) ) ); //LEVELS!


		dchem_c1[4] += phi4 ;
		dchem_c2[4] -= phi4 ;

/*
 * Un-comment following lines concerning chemical 8 for Feedback inhibition model
 */
//	    double phi8 = (w->GetLength() ) * m_D[8] * ( ( w->GetC2()->GetChemical(8) / (w->GetC2()->GetArea()) )
//				- ( w->GetC1()->GetChemical(8) / (w->GetC1()->GetArea()) ) ); //LEVELS!
//
//		dchem_c1[8] += phi8 ; //LEVELS!
//		dchem_c2[8] -= phi8 ; //LEVELS!

	}
}

} // namespace
} // namespace
