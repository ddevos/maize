/*
* Copyright 2011-2018 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellColor for Maize model.
 */

#include "Maize.h"

#include "bio/Cell.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Blad {
namespace CellColor {

using namespace std;

Maize::Maize(const boost::property_tree::ptree& )
{
}

array<double, 3> Maize::operator()(SimPT_Sim::Cell* cell)
{
	const double c9 = (cell->GetChemicals().size() >= 2) ? cell->GetChemical(9) / cell->GetArea() : 0.0;
//	return  (c9 <= 0.8) ? array<double, 3> {{ 1.0, 1.0, 1.0}} : array<double, 3> {{1.0, c9 / (1. + c9 ), 0.0 }};
	return  (c9 <= 9.5) ? array<double, 3> {{ 0.0, 1.0, 0.0}} : array<double, 3> {{0.0, 0.0, 1.0 }};

}

} // namespace
} // namespace



