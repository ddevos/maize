/*
 * Copyright 2011-2018 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Maize model.
 */

#include "bio/Cell.h"

#include "math/RandomEngine.h"
#include <trng/uniform01_dist.hpp>


#include <cmath>
#include <iostream>
#include "MaizeCS.h"

namespace SimPT_Blad {
namespace CellSplit {

using namespace std;
using namespace SimPT_Sim::Util;
using boost::property_tree::ptree;

MaizeCS::MaizeCS(const CoreData& cd)
{
	Initialize(cd);
}

void MaizeCS::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p					= m_cd.m_parameters->get_child("cell_mechanics");
        const auto& q					= m_cd.m_parameters->get_child("maize");

		const uniform_real_distribution<double> dist(0.0, 1.0);
		m_uniform_generator 		= m_cd.m_random_engine->GetGenerator(dist);

		m_fixed_division_axis      = p.get<bool>("fixed_division_axis", false);
		m_cell_division_noise  	   = q.get<double>("cell_division_noise");
		m_cell_division_threshold  = q.get<double>("cell_division_threshold");
		m_CDK_threshold	      	   = q.get<double>("CDK_threshold");

}

std::tuple<bool, bool, std::array<double, 3>> MaizeCS::operator()(Cell* cell)
{
	const int celltype		= cell->GetCellType();
	const double a_area		= cell->GetArea();
	const double chem9      = cell->GetChemical(9);

	bool must_divide        = false;

	double time_now 	= m_cd.m_time_data->m_sim_time;
	double CCnoise = 1 + m_cell_division_noise * (m_uniform_generator() - 0.5);

	if (cell->GetBoundaryType() == BoundaryType::None) {
		if(  time_now <= 300 )
		{
			if ( (cell->GetCentroid()[1])  > (128. - 80 ) && celltype != 0 && a_area >= (  m_cell_division_threshold * CCnoise) )
			{
				must_divide = true;
			}
		}
		else
		{
			if(  ((chem9 / a_area) > m_CDK_threshold ) && a_area >= (  m_cell_division_threshold * CCnoise ) )
			{
				must_divide = true;
			}
		}
	}
	return std::make_tuple(must_divide, true, array<double, 3> {{1.0, 0.0, 0.0}});


}

} // namespace
} // namespace





