#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
                     
#============================================================================
# Test definitions.
#============================================================================

#--------------------------- variables --------------------------------------
set( EXEC_PATH    ${CMAKE_INSTALL_PREFIX}/${BIN_INSTALL_LOCATION}/$<TARGET_FILE_NAME:simPT_sim> )
set( WORKSPACE    ${CMAKE_INSTALL_PREFIX}/${TESTS_INSTALL_LOCATION}/data/simPT_Default_workspace )
set( DATA_FILE    "leaf_000000.xml" )

#------------------------ test definition -----------------------------------
foreach (PROJ AuxinGrowth Geometric Meinhardt NoGrowth TipGrowth )
	add_test( NAME  ${PROJ}_start_basic
        	WORKING_DIRECTORY ${WORKSPACE}/${PROJ} 
                COMMAND ${EXEC_PATH} -m cli -w ${WORKSPACE} -p ${PROJ} -f ${DATA_FILE} -s 5 -q 
            )
 	add_test( NAME  ${PROJ}_start_full
        	WORKING_DIRECTORY ${WORKSPACE}/${PROJ} 
                COMMAND ${EXEC_PATH} -m cli -w ${WORKSPACE} -p ${PROJ} -f ${DATA_FILE} -s 50 -q 
            )               
endforeach()

# Sean: Removed Wortel until fixed 
foreach (PROJ  WrapperModel TestCoupling)
	add_test( NAME  ${PROJ}_start_basic
        	WORKING_DIRECTORY ${WORKSPACE}/${PROJ} 
                COMMAND ${EXEC_PATH} -m cli -w ${WORKSPACE} -p ${PROJ} -f ${DATA_FILE} -s 1 -q 
            )
	add_test( NAME  ${PROJ}_start_full
        	WORKING_DIRECTORY ${WORKSPACE}/${PROJ} 
                COMMAND ${EXEC_PATH} -m cli -w ${WORKSPACE} -p ${PROJ} -f ${DATA_FILE} -s 5 -q 
            )                
endforeach()
        
#-------------------------- unset variables ---------------------------------
unset( EXEC_PATH  ) 
unset( WORKSPACE  )

#############################################################################
