#ifndef DEFAULT_WALL_CHEMISTRY_NOOP_H_INCLUDED
#define DEFAULT_WALL_CHEMISTRY_NOOP_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * WallChemistry NoOp component.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim { class Wall; }

namespace SimPT_Default {
namespace WallChemistry {

using namespace SimPT_Sim;

/**
 * NoOp (do nothing) approach to wall chemistry.
 */
class NoOp
{
public:
	/// Set up empty object.
	NoOp(unsigned int ) {}

	/// Initializing constructor.
	NoOp(const CoreData& ) {}

	/// Initialize from ptree.
	void Initialize(const CoreData& ) {}

	/// Execute.
	void operator()(Wall* , double* , double* ) {}
};

} // namespace
} // namespace

#endif // end_of_include_guard
