#ifndef DEFAULT_WALL_CHEMISTRY_MEINHARDT_H_INCLUDED
#define DEFAULT_WALL_CHEMISTRY_MEINHARDT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * WallChemistry for Meinhardt model.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim { class Wall; }

namespace SimPT_Default {
namespace WallChemistry {

using namespace SimPT_Sim;

/**
 * Meinhardt version of wall chemistry.
 */
class Meinhardt
{
public:
	/// Set up empty object.
	Meinhardt();

	/// Initializing constructor.
	Meinhardt(const CoreData& cd);

	/// Initialize from ptree.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Wall* w, double* dw1, double* dw2);

private:
	CoreData      m_cd;                    ///< Core data (mesh, params, sim_time,...).
	unsigned int  m_chemical_count;
};

} // namespace
} // namespace

#endif // end_of_include_guard
