/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Basic WallChemistry component.
 */

#include "Basic.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace WallChemistry {

using namespace std;
using namespace boost::property_tree;

Basic::Basic()
	: m_k1(0.0), m_k2(0.0), m_km(0.0), m_kr(0.0), m_r(0.0)
{
}

Basic::Basic(const CoreData& cd)
	: Basic()
{
	Initialize(cd);
}

void Basic::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        m_k1 = p->get<double>("auxin_transport.k1");
        m_k2 = p->get<double>("auxin_transport.k2");
        m_km = p->get<double>("auxin_transport.km");
        m_kr = p->get<double>("auxin_transport.kr");
        m_r  = p->get<double>("auxin_transport.r");
}

void Basic::operator()(Wall* w, double* dw1, double* dw2)
{
	// add biochemical networks for reactions occurring at walls here
	dw1[0] = 0.;
	dw2[0] = 0.; // chemical 0 unused in walls
	dw1[1] = PINflux(w->GetC1(), w->GetC2(), w);
	dw2[1] = PINflux(w->GetC2(), w->GetC1(), w);
}

double Basic::PINflux(Cell* this_cell, Cell* adjacent_cell, Wall* w)
{
	// PIN1 localization at wall
	// Note: chemical 0 is Auxin (intracellular storage only)
	//  PIN1 is Chemical 1 (both in walls and intracellular storage)
	//! \f$ \frac{d Pij/dt}{dt} = k_1 A_j \frac{P_i}{L_ij} - k_2 P_{ij} \f$
	// Note that Pij is measured in term of concentration (mol/L)
	// Pi in terms of quantity (mol)
	// Equations as in Merks et al., Trends in Plant Science 2007

	// calculate PIN translocation rate from cell to membrane
	double const adj_auxin = adjacent_cell->GetChemical(0);
	double const receptor = adj_auxin * m_r / (m_kr + adj_auxin);

	// pick the correct side of the Wall
	double pin_atwall;
	if (w->GetC1() == this_cell) {
		pin_atwall = w->GetTransporters1(1);
	} else {
		pin_atwall = w->GetTransporters2(1);
	}

	// note: pin_flux is net flux from endosome to wall
	double const chem = this_cell->GetChemical(1);
	double const pin_flux = m_k1 * chem * receptor / (m_km + chem) - m_k2 * pin_atwall;
	return pin_flux;
}

} // namespace
} // namespace
