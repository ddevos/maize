/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CellColor::ChemGreen.
 */

#include "ChemGreen.h"

#include "bio/Cell.h"
#include "util/color/hsv.h"
#include "util/color/Hsv2Rgb.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellColor {

using namespace std;
using namespace SimPT_Sim::Color;

ChemGreen::ChemGreen(const boost::property_tree::ptree& , unsigned int index)
	: m_index(index)
{
}

array<double, 3> ChemGreen::operator()(SimPT_Sim::Cell* cell)
{
	const double c0    = (cell->GetChemicals().size() > m_index) ? cell->GetChemical(m_index) : 0.0;
	const double conc  = max(c0, 0.0) / cell->GetArea();

	// ------------------------------------------------------------------
	// Lime green at full saturation; brightness (value) proportional
	// to log10 of concentration with 1.0 as reference concentration
	// and a range of four orders of magnitude: concentrations above
	// that get mapped to brightness 1.0 anyway.
	// ------------------------------------------------------------------
//	const double h = 120.0 / 360.0;    // lime green
//	const double s = 1.0;              // full saturation
//	// value 0.0 corresponds to black; value 1.0 is green.
//	const double lower = 1.0e-4;
//	const double     v = min(max(0.0, log10(conc/lower)), 1.0);

	const double x = log10(conc);

	const double x_black = -4.0;
	const double x_lime  = -3.0;

	hsv_t t_color;
	if (x < x_black) {
		t_color = black;
	}
	else if (x < x_lime) {
		t_color = interpolate(x, black, x_black, lime, x_lime);
	}
	else {
		t_color = lime;
	}

	return Hsv2Rgb(get<0>(t_color), get<1>(t_color), get<2>(t_color));
}

} // namespace
} // namespace


