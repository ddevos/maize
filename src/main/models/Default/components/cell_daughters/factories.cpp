/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters component factory map.
 */

#include "factories.h"

#include "Auxin.h"
#include "BasicPIN.h"
#include "NoOp.h"
#include "Perimeter.h"
#include "PIN.h"
#include "SmithPhyllotaxis.h"
#include "Wortel.h"
#include "WrapperModel.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

namespace SimPT_Default {
namespace CellDaughters {

const typename ComponentTraits<CellDaughtersTag>::MapType g_component_factories
{{
	std::make_pair("Auxin",               boost::value_factory<Auxin>()),
	std::make_pair("BasicPIN",            boost::value_factory<BasicPIN>()),
	std::make_pair("NoOp",                boost::value_factory<NoOp>()),
	std::make_pair("Perimeter",           boost::value_factory<Perimeter>()),
	std::make_pair("PIN",                 boost::value_factory<PIN>()),
	std::make_pair("SmithPhyllotaxis",    boost::value_factory<SmithPhyllotaxis>()),
	std::make_pair("Wortel",              boost::value_factory<Wortel>()),
	std::make_pair("WrapperModel",        boost::value_factory<WrapperModel>())
}};

} // namespace
} // namespace
