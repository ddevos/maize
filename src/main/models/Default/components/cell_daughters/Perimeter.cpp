/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters::Perimeter implementation file.
 */

#include "Perimeter.h"

#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "math/RandomEngine.h"

#include <trng/uniform01_dist.hpp>
#include <cmath>

using namespace std;
using namespace boost::property_tree;

namespace SimPT_Default {
namespace CellDaughters {

Perimeter::Perimeter(const CoreData& cd)
{
	Initialize(cd);
}

void Perimeter::Initialize(const CoreData& cd)
{
        m_cd = cd;

        const trng::uniform01_dist<double> dist;
        m_uniform_generator = m_cd.m_random_engine->GetGenerator(dist);
}

void Perimeter::operator()(Cell* daughter1, Cell* daughter2)
{
	// set one cell to source after first division
	const unsigned int cell_count = m_cd.m_mesh->GetCells().size();
	if (cell_count == 2) {
		daughter1->SetCellType(1);
		daughter2->SetCellType(0);
	}

	// if a source cell has divided, one of the daughters becomes the new source
	if (daughter1->GetCellType() == 1) {
		// if both cells are at the tissue perimeter, choose at random
		if (daughter1->HasBoundaryWall() && daughter2->HasBoundaryWall()) {
			if (m_uniform_generator() < 0.5) {
				daughter1->SetCellType(1);
				daughter2->SetCellType(0);
			} else {
				daughter1->SetCellType(0);
				daughter2->SetCellType(1);
			}
		} else {
			// otherwise choose the one that is still at the perimeter
			if (daughter1->HasBoundaryWall()) {
				daughter1->SetCellType(1);
				daughter2->SetCellType(0);
			} else {
				daughter1->SetCellType(0);
				daughter2->SetCellType(1);
			}
		}
	}
}

} // namespace
} // namespace
