/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters for Wortel model.
 */

#include "Wortel.h"

#include "bio/Cell.h"

namespace SimPT_Default {
namespace CellDaughters {

Wortel::Wortel(const CoreData& cd)
{
	Initialize(cd);
}

void Wortel::Initialize(const CoreData& cd)
{
	m_cd = cd;
}

void Wortel::operator()(Cell* daughter1, Cell* daughter2)
{
	double const area1 = daughter1->GetArea();
	double const area2 = daughter2->GetArea();
	double const tot_area = area1 + area2;

	daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
	daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
	daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));
	daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));
	daughter1->SetChemical(2, daughter1->GetChemical(2) * (area1 / tot_area));
	daughter2->SetChemical(2, daughter2->GetChemical(2) * (area2 / tot_area));
	daughter1->SetChemical(3, daughter1->GetChemical(3) * (area1 / tot_area));
	daughter2->SetChemical(3, daughter2->GetChemical(3) * (area2 / tot_area));
	daughter1->SetChemical(4, daughter1->GetChemical(4) * (area1 / tot_area));
	daughter2->SetChemical(4, daughter2->GetChemical(4) * (area2 / tot_area));
	daughter1->SetChemical(5, daughter1->GetChemical(5) * (area1 / tot_area));
	daughter2->SetChemical(5, daughter2->GetChemical(5) * (area2 / tot_area));
	daughter1->SetChemical(6, daughter1->GetChemical(6) * (area1 / tot_area));
	daughter2->SetChemical(6, daughter2->GetChemical(6) * (area2 / tot_area));
	daughter1->SetChemical(7, daughter1->GetChemical(7) * (area1 / tot_area));
	daughter2->SetChemical(7, daughter2->GetChemical(7) * (area2 / tot_area));
	daughter1->SetChemical(8, daughter1->GetChemical(8) * (area1 / tot_area));
	daughter2->SetChemical(8, daughter2->GetChemical(8) * (area2 / tot_area));

	daughter1->SetTransporters(1, 1., 1.);
	daughter2->SetTransporters(1, 1., 1.);
}

} // namespace
} // namespace
