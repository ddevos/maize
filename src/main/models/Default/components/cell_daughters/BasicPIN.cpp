/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters::BasicPIN implementation file.
 */

#include "BasicPIN.h"

#include "bio/Cell.h"

using namespace std;
using namespace boost::property_tree;

namespace SimPT_Default {
namespace CellDaughters {

BasicPIN::BasicPIN(const CoreData& cd)
{
	Initialize(cd);
}

void BasicPIN::Initialize(const CoreData& cd)
{
	m_cd = cd;
	auto& p = m_cd.m_parameters;

	ptree const& arr_initval = p->get_child("auxin_transport.initval.value_array");
	m_x_initval = (++arr_initval.begin())->second.get_value<double>();
}

void BasicPIN::operator()(Cell* daughter1, Cell* daughter2)
{
	daughter1->SetChemical(1, m_x_initval);
	daughter2->SetChemical(1, m_x_initval);
}

} // namespace
} // namespace
