#ifndef DEFAULT_CELL_DAUGHTERS_BASICPIN_H_INCLUDED
#define DEFAULT_CELL_DAUGHTERS_BASICPIN_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CellDaughters::BasicPIN.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim {
class Cell;
}

namespace SimPT_Default {
namespace CellDaughters {

using namespace SimPT_Sim;

/**
 * Implements PIN division dynamics. After divisions, parent
 * and daughter cells get a standard stock of PINs.
 */
class BasicPIN
{
public:
	/// Initializing constructor.
	BasicPIN(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Cell* daughter1, Cell* daughter2);

private:
	CoreData   m_cd;           ///< Core data (mesh, params, sim_time,...).
	double     m_x_initval;    ///<
};

} // namespace
} // namespace

#endif // end_of_include_guard
