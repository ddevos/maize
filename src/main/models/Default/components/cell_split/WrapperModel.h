#ifndef DEFAULT_CELL_SPLIT_WRAPPER_MODEL_H_INCLUDED
#define DEFAULT_CELL_SPLIT_WRAPPER_MODEL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Wrapper model.
 */

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <tuple>

namespace SimPT_Sim {
class Cell;
struct CoreData;
}

namespace SimPT_Default {
namespace CellSplit {

using namespace SimPT_Sim;

/**
 * Geometry based cell housekeeping.
 */
class WrapperModel
{
public:
	/// Initializing constructor.
	WrapperModel(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Execute.
	std::tuple<bool, bool, std::array<double, 3>> operator()(Cell* cell);

private:
	std::function<double()>    m_uniform_generator;          ///< Generates uniform random between 0 and 1.
	double	                   m_cell_division_threshold;    ///<
};

} // namespace
} // namespace

#endif // end_of_include_guard
