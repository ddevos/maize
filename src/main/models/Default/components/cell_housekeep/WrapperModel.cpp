/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for Wrapper model.
 */

#include "WrapperModel.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellHousekeep {

using boost::property_tree::ptree;

WrapperModel::WrapperModel(const CoreData& cd)
{
	Initialize(cd);
}

void WrapperModel::Initialize(const CoreData& cd)
{
	m_cd = cd;

	const auto& p = m_cd.m_parameters->get_child("wrapper_model");
	m_ch0_threshold = p.get<double>("ch0_threshold");
	m_expansion_rate = p.get<double>("expansion_rate");
}

void WrapperModel::operator()(Cell* cell)
{
	const double t_area = cell->GetTargetArea();
	const double a_area = cell->GetArea();

	// Prevent the 8 upper cells from expanding. They are used as a boundary
	// layer and are not supposed to grow / divide akin to regular cells.
	if (cell->GetIndex() > 7) {
		if ((cell->GetChemical(0) / a_area) > m_ch0_threshold /*|| (c->Chemical(6) / c->Area()) > 0.5  */) {
			const double incr = m_expansion_rate * a_area; //Time step dependent
			const double update_t_area = t_area + incr;
			cell->SetTargetArea(update_t_area);
		}
	}
}

} // namespace
} // namespace
