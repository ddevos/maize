#ifndef DEFAULT_CELL_HOUSEKEEP_AUXIN_H_INCLUDED
#define DEFAULT_CELL_HOUSEKEEP_AUXIN_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep::Auxin header file.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim {
class Cell;
}

namespace SimPT_Default {
namespace CellHousekeep {

using namespace SimPT_Sim;

/**
 * Implements Auxin cell housekeeping.
 */
class Auxin
{
public:
	/// Initializing constructor.
	Auxin(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Cell* cell);

private:
	CoreData     m_cd;                    ///< Core data (mesh, params, sim_time,...).
	double       m_cell_base_area;
	double       m_cell_expansion_rate;
	double       m_division_ratio;
	double       m_elastic_modulus;
	double       m_response_time;
	double       m_time_step;
	double       m_viscosity_const;

private:
	double       m_area_incr;
	double       m_div_area;
};

} // namespace
} // namespace

#endif // end_of_include_guard
