#ifndef DEFAULT_TIME_EVOLVER_VPTISSUE_H_INCLUDED
#define DEFAULT_TIME_EVOLVER_VPTISSUE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SimPT time evolver.
 */

#include "sim/CoreData.h"
#include "sim/SimPhase.h"
#include "sim/SimTimingTraits.h"

namespace SimPT_Default  {
namespace TimeEvolver {

using namespace SimPT_Sim;

using boost::property_tree::ptree;
class Sim;

/**
 * Composite time step: ReactionAndTransport, then Grow and then Housekeep.
 */
class VPTissue : public SimTimingTraits
{
public:
	/// Initializing constructor.
	VPTissue(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Take a time step.
	std::tuple<SimTimingTraits::CumulativeTimings, bool>
	operator()(double time_slice, SimPhase phase = SimPhase::NONE);

private:
	CoreData    m_cd;               ///< Core data (mesh, params, sim_time,...).
};

} // namespace
} // namespace

#endif // end-of-include-guard
