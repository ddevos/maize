/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time evolver for Housekeeping only.
 */

#include "Housekeep.h"

#include "algo/CellDivider.h"
#include "algo/CellHousekeeper.h"

#include <cassert>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default  {
namespace TimeEvolver {

using namespace std;

Housekeep::Housekeep(const CoreData& cd)
{
	Initialize(cd);
}

void Housekeep::Initialize(const CoreData& cd)
{
	assert( cd.Check() && "CoreData not ok.");
	m_cd = cd;
}

std::tuple<SimTimingTraits::CumulativeTimings, bool> Housekeep::operator()(double, SimPhase)
{
	// -------------------------------------------------------------------------------
	// Intro
	// -------------------------------------------------------------------------------
	Stopclock   chrono_hk("housekeeping", true);
	CumulativeTimings  timings;

	// -------------------------------------------------------------------------------
	// Cell division. Time does not increase during the division process.
	// -------------------------------------------------------------------------------
	CellDivider cell_divider(m_cd);
	const unsigned int div_count = cell_divider.DivideCells();
	const bool is_stationary = (div_count == 0U);

	// -------------------------------------------------------------------------------
	// Cell housekeeping. Time does not increase during the division process.
	// -------------------------------------------------------------------------------
	CellHousekeeper  cell_housekeeper;
	cell_housekeeper.Initialize(m_cd);
	cell_housekeeper.Exec();

	// -------------------------------------------------------------------------------
	// We are done ...
	// -------------------------------------------------------------------------------
	timings.Record(chrono_hk.GetName(), chrono_hk.Get());
	return make_tuple(timings, is_stationary);
}

} // namespace
} // namespace
