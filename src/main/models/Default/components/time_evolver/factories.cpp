/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * TimeEvolver component factory map.
 */

#include "factories.h"

#include "Grow.h"
#include "Housekeep.h"
#include "HousekeepGrow.h"
#include "Transport.h"
#include "VLeaf.h"
#include "VPTissue.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

using namespace std;

namespace SimPT_Default {
namespace TimeEvolver {

const typename ComponentTraits<TimeEvolverTag>::MapType g_component_factories
{{
        make_pair("Grow",            boost::value_factory<Grow>()),
        make_pair("Housekeep",       boost::value_factory<Housekeep>()),
        make_pair("HousekeepGrow",   boost::value_factory<HousekeepGrow>()),
        make_pair("Transport",       boost::value_factory<Transport>()),
        make_pair("VLeaf",           boost::value_factory<VLeaf>()),
        make_pair("VPTissue",        boost::value_factory<VPTissue>())
}};

} // namespace
} // namespace
