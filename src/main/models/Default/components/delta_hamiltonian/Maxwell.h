#ifndef SIMPT_DEFAULT_DELTA_HAMILTONIAN_MAXWELL_H_INCLUDED
#define SIMPT_DEFAULT_DELTA_HAMILTONIAN_MAXWELL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for DeltaHamiltonian::Maxwell.
 * @author Abdiravuf Dzhurakhalov.
 */

#include <array>

namespace SimPT_Sim {
struct CoreData;
class Mesh;
class NeighborNodes;
class Node;
}

namespace SimPT_Default {
namespace DeltaHamiltonian {

using namespace SimPT_Sim;
/**
 * Maxwell DeltaHamiltonian function.
 */
class Maxwell
{
public:
	/// Initializing constructor.
	Maxwell(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Evaluation.
	double operator()(const NeighborNodes&, Node*, std::array<double, 3>);

private:
	double     m_elastic_modulus;               ///<
	double     m_lambda_alignment;              ///<
	double     m_lambda_bend;                   ///<
	double     m_lambda_cell_length;            ///<
	double     m_lambda_length;                 ///<
	Mesh*      m_mesh;                          ///<
	double     m_rp_stiffness;                  ///<
	double     m_target_node_distance;          ///<
};

} // namespace
} // namespace

#endif // end-of-include-guard
