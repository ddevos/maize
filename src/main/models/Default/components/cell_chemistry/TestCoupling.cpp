/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry component for the TestCoupling model.
 */

#include "TestCoupling.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"

using namespace std;
using namespace SimPT_Sim::Util;
using boost::property_tree::ptree;

namespace SimPT_Default {
namespace CellChemistry {

TestCoupling::TestCoupling(const CoreData& cd)
{
	Initialize(cd);
}

void TestCoupling::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p           = m_cd.m_parameters->get_child("test_coupling");
        m_ch0_production        = p.get<double>("ch0_production");
        m_ch0_breakdown	  	= p.get<double>("ch0_breakdown");
        m_ch1_production        = p.get<double>("ch1_production");
        m_ch1_breakdown	  	= p.get<double>("ch1_breakdown");
}

void TestCoupling::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// auxin and cytokinin dynamics

	const double chem0      = cell->GetChemical(0);
	const double chem1      = cell->GetChemical(1);

	dchem[0] = m_ch0_production - m_ch0_breakdown * chem0; //LEVELS
	dchem[1] = m_ch1_production - m_ch1_breakdown * chem1; //LEVELS
}

} // namespace
} // namespace
