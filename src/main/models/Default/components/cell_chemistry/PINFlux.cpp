/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry::PINFlux implementation file.
 */

#include "PINFlux.h"

#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"

#include <functional>
using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Default {
namespace CellChemistry {

PINFlux::PINFlux(const CoreData& cd)
{
	Initialize(cd);
}

void PINFlux::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters->get_child("auxin_transport");

        m_k1 	= p.get<double>("k1");
        m_k2 	= p.get<double>("k2");
        m_km 	= p.get<double>("km");
        m_kr 	= p.get<double>("kr");
        m_r  	= p.get<double>("r");
}

void PINFlux::operator()(Cell* cell, double* dchem)
{
	// sum all incoming fluxes of PINs
	dchem[1] = -SumFluxFromWalls(cell);
}

double PINFlux::SumFluxFromWalls(Cell* cell)
{
	using std::placeholders::_1;
	using std::placeholders::_2;
	using std::placeholders::_3;

	function<double(Cell*, Cell*, Wall*)> f = bind(&PINFlux::Flux, this, _1, _2, _3);
	double const sum = ReduceCellWalls<double>(cell, f);
	return sum;
}

double PINFlux::Flux(Cell* this_cell, Cell* adjacent_cell, Wall* w)
{
	// PIN1 localization at wall
	// Note: chemical 0 is Auxin (intracellular storage only)
	//  PIN1 is Chemical 1 (both in walls and intracellular storage)
	//! \f$ \frac{d Pij/dt}{dt} = k_1 A_j \frac{P_i}{L_ij} - k_2 P_{ij} \f$
	// Note that Pij is measured in term of concentration (mol/L)
	// Pi in terms of quantity (mol)
	// Equations as in Merks et al., Trends in Plant Science 2007

	// calculate PIN translocation rate from cell to membrane
	double const adj_auxin = adjacent_cell->GetChemical(0);
	double const receptor = adj_auxin * m_r / (m_kr + adj_auxin);

	// pick the correct side of the Wall
	double pin_atwall;
	if (w->GetC1() == this_cell) {
		pin_atwall = w->GetTransporters1(1);
	} else {
		pin_atwall = w->GetTransporters2(1);
	}

	// note: pin_flux is net flux from endosome to wall
	double const chem = this_cell->GetChemical(1);
	double const pin_flux = m_k1 * chem * receptor / (m_km + chem) - m_k2 * pin_atwall;
	return pin_flux;
}

} // namespace
} // namespace
