/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * EnabledActions implementation.
 */

#include "EnabledActions.h"

#include <QAction>
#include <QMenu>

using namespace std;

namespace SimShell {
namespace Gui {

void EnabledActions::Add(QAction* a)
{
	actions.push_back(a);
}

void EnabledActions::Add(QMenu* m)
{
	menus.push_back(m);
}

void EnabledActions::Set(vector<QAction*> a)
{
	actions = move(a);
}

void EnabledActions::Enable()
{
	for (auto a : actions) a->setEnabled(true);
	for (auto m : menus) m->setEnabled(true);
}

void EnabledActions::Disable()
{
	for (auto a : actions) a->setEnabled(false);
	for (auto m : menus) m->setEnabled(false);
}

} // namespace Gui
} // namespace SimShell
