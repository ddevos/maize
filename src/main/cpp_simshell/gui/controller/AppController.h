#ifndef GUI_APPCONTROLLER_H_INCLUDED
#define GUI_APPCONTROLLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * AppController header.
 */

#include "WorkspaceController.h"

#include "gui/factory/IFactory.h"
#include "gui/IHasPTreeState.h"
#include "gui/LogWindow.h"
#include "util/clock_man/Timeable.h"

#include <QMainWindow>
#include <QSettings>
#include <memory>

class QCloseEvent;

namespace SimShell {
namespace Gui {
namespace Controller {

/**
 * Main window of GUI.
 * Owner of 1 WorkspaceController instance.
 */
class AppController :  public QMainWindow,
                       public IHasPTreeState,
                       public SimPT_Sim::ClockMan::Timeable<>
{
	Q_OBJECT
public:
	AppController(const std::shared_ptr<Gui::IFactory>&,
	               QWidget* parent = 0);

	virtual ~AppController();

	/// @see IHasPTreeState
	virtual boost::property_tree::ptree GetPTreeState() const;

	/// @see SimPT_Sim::ClockMan::Timeable
	virtual Timings GetTimings() const;

	void Log(const std::string&);

	/// @see IHasPTreeState
	virtual void SetPTreeState(const boost::property_tree::ptree&);

protected:
	virtual void closeEvent(QCloseEvent*);

private slots:
	void SLOT_AboutDialog();
	void SLOT_WorkspaceWizard();

private:
	void InitMenu();
	void InitProject();
	void InitWidgets();
	void InitWorkspace();

	void SetWorkspaceWindowTitle(const std::string & path);

private:
	std::shared_ptr<Gui::IFactory>              m_factory;

	QSettings                                   m_settings;

	// Widgets
	LogWindow*                                  m_log_dock;

	// Sub-state
	WorkspaceController                         m_workspace_controller;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
