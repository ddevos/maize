/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTree undo commands.
 */

#include "PTreeUndoCommands.h"

#include <boost/property_tree/ptree.hpp>

namespace SimShell {
namespace Gui {

using boost::property_tree::ptree;

PTreeModel::EditKeyCommand::EditKeyCommand(PTreeModel* m, Item* it, QModelIndex const & in,
		QVariant const & ov, QVariant const & nv)
	: model(m), item(it), index(in), old_value(ov), new_value(nv)
{
	setText("Edit");
}

PTreeModel::EditKeyCommand::~EditKeyCommand()
{
}

void PTreeModel::EditKeyCommand::undo()
{
	item->key = old_value;
	emit model->dataChanged(index, index);
}

void PTreeModel::EditKeyCommand::redo()
{
	item->key = new_value;
	emit model->dataChanged(index, index);
}

PTreeModel::EditDataCommand::EditDataCommand(PTreeModel* m, Item* it, QModelIndex const & in,
		QVariant const & ov, QVariant const & nv)
	: model(m), item(it), index(in), old_value(ov), new_value(nv)
{
	setText("Edit");
}

PTreeModel::EditDataCommand::~EditDataCommand()
{
}

void PTreeModel::EditDataCommand::undo()
{
	item->data = old_value;
	emit model->dataChanged(index, index);
}

void PTreeModel::EditDataCommand::redo()
{
	item->data = new_value;
	emit model->dataChanged(index, index);
}

PTreeModel::InsertRowsCommand::InsertRowsCommand(PTreeModel* m, Item* p, QModelIndex const & pi, int r, int c)
	: model(m), parent(p), parent_index(pi), row(r), count(c), undone(false)
{
	items = new Item*[count];
	for (int i = 0; i < count; i++) {
		items[i] = new Item(parent, "(empty)", i + row);
	}

setText("Insert");
}

PTreeModel::InsertRowsCommand::InsertRowsCommand(PTreeModel* m, Item* p,
		QModelIndex const & pi, int r, ptree const & tree)
	: model(m), parent(p), parent_index(pi), row(r), count(tree.size()), undone(false)
{
	items = new Item*[count];
	int i = 0;
	for (ptree::const_iterator it = tree.begin(); it != tree.end(); it++) {
		items[i] = new Item(parent, it->first.c_str(), i + row);
		items[i]->Load(it->second);
		i++;
	}
	setText("Insert");
}

PTreeModel::InsertRowsCommand::~InsertRowsCommand()
{
	// only delete created rows if this command was undone.
	if (undone) {
		for (int i = 0; i < count; i++) {
			delete items[i];
		}
	}
	delete[] items;
}

void PTreeModel::InsertRowsCommand::undo()
{
	model->beginRemoveRows(parent_index, row, row + count - 1);
	for (int i = 0; i < count; i++) {
		parent->RemoveChild(row);
	}
	undone = true;
	model->endRemoveRows();
}

void PTreeModel::InsertRowsCommand::redo()
{
	undone = false;
	model->beginInsertRows(parent_index, row, row + count - 1);
	for (int i = 0; i < count; i++) {
		parent->InsertChild(row + i, items[i]);
	}
	model->endInsertRows();
}

PTreeModel::RemoveRowsCommand::RemoveRowsCommand(PTreeModel* m, Item* p, QModelIndex const & pi, int r, int c)
	: model(m), parent(p), parent_index(pi), row(r), count(c), undone(false)
{
	items = new Item*[count];
	for (int i = 0; i < count; i++) {
		items[i] = parent->GetChild(i + row);
	}
	setText("Remove");
}

PTreeModel::RemoveRowsCommand::~RemoveRowsCommand()
{
	// only delete removed rows if command was not undone.
	if (!undone) {
		for (int i = 0; i < count; i++) {
			delete items[i];
		}
	}
	delete[] items;
}

void PTreeModel::RemoveRowsCommand::undo()
{
	undone = true;
	model->beginInsertRows(parent_index, row, row + count - 1);
	for (int i = 0; i < count; i++) {
		parent->InsertChild(row + i, items[i]);
	}
	model->endInsertRows();
}

void PTreeModel::RemoveRowsCommand::redo()
{
	model->beginRemoveRows(parent_index, row, row + count - 1);
	for (int i = 0; i < count; i++) {
		parent->RemoveChild(row);
	}
	undone = false;
	model->endRemoveRows();
}

PTreeModel::MoveRowsCommand::MoveRowsCommand(PTreeModel* m, Item* op, QModelIndex const & opi,
		int r, Item* np, QModelIndex const & npi, int nr, int c)
	: model(m), old_parent(op), new_parent(np), old_parent_index(opi), new_parent_index(npi),
	  old_row(r), new_row(nr), count(c)
{
	items = new Item*[count];
	for (int i = 0; i < count; i++) {
		items[i] = old_parent->GetChild(old_row + i);
	}
	setText("Move");
}

PTreeModel::MoveRowsCommand::~MoveRowsCommand()
{
	// delete array, don't delete individual items.
	delete[] items;
}

void PTreeModel::MoveRowsCommand::undo()
{
	model->beginRemoveRows(new_parent_index, new_row, new_row + count - 1);
	for (int i = 0; i < count; i++) {
		new_parent->RemoveChild(new_row + i);
	}
	model->endRemoveRows();

	model->beginInsertRows(old_parent_index, old_row, old_row + count - 1);
	for (int i = 0; i < count; i++) {
		items[i]->row = old_row + i;
		old_parent->InsertChild(old_row + i, items[i]);
	}
	model->endInsertRows();
}

void PTreeModel::MoveRowsCommand::redo()
{
	// TODO: try this out:
	// model->beginMoveRows(old_parent_index, old_row, old_row+count-1, new_parent_index, new_row);
	model->beginRemoveRows(old_parent_index, old_row, old_row + count - 1);
	for (int i = 0; i < count; i++) {
		old_parent->RemoveChild(old_row + i);
	}
	model->endRemoveRows();

	model->beginInsertRows(new_parent_index, new_row, new_row + count - 1);
	for (int i = 0; i < count; i++) {
		items[i]->row = new_row + i;
		new_parent->InsertChild(new_row + i, items[i]);
	}
	model->endInsertRows();
	// model->endMoveRows();
}

} // end of namespace Gui
} // end of namespace SimShell
