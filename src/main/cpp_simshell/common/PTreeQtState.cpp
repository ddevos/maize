/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreeQtState.
 */

#include "PTreeQtState.h"

#include <functional>
#include <QDockWidget>
#include <QTreeView>
#include <QWidget>

using namespace std;
using namespace boost::property_tree;

namespace SimShell {

namespace {
	string& escape_path(string& s) {
		for (auto& c : s)
			if (c == '.') c = '_';
		return s;
	}
}

ptree PTreeQtState::GetTreeViewState(const QTreeView* view)
{
	function<ptree(const QModelIndex&)> get_state_recursive;
	get_state_recursive = [&](const QModelIndex& root_index)
		-> ptree
	{
		ptree result;
		if (view->model()->index(0,0,root_index).isValid()) {
			result.put("expanded", view->isExpanded(root_index));
			const int column = 0;
			for (int row = 0; true; row++) {
				auto child_index = view->model()->index(row, column, root_index);
				if (!child_index.isValid())
					break;
				auto child_name  = child_index.data().toString().toStdString();
				auto child_path  = ptree::path_type(escape_path(child_name));
				auto child_pt    = get_state_recursive(child_index);
				if (!child_pt.empty())
					result.add_child(ptree::path_type("children") / child_path, child_pt);
			}
		}
		return result;
	};

	ptree result;
	result.put_child("items_state", get_state_recursive(view->rootIndex()));
	return result;
}

void PTreeQtState::SetTreeViewState(QTreeView* view, const ptree& state)
{
	function<void(const QModelIndex&, const ptree&)> set_state_recursive;
	set_state_recursive = [&](const QModelIndex& root_index, const ptree& state)
	{
		auto expanded_optional = state.get_optional<bool>("expanded");
		if (expanded_optional) {
			view->setExpanded(root_index, expanded_optional.get());
			const int column = 0;
			for (int row = 0; true; row++) {
				auto child_index = view->model()->index(row, column, root_index);
				if (!child_index.isValid())
					break;
				auto child_name  = child_index.data().toString().toStdString();
				auto child_path  = ptree::path_type(escape_path(child_name));
				auto child_pt_optional = state.get_child_optional(ptree::path_type("children") / child_path);
				if (child_pt_optional) {
					set_state_recursive(child_index, child_pt_optional.get());
				}
			}
		}
	};
	set_state_recursive(view->rootIndex(), state.get_child("items_state"));
}

ptree PTreeQtState::GetWidgetState(const QWidget* widget)
{
	ptree result;
	auto g = widget->geometry();
	result.put("enabled", widget->isEnabled());
	result.put("visible", widget->isVisible());
	result.put("x", g.x());
	result.put("y", g.y());
	result.put("width", g.width());
	result.put("height", g.height());
	return result;
}

void PTreeQtState::SetWidgetState(QWidget* widget, const ptree& state)
{
	try {
		widget->setEnabled(state.get<bool>("enabled"));
		widget->setVisible(state.get<bool>("visible"));
		auto x = state.get<int>("x");
		auto y = state.get<int>("y");
		auto width = state.get<int>("width");
		auto height = state.get<int>("height");
		widget->setGeometry(x, y, width, height);
	}
	// Ignore non-existing values.
	catch (ptree_bad_path&) {}
	catch (ptree_bad_data&) {}
}

ptree PTreeQtState::GetDockWidgetState(const QDockWidget* dock)
{
	ptree result;
	result.put("floating", dock->isFloating());
	return result;
}

void PTreeQtState::SetDockWidgetState(QDockWidget* dock, const ptree& state)
{
	try {
		dock->setFloating(state.get<bool>("floating"));
	}
	// Ignore non-existing values.
	catch (ptree_bad_path&) {}
	catch (ptree_bad_data&) {}
}

const map<Qt::DockWidgetArea, string> PTreeQtState::g_dock_widget_area_to_string({
	{Qt::LeftDockWidgetArea, "left"},
	{Qt::RightDockWidgetArea, "right"},
	{Qt::TopDockWidgetArea, "top"},
	{Qt::BottomDockWidgetArea, "bottom"}
});

const map<string, Qt::DockWidgetArea> PTreeQtState::g_string_to_dock_widget_area({
	{"left", Qt::LeftDockWidgetArea},
	{"right", Qt::RightDockWidgetArea},
	{"top", Qt::TopDockWidgetArea},
	{"bottom", Qt::BottomDockWidgetArea}
});

ptree PTreeQtState::GetDockWidgetArea(const QMainWindow* window, QDockWidget* dock)
{
	ptree result;
	auto it = g_dock_widget_area_to_string.find(window->dockWidgetArea(dock));
	if (it != g_dock_widget_area_to_string.end()) {
		result.put("area", it->second);
	}
	return result;
}

void PTreeQtState::SetDockWidgetArea(QMainWindow* window, QDockWidget* dock, const ptree& state)
{
	auto value = state.get_optional<string>("area");
	if (value) {
		auto it = g_string_to_dock_widget_area.find(value.get());
		if (it != g_string_to_dock_widget_area.end()) {
			window->addDockWidget(it->second, dock);
		}
	}
}

} // namespace SimShell
