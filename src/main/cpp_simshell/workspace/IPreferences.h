#ifndef WS_IPREFERENCES_H_INCLUDED
#define WS_IPREFERENCES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IPreferences.
 */

#include "event/PreferencesChanged.h"
#include "util/misc/Subject.h"

#include <boost/property_tree/ptree.hpp>

namespace SimShell {
namespace Ws {

/**
 * Interface expressing the ability of an object to have a ptree of preferences stored in it.
 */
class IPreferences : public SimPT_Sim::Util::Subject<Event::PreferencesChanged, std::weak_ptr<const void>>
{
public:
	virtual ~IPreferences() {}

	virtual const boost::property_tree::ptree& GetPreferences() const = 0;

	virtual void SetPreferences(const boost::property_tree::ptree&) = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
