#ifndef WS_EVENT_WORKSPACECHANGED_H_INCLUDED
#define WS_EVENT_WORKSPACECHANGED_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition for WorkspaceChanged.
 */

namespace SimShell {
namespace Ws {
namespace Event {

/**
 * Event used to inform some observer that the workspace has changed.
 *
 * Really a POD but packaged to force users to initialize all data members.
 */
class WorkspaceChanged
{
public:
	enum Type {
		ProjectAdded,
		ProjectRenamed,
		ProjectRemoved
	};

	WorkspaceChanged(Type type, const std::string& name)
		: m_name(name), m_old_name(""), m_type(type) {}

	WorkspaceChanged(const std::string& old_name, const std::string& name)
		: m_name(name), m_old_name(old_name), m_type(ProjectRenamed) {}

	~WorkspaceChanged() {}

	/// Get type.
	Type GetType() const { return m_type;}

	/// In case type is ProjectAdded or ProjectRemoved, get name of project.
	/// In case type is not ProjectAdded and not ProjectRemoved, behavior is undefined.
	const std::string GetName() const
	{
		return (m_type==Type::ProjectAdded || m_type==Type::ProjectRemoved) ? m_name : "";
	}

	/// In case type is ProjectRenamed, get old name of renamed project.
	/// If type is not ProjectRenamed, behavior is undefined.
	const std::string GetOldName() const
	{
		return (m_type==Type::ProjectRenamed) ? m_old_name : "";
	}

	/// In case type is ProjectRenamed, get new name of renamed project.
	/// If type is not ProjectRenamed, behavior is undefined.
	const std::string GetNewName() const
	{
		return (m_type==Type::ProjectRenamed) ? m_name : "";
	}

private:
	std::string    m_name;
	std::string    m_old_name;
	Type           m_type;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
