#ifndef WS_IUSERDATE_H_INCLUDED
#define WS_IUSERDATE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IUserData.
 */

namespace SimShell {
namespace Ws {

/**
 * Interface that expresses the ability to have user data, i.e. any data
 * that one wishes to be stored for a lifetime larger than the application's.
 */
class IUserData
{
public:
	virtual ~IUserData() {}

	virtual const boost::property_tree::ptree& GetUserData(const std::string& user) const = 0;

	virtual void SetUserData(const std::string& user, const boost::property_tree::ptree&) = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
