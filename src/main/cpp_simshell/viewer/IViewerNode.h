#ifndef VIEWER_IVIEWERNODE_H_INCLUDED
#define VIEWER_IVIEWERNODE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for IViewerNode.
 */
#include "event/ViewerEvent.h"
#include "util/misc/Subject.h"

namespace SimShell {
namespace Viewer {

/**
 * Interface for a node in a hierarchical tree of viewers.
 * A viewer node can be enabled and disabled at runtime.
 */
class IViewerNode : public SimPT_Sim::Util::Subject<Event::ViewerEvent, std::weak_ptr<const void>>
{
public:
	typedef std::map<std::string, std::shared_ptr<IViewerNode>> ChildrenMap;

	/// Virtual destructor
	virtual ~IViewerNode() {}

	/// Disable node.
	virtual void Disable() = 0;

	/// Enable node.
	virtual void Enable() = 0;

	/// Test whether node is enabled.
	/// Note that an enabled node still won't do anything if its parent is disabled.
	/// @see IViewerNodeWithParent::IsParentEnabled
	virtual bool IsEnabled() const = 0;

	/// Test whether parent node is enabled.
	virtual bool IsParentEnabled() const = 0;

	/// Get iterator pointing to first child.
	virtual ChildrenMap::const_iterator begin() const = 0;

	/// Get iterator pointing to one position after last child.
	virtual ChildrenMap::const_iterator end() const = 0;
};

/**
 * A viewer node with a parent.
 */
template <class SubjectType>
class IViewerNodeWithParent : public IViewerNode
{
public:
	/// Virtual destructor
	virtual ~IViewerNodeWithParent() {}

	/// Enable entire subtree.
	virtual void ParentDisabled() = 0;

	/// Disable entire subtree.
	virtual void ParentEnabled(std::shared_ptr<SubjectType>) = 0;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
