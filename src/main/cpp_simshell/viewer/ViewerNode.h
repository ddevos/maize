#ifndef VIEWER_VIEWERNODE_H_INCLUDED
#define VIEWER_VIEWERNODE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ViewerNode.
 */

#include "IViewerNode.h"
#include "gui/controller/AppController.h"
#include "workspace/MergedPreferences.h"

#include <iostream>
#include <type_traits>

class QWidget;

namespace SimShell {
namespace Viewer {

template <class ViewerType>
struct viewer_is_subject {
	static const bool value = false;
};

template <class ViewerTYpe>
struct viewer_is_widget {
	static const bool value = false;
};

/**
 * Template implementing a node in a hierarchical tree of viewers.Parameters:
 * - ViewerType : Type of viewer to be dynamically created/destroyed upon being enabled/disabled.
 * - SubjectType: Type of subject the (dynamically created/destroyed) viewer is observing.
 */
template <class ViewerType, class SubjectType>
class ViewerNode : public IViewerNodeWithParent<SubjectType>,
                   public std::enable_shared_from_this<ViewerNode<ViewerType, SubjectType>>
{
public:
	ViewerNode(std::shared_ptr<Ws::MergedPreferences> p,
	           IViewerNode::ChildrenMap&& c,
	           Gui::Controller::AppController* app = nullptr);

	ViewerNode(std::shared_ptr<Ws::MergedPreferences> p,
	           Gui::Controller::AppController* app = nullptr);

	/// Virtual destructor.
	virtual ~ViewerNode() {}

	/// @see IViewerNode
	virtual void Disable();

	/// @see IViewerNode
	virtual void Enable();

	/// @see IViewerNode
	virtual bool IsEnabled() const;

	/// @see IViewerNodeWithParent
	virtual bool IsParentEnabled() const;

	/// @see IViewerNode
	virtual IViewerNode::ChildrenMap::const_iterator begin() const;

	/// @see IViewerNode
	virtual IViewerNode::ChildrenMap::const_iterator end() const;

protected:
	/// @see IViewerNodeWithParent
	virtual void ParentDisabled();

	/// @see IViewerNodeWithParent
	virtual void ParentEnabled(std::shared_ptr<SubjectType>);

private:
	void Check();

	template <class T = ViewerType>
	typename std::enable_if<!viewer_is_widget<T>::value, std::shared_ptr<T>>::type CreateViewer();

	template <class T = ViewerType>
	typename std::enable_if<viewer_is_widget<T>::value, std::shared_ptr<T>>::type CreateViewer();

private:
	std::shared_ptr<Ws::MergedPreferences>  m_preferences;     ///< Preferences.
	std::shared_ptr<SubjectType>            m_subject;         ///< The subject to register viewer with on Enable().
	IViewerNode::ChildrenMap                m_children;        ///< Children viewers, observers of our own viewer.
	bool                                    m_enabled;         ///< Whether viewer is enabled.
	bool                                    m_parent_enabled;  ///< Only true if all parents are enabled.
	std::shared_ptr<ViewerType>             m_viewer;          ///< Associated viewer object.
	Gui::Controller::AppController*         m_app;
};

template <class ViewerType, class SubjectType>
ViewerNode<ViewerType, SubjectType>::ViewerNode(std::shared_ptr<Ws::MergedPreferences> p,
                                                IViewerNode::ChildrenMap&& c,
                                                Gui::Controller::AppController* a)
	: m_preferences(p),
	  m_children(c),
	  m_enabled(m_preferences->Get<bool>("enabled_at_startup")),
	  m_parent_enabled(false),
	  m_app(a)
{
}

template <class ViewerType, class SubjectType>
ViewerNode<ViewerType, SubjectType>::ViewerNode(std::shared_ptr<Ws::MergedPreferences> p,
						Gui::Controller::AppController* a)
	: m_preferences(p),
	  m_enabled(m_preferences->Get<bool>("enabled_at_startup")),
	  m_parent_enabled(false),
	  m_app(a)
{
}

template <bool enabled>
struct Register
{
	template <class ViewerType>
	static void Do(std::shared_ptr<ViewerType>, IViewerNode::ChildrenMap&) {}

	template <class ViewerType>
	static void Undo(std::shared_ptr<ViewerType> , IViewerNode::ChildrenMap& ) {}
};

template <>
struct Register<true>
{
	template <class ViewerType>
	static void Do(std::shared_ptr<ViewerType> v, IViewerNode::ChildrenMap& m)
	{
		for (auto& child : m) {
			std::static_pointer_cast<IViewerNodeWithParent<ViewerType>>(child.second)->ParentEnabled(v);
		}
	}

	template <class ViewerType>
	static void Undo(std::shared_ptr<ViewerType> , IViewerNode::ChildrenMap& m)
	{
		for (auto& child : m) {
			std::static_pointer_cast<IViewerNodeWithParent<ViewerType>>(child.second)->ParentDisabled();
		}
	}
};

template <class ViewerType, class SubjectType>
template <class T>
typename std::enable_if<!viewer_is_widget<T>::value, std::shared_ptr<T>>::type
ViewerNode<ViewerType, SubjectType>::CreateViewer()
{
	return std::make_shared<T>(m_preferences);
}

template <class ViewerType, class SubjectType>
template <class T>
typename std::enable_if<viewer_is_widget<T>::value, std::shared_ptr<T>>::type
ViewerNode<ViewerType, SubjectType>::CreateViewer()
{
	if (m_app) {
		return std::make_shared<ViewerType>(m_preferences, m_app, [this](){ this->Disable();});
	} else {
		return nullptr;
	}
}

// Wrapped in struct because we want to partially specialize it.
template <class ViewerType, class SubjectType>
struct InitNotifier
{
	/// Try to initialize viewer with "Initialized" type of event.
	template <typename V = ViewerType, typename S = SubjectType,
		typename S::EventType::Type init_type = S::EventType::Type::Initialized>
	static void Do(std::shared_ptr<V> v, typename S::EventType::Source s)
	{
		//std::cout << "InitNotify - version that does something - called" << std::endl;
		v->Update(typename SubjectType::EventType({s, 0, init_type}));
	}

	template <typename T, typename U>
	static void Do(T, U)
	{
		//std::cout << "InitNotify - version that does nothing - called" << std::endl;
	}
};

template <class ViewerType, class SubjectType>
void ViewerNode<ViewerType, SubjectType>::Check()
{
	if (m_enabled && m_parent_enabled)
	{
		// Create viewer
		if (!m_viewer && (m_viewer = CreateViewer())) {
			//m_viewer = std::make_shared<ViewerType>(m_preferences);
			if (m_subject) {
				typedef typename SubjectType::EventType EventType;
				auto handler = std::bind(&ViewerType::template Update<EventType>, m_viewer.get(), std::placeholders::_1);
				m_subject->Register(m_viewer, handler);
				InitNotifier<ViewerType, SubjectType>::Do(m_viewer, m_subject);
			}

			// Enable children after this is enabled.
			Register<viewer_is_subject<ViewerType>::value>::Do(m_viewer, m_children);
		}
	} else {
		if (m_viewer) {
			// Disable children before this is disabled.
			Register<viewer_is_subject<ViewerType>::value>::Undo(m_viewer, m_children);

			// Disable viewer
			if (m_subject) {
				m_subject->Unregister(m_viewer);
			}
			m_viewer.reset();
		}
	}
}

template <class ViewerType, class SubjectType>
void ViewerNode<ViewerType, SubjectType>::Disable()
{
	m_enabled = false;
	m_preferences->Put("enabled_at_startup", false);
	Check();
	IViewerNode::Notify({ ViewerNode<ViewerType, SubjectType>::shared_from_this(), Event::ViewerEvent::Disabled });
}

template <class ViewerType, class SubjectType>
void ViewerNode<ViewerType, SubjectType>::Enable()
{
	m_enabled = true;
	m_preferences->Put("enabled_at_startup", true);
	Check();
	IViewerNode::Notify({ ViewerNode<ViewerType, SubjectType>::shared_from_this(), Event::ViewerEvent::Enabled });
}

template <class ViewerType, class SubjectType>
bool ViewerNode<ViewerType, SubjectType>::IsEnabled() const
{
	return m_enabled;
}

template <class ViewerType, class SubjectType>
bool ViewerNode<ViewerType, SubjectType>::IsParentEnabled() const
{
	return m_parent_enabled;
}

template <class ViewerType, class SubjectType>
IViewerNode::ChildrenMap::const_iterator ViewerNode<ViewerType, SubjectType>::begin() const
{
	return m_children.begin();
}

template <class ViewerType, class SubjectType>
IViewerNode::ChildrenMap::const_iterator ViewerNode<ViewerType, SubjectType>::end() const
{
	return m_children.end();
}

template <class ViewerType, class SubjectType>
void ViewerNode<ViewerType, SubjectType>::ParentEnabled(std::shared_ptr<SubjectType> s)
{
	m_subject = s;
	m_parent_enabled = true;
	Check();
}

template <class ViewerType, class SubjectType>
void ViewerNode<ViewerType, SubjectType>::ParentDisabled()
{
	m_parent_enabled = false;
	Check();
}

} // namespace
} // namespace

#endif // end_of_inclde_guard
