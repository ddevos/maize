#ifndef SIMPT_EDITOR_REGULAR_TILING_H_INCLUDED
#define SIMPT_EDITOR_REGULAR_TILING_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for RegularTiling.
 */

#include <QGraphicsItem>

#include <list>
#include <utility>

namespace SimPT_Editor {

class Tile;

/**
 * Class of a cell tiling with regular polygons
 */
class RegularTiling : public QGraphicsItemGroup
{
public:
	/**
	 * Constructor
	 *
	 * @param	seed	The initial tile to start with (the tiling takes ownership)
	 * @param	rect	The initial rectangle (see SetRectangle())
	 * @param	parent	The QGraphicsItem parent
	 */
	RegularTiling(Tile *seed, const QRectF &rect, QGraphicsItem *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~RegularTiling();


	/**
	 * The rectangle the tiling should overlay
	 *
	 * @param	rect	The rectangle (in the coordinates of the parent of this item)
	 */
	void SetRectangle(const QRectF &rect);

	/**
	 * Get the polygons associated with this tiling.
	 *
	 * @return	The polygons of all the tiles mapped to the coordinate system of the parent.
	 */
	std::list<QPolygonF> GetPolygons() const;

protected:
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
	void ResizeTileRow(std::list<Tile*> &row);
	void ResizeTiles();

	std::list<std::list<Tile*>> m_tiles;
	QRectF m_parent_rect;
	QRectF m_rect;
};

} // namespace

#endif // end-of-include-guard
