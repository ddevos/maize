/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for RegularTiling.
 */

#include "RegularTiling.h"

#include "tiles/Tile.h"

#include <cassert>
#include <QPen>

namespace SimPT_Editor {

RegularTiling::RegularTiling(Tile *tile, const QRectF &rect, QGraphicsItem *parent)
	: QGraphicsItemGroup(parent), m_tiles({ { tile } })
{
	tile->setParentItem(this);

	SetRectangle(rect);

	setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

RegularTiling::~RegularTiling()
{
}

void RegularTiling::SetRectangle(const QRectF &rect)
{
	m_parent_rect = rect;
	m_rect = mapRectFromParent(m_parent_rect);
	ResizeTiles();
}

std::list<QPolygonF> RegularTiling::GetPolygons() const
{
	std::list<QPolygonF> polygons;
	for (auto row : m_tiles) {
		for (Tile* tile : row) {
			polygons.push_back(mapToParent(tile->GetPolygon()));
		}
	}
	return polygons;
}

QVariant RegularTiling::itemChange(GraphicsItemChange change, const QVariant &value)
{
	if (change == QGraphicsItem::ItemTransformHasChanged || change == QGraphicsItem::ItemRotationHasChanged) {
		m_rect = mapRectFromParent(m_parent_rect);
		ResizeTiles();
	}

	return QGraphicsItem::itemChange(change, value);
}

void RegularTiling::ResizeTileRow(std::list<Tile*> &row)
{
	auto tile_rect = [this] (Tile *tile) -> QRectF { return mapRectFromItem(tile, tile->GetPolygon().boundingRect()); };

	// Extend left and right side (first extend, we don't want to lose all our tiles of this row!)
	while (tile_rect(row.front()).right() > m_rect.left()) {
		row.push_front(row.front()->Left());
	}
	while(tile_rect(row.back()).left() < m_rect.right()) {
		row.push_back(row.back()->Right());
	}

	// Shrink left and right side
	while(tile_rect(row.front()).right() < m_rect.left()) {
		delete row.front();
		row.pop_front();
	}
	while (tile_rect(row.back()).left() > m_rect.right()) {
		delete row.back();
		row.pop_back();
	}

	assert( !row.empty() && "Number of tiles on a row should stay greater or equal to 1");
}

void RegularTiling::ResizeTiles()
{
	auto tile_rect = [this] (Tile *tile) -> QRectF { return mapRectFromItem(tile, tile->GetPolygon().boundingRect()); };
	auto row_rect = [this, tile_rect] (const std::list<Tile*> &row) -> QRectF { QRectF rect = tile_rect(row.front()); for (Tile *tile : row) rect.united(tile_rect(tile)); return rect; };
	auto delete_row = [] (Tile *tile) -> bool { delete tile; return true; };

	for (auto &row : m_tiles) {
		ResizeTileRow(row);
	}

	// Extend up and down (first extend, we don't want to lose all our rows!)
	while (row_rect(m_tiles.front()).bottom() > m_rect.top()) {
		m_tiles.push_front({ m_tiles.front().front()->Up() });
		ResizeTileRow(m_tiles.front());
	}
	while(row_rect(m_tiles.back()).top() < m_rect.bottom()) {
		m_tiles.push_back({ m_tiles.back().front()->Down() });
		ResizeTileRow(m_tiles.back());
	}

	// Shrink up and down direction
	while(row_rect(m_tiles.front()).bottom() < m_rect.top()) {
		m_tiles.front().remove_if(delete_row);
		m_tiles.pop_front();
	}
	while (row_rect(m_tiles.back()).top() > m_rect.bottom()) {
		m_tiles.back().remove_if(delete_row);
		m_tiles.pop_back();
	}

	assert( !m_tiles.empty() && "Number of tile rows should stay greater or equal to 1");
}

} // namespace
