#ifndef SIMPT_EDITOR_TISSUE_SLICER_H_INCLUDED
#define SIMPT_EDITOR_TISSUE_SLICER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for TissueSlicer.
 */

#include "../editor/EditControlLogic.h"

#include "slicer/SliceItem.h"
#include <QGraphicsLineItem>
#include <QGraphicsScene>
#include <QObject>

namespace SimPT_Editor {

/**
 * Functionality and visual representation of slicing a cell complex.
 */
class TissueSlicer : public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 *
	 * @param	tissue			The tissue that will be sliced.
	 * @param	scene			The scene for the representation (no ownership will be passed).
	 */
	TissueSlicer(std::shared_ptr<EditControlLogic> tissue, QGraphicsScene* scene);

	/**
	 * Destructor.
	 */
	virtual ~TissueSlicer();

	/**
	 * Start the cut procedure by putting a starting point for the cut.
	 *
	 * @param	point			The first point of the cut.
	 */
	void StartCut(const QPointF& point);

	/**
	 * End the cut procedure by putting a second point for the cut.
	 *
	 * @param	point			The second point of the cut.
	 */
	void EndCut();

	/**
	 * This procedure is for dynamically moving the line in the canvas (the first point is fixed).
	 *
	 * @param	point			The new second point of the cut.
	 * @return	True if the cut can be moved (if the first point is different and the cut-action hasn't ended yet).
	 */
	bool MoveCut(const QPointF& point);

	/**
	 * Checks whether the cut-action has ended.
	 *
	 * @return	True if the cut-action has ended.
	 */
	bool CutEnded() const;

signals:
	/**
	 * Emitted when the slicing has finished.
	 * The object has to be deleted later.
	 */
	void Finished();

private slots:
	/**
	 * Finish the slicing action.
	 */
	void Finish();

private:
	std::shared_ptr<EditControlLogic>     m_tissue;
	QGraphicsScene*                       m_scene;
	QGraphicsLineItem*                    m_cut;
	std::list<SliceItem*>                 m_slices;
};

} // namespace

#endif // end_of_include_guard
