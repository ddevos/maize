/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for TissueSlicer.
 */

#include "editor/PolygonUtils.h"
#include "slicer/SliceItem.h"
#include "bio/Cell.h"
#include "bio/Node.h"
#include "TissueSlicer.h"

class QColor;
namespace SimPT_Shell { class EditControlLogic; }

namespace SimPT_Editor {

TissueSlicer::TissueSlicer(std::shared_ptr<EditControlLogic> tissue, QGraphicsScene* scene)
		: m_tissue(tissue), m_scene(scene), m_cut(nullptr) {}

TissueSlicer::~TissueSlicer()
{
	delete m_cut;
	for (auto slice : m_slices) {
		delete slice;
	}
}

void TissueSlicer::StartCut(const QPointF& point)
{
	m_cut = new QGraphicsLineItem(QLineF(point, point));
	m_cut->setZValue(0.5);
	m_cut->setPen(QPen(QBrush(QColor(0,0,255,255)), 0, Qt::DashLine));
	m_scene->addItem(m_cut);
}

void TissueSlicer::EndCut()
{
	QPolygonF boundaryPolygon;
	for (auto node : m_tissue->GetMesh()->GetMesh()->GetBoundaryPolygon()->GetNodes()) {
		boundaryPolygon.append(QPointF((*node)[0], (*node)[1]));
	}

	for (auto polygon : PolygonUtils::SlicePolygon(boundaryPolygon, m_cut->line())) {
		auto item = new SliceItem(polygon, m_cut->line(), m_tissue, m_scene);
		item->setZValue(0.6);
		connect(item, SIGNAL(Truncated()), this, SLOT(Finish()));
		m_slices.push_back(item);
		m_scene->addItem(item);
	}
}

bool TissueSlicer::MoveCut(const QPointF& point)
{
	if (m_slices.size() == 0 && point != m_cut->line().p1()) {
		// The cut-action hasn't ended yet and the given point is different from the first point.
		m_cut->setLine(QLineF(m_cut->line().p1(), point));
		return true;
	}
	else {
		return false;
	}
}

bool TissueSlicer::CutEnded() const
{
	return m_slices.size() > 0;
}

void TissueSlicer::Finish()
{
	m_scene->removeItem(m_cut);
	for (auto slice : m_slices) {
		m_scene->removeItem(slice);
	}

	emit Finished();
}

} // namespace
