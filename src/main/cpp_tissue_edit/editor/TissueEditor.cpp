/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for LeafEditor.
 */

#include "TissueEditor.h"

#include "EditControlLogic.h"
#include "EditorActions.h"
#include "PTreePanels.h"
#include "TissueGraphicsView.h"
#include "UndoStack.h"

#include "fileformats/PTreeFile.h"
#include "gui/controller/AppController.h"
#include "gui/controller/ProjectController.h"
#include "gui/PTreeContainer.h"

#include <boost/property_tree/detail/ptree_implementation.hpp>
#include <boost/property_tree/detail/xml_parser_error.hpp>
#include <cassert>
#include <QCloseEvent>
#include <QDockWidget>
#include <QFileDialog>
#include <QGraphicsItem>
#include <QList>
#include <QMessageBox>
#include <QMimeData>
#include <QStatusBar>
#include <QUrl>

namespace SimPT_Editor {

using namespace std;
using namespace boost::property_tree::xml_parser;
using namespace SimShell::Gui;
using namespace SimPT_Sim::Util;


TissueEditor::TissueEditor(QWidget* parent) : QMainWindow(parent),
	HasUnsavedChangesPrompt("Cell Editor"), m_tissue(nullptr), m_undo_stack(new UndoStack()),
	m_graphics_view(new TissueGraphicsView(this)),
	m_ptree_panels(new PTreePanels(this)),
	m_current_path(QDir().homePath().toStdString()),
	m_actions(new EditorActions(this, m_graphics_view, m_ptree_panels, m_undo_stack)),
	m_modified(false)
{
	setWindowTitle("simPT - Tissue Editor");
	setMenuBar(m_actions);
	setMinimumSize(800, 600);
	setAcceptDrops(true);

	CreateLayout();

	connect(m_graphics_view, SIGNAL(ItemsSelected(unsigned int)),
				m_actions, SLOT(ItemsSelected(unsigned int)));
	connect(m_graphics_view, SIGNAL(ModeChanged()), m_actions, SLOT(ModeChanged()));
}

TissueEditor::~TissueEditor() {}

void TissueEditor::closeEvent(QCloseEvent* event)
{
	if (CloseLeaf()) {
		event->accept();
	} else {
		event->ignore();
	}
}

void TissueEditor::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasUrls() ) {
	    if (e->mimeData()->urls().size()==1) {
		    const QString &fileName = e->mimeData()->urls().first().toLocalFile();
		    if (PTreeFile::IsPTreeFile(fileName.toStdString()))
			    e->acceptProposedAction();
	    }
    }
}

void TissueEditor::dropEvent(QDropEvent *e)
{
	if (e->mimeData()->urls().size()==1) {
		const QString &fileName = e->mimeData()->urls().first().toLocalFile();
		OpenPath(fileName.toStdString());
	}
}

bool TissueEditor::CloseLeaf()
{
	return PromptClose(this);
}

void TissueEditor::CreateLayout()
{
	m_graphics_view->setHidden(true);
	setCentralWidget(m_graphics_view);

	m_ptree_panels->AttributePanel()->GetDock()->setHidden(true);
	m_ptree_panels->GeometricPanel()->GetDock()->setHidden(true);
	m_ptree_panels->ParametersPanel()->GetDock()->setHidden(true);
	addDockWidget(Qt::RightDockWidgetArea, m_ptree_panels->AttributePanel()->GetDock());
	addDockWidget(Qt::RightDockWidgetArea, m_ptree_panels->GeometricPanel()->GetDock());
	addDockWidget(Qt::LeftDockWidgetArea, m_ptree_panels->ParametersPanel()->GetDock());

	m_status_info = new QLabel("");
	statusBar()->addWidget(m_status_info);	// m_status_info will be deleted by this widget.
}

void TissueEditor::GenerateRegularPattern()
{
	m_actions->Show(false);
	m_graphics_view->GenerateRegularPattern();
	m_actions->Show(true);
}

void TissueEditor::GenerateVoronoiPattern()
{
	m_actions->Show(false);
	m_graphics_view->GenerateVoronoiPattern();
	m_actions->Show(true);
}

void TissueEditor::InternalForceClose()
{
	m_graphics_view->Cleanup();
	m_tissue = nullptr;
	m_modified = false;

	m_graphics_view->setHidden(true);
	m_ptree_panels->AttributePanel()->GetDock()->setHidden(true);
	m_ptree_panels->GeometricPanel()->GetDock()->setHidden(true);
	m_ptree_panels->ParametersPanel()->GetDock()->setHidden(true);

	m_actions->LeafClosed();
	m_status_info->setText("");
}

bool TissueEditor::InternalIsClean() const
{
	return !m_modified;
}

bool TissueEditor::InternalSave()
{
	SaveLeaf();
	return true;
}

bool TissueEditor::IsOpened() const
{
	return (m_tissue != nullptr);
}

void TissueEditor::NewLeaf()
{
	try {
		m_actions->Show(false);
		//Native dialog has been disabled, due to a bug when an item is selected.
		string fileName = QFileDialog::getOpenFileName(this,
				"Select a template file with the attributes of the new tissue",
				QString::fromStdString(m_current_path), "XML files (*.xml *.xml.gz);;All files (*.*)",
				nullptr, QFileDialog::DontUseNativeDialog | QFileDialog::ReadOnly).toStdString();
		m_actions->Show(true);

		if (fileName != "") {
			if (m_tissue != nullptr) {
				CloseLeaf();
			}

			if (PTreeFile::IsGzipped(fileName)){
				PTreeFile::ReadXmlGz(fileName, m_ptree);
			} else {
				PTreeFile::ReadXml(fileName, m_ptree);
			}

			if (m_ptree.find("vleaf2") == m_ptree.not_found()
				|| m_ptree.get_child("vleaf2").find("mesh") == m_ptree.not_found()
				|| m_ptree.get_child("vleaf2.mesh").find("cells") == m_ptree.not_found()
				|| m_ptree.get_child("vleaf2.mesh.cells").find("chemical_count") == m_ptree.not_found()) {

				QMessageBox messageBox(this);
				messageBox.critical(this, "Invalid ptree error", "Property tree doesn't contain valid sim data.");
			}

			m_tissue = std::make_shared<EditControlLogic>(
					m_ptree.get<unsigned int>("vleaf2.mesh.cells.chemical_count"));
			m_ptree.get_child("vleaf2.mesh").clear();

			m_undo_stack->Initialize(m_tissue->ToPTree());
			UpdateViews();

			connect(m_tissue.get(), SIGNAL(Moved(Node*, double, double)),
							this, SLOT(SetModifiedWithoutUndo()));
			connect(m_tissue.get(), SIGNAL(Modified()), this, SLOT(SetModified()));
			connect(m_tissue.get(), SIGNAL(Modified()), m_actions, SLOT(Modified()));
			connect(m_tissue.get(), SIGNAL(StatusInfoChanged(const std::string&)),
							this, SLOT(SetStatusBar(const std::string&)));

			m_current_path = fileName;
			m_actions->LeafOpened();
			m_modified = true;
		}
	}
	catch (boost::property_tree::xml_parser::xml_parser_error& e) {
		QMessageBox messageBox(this);
		messageBox.critical(this, "XML parsing error", "Sim data file doesn't contain valid property tree.");
	}
}

void TissueEditor::OpenLeaf()
{
	try {
		m_actions->Show(false);
		//Native dialog has been disabled, due to a bug when an item is selected.
		string fileName = QFileDialog::getOpenFileName(this,
				"Open sim data file", QString::fromStdString(m_current_path),
				"XML files (*.xml *.xml.gz);;All files (*.*)", nullptr,
				QFileDialog::DontUseNativeDialog | QFileDialog::ReadOnly).toStdString();
		m_actions->Show(true);

		OpenPath(fileName);
	}
	catch (boost::property_tree::xml_parser::xml_parser_error& e) {
		QMessageBox messageBox(this);
		messageBox.critical(this, "XML parsing error", "Sim data file doesn't contain a valid property tree.");
	}
	catch (boost::property_tree::ptree_error& e) {
		QMessageBox messageBox(this);
		messageBox.critical(this, "Invalid ptree error", "Property tree doesn't contain sim data.");
	}
}

bool TissueEditor::OpenPath(const std::string& path)
{
	try {
		if (path != "") {
			if (m_tissue != nullptr) {
				CloseLeaf();
			}
			if (PTreeFile::IsGzipped(path)){
				PTreeFile::ReadXmlGz(path, m_ptree);
			} else {
				PTreeFile::ReadXml(path, m_ptree);
			}

			m_tissue = make_shared<EditControlLogic>(m_ptree.get_child("vleaf2"));

			m_undo_stack->Initialize(m_tissue->ToPTree());
			UpdateViews();

			connect(m_tissue.get(), SIGNAL(Moved(Node*, double, double)),
							this, SLOT(SetModifiedWithoutUndo()));
			connect(m_tissue.get(), SIGNAL(Modified()), this, SLOT(SetModified()));
			connect(m_tissue.get(), SIGNAL(Modified()), m_actions, SLOT(Modified()));
			connect(m_tissue.get(), SIGNAL(StatusInfoChanged(const std::string&)),
						this, SLOT(SetStatusBar(const std::string&)));

			m_current_path = path;
			m_actions->LeafOpened();

			return true;
		}
	}
	catch (boost::property_tree::xml_parser::xml_parser_error& e) {
		QMessageBox messageBox(this);
		messageBox.critical(this, "XML parsing error", "Sim data file doesn't contain a valid property tree.");
	}
	catch (boost::property_tree::ptree_error& e) {
		QMessageBox messageBox(this);
		messageBox.critical(this, "Invalid ptree error", "Property tree doesn't contain valid sim data.");
	}
	catch (std::runtime_error& e) {
		QMessageBox messageBox(this);
		messageBox.critical(this, "File not found", "File doesn't exist.");
	}

	return false;
}

void TissueEditor::Redo()
{
	assert(m_undo_stack->CanRedo() && "Tried to redo when it can't be done.");

	m_tissue = std::make_shared<EditControlLogic>(m_undo_stack->Redo());
	connect(m_tissue.get(), SIGNAL(Moved(Node*, double, double)),
			this, SLOT(SetModifiedWithoutUndo()));
	connect(m_tissue.get(), SIGNAL(Modified()), this, SLOT(SetModified()));
	connect(m_tissue.get(), SIGNAL(Modified()), m_actions, SLOT(Modified()));
	connect(m_tissue.get(), SIGNAL(StatusInfoChanged(const std::string&)),
				this, SLOT(SetStatusBar(const std::string&)));

	Mode mode = m_graphics_view->GetMode();
	UpdateViews();
	m_graphics_view->SetMode(mode);

	m_actions->Modified();
}

void TissueEditor::SaveLeaf()
{
	m_actions->Show(false);
	//Native dialog has been disabled, due to a bug when an item is selected.
	string fileName = QFileDialog::getSaveFileName(this,
			"Save sim data file", QString::fromStdString(m_current_path),
			"XML files (*.xml *.xml.gz);;All files (*.*)", nullptr,
			QFileDialog::DontUseNativeDialog).toStdString();
	m_actions->Show(true);

	if (fileName != "") {
		boost::property_tree::ptree pt = m_tissue->ToPTree();
		m_ptree.put_child("vleaf2.mesh", pt.get_child("mesh"));
		m_ptree.put_child("vleaf2.parameters", pt.get_child("parameters"));
		if (PTreeFile::IsGzipped(fileName)){
			PTreeFile::WriteXmlGz(fileName, m_ptree);
		} else {
			PTreeFile::WriteXml(fileName, m_ptree);
		}
		//xml_writer_settings<char> settings('\t', 1);
		//write_xml(fileName, m_ptree, std::locale(), settings);
		m_current_path = fileName;
		m_modified = false; //data has been written to file, so there is no modified data
	}
}

void TissueEditor::SetCellMode()
{
	if (m_graphics_view->GetMode() == Mode::CELL) {
		m_graphics_view->SetMode(Mode::DISPLAY);
	} else {
		m_graphics_view->SetMode(Mode::CELL);
	}
}

void TissueEditor::SetEdgeMode()
{
	if (m_graphics_view->GetMode() == Mode::EDGE) {
		m_graphics_view->SetMode(Mode::DISPLAY);
	} else {
		m_graphics_view->SetMode(Mode::EDGE);
	}
}

void TissueEditor::SetNodeMode()
{
	if (m_graphics_view->GetMode() == Mode::NODE) {
		m_graphics_view->SetMode(Mode::DISPLAY);
	} else {
		m_graphics_view->SetMode(Mode::NODE);
	}
}

void TissueEditor::SetStatusBar(const std::string& info)
{
	m_status_info->setText(QString::fromStdString(info));
}

void TissueEditor::SetModified()
{
	m_undo_stack->Push(m_tissue->ToPTree());
	m_modified = true;
}

void TissueEditor::SetModifiedWithoutUndo()
{
	m_modified = true;
}

void TissueEditor::Undo()
{
	assert(m_undo_stack->CanUndo() && "Tried to undo when it can't be done.");

	m_tissue = std::make_shared<EditControlLogic>(m_undo_stack->Undo());
	connect(m_tissue.get(), SIGNAL(Moved(Node*, double, double)),
			this, SLOT(SetModifiedWithoutUndo()));
	connect(m_tissue.get(), SIGNAL(Modified()), this, SLOT(SetModified()));
	connect(m_tissue.get(), SIGNAL(Modified()), m_actions, SLOT(Modified()));
	connect(m_tissue.get(), SIGNAL(StatusInfoChanged(const std::string&)),
				this, SLOT(SetStatusBar(const std::string&)));

	Mode mode = m_graphics_view->GetMode();
	UpdateViews();
	m_graphics_view->SetMode(mode);

	m_actions->Modified();
}

void TissueEditor::UpdateViews()
{
	assert(m_tissue != nullptr && "The tissue isn't initialized.");

	m_graphics_view->SetColorComponent(m_ptree.get_child("vleaf2.parameters"));
	m_graphics_view->Initialize(m_tissue);
	m_graphics_view->setHidden(false);
	m_ptree_panels->Initialize(m_tissue);
	m_ptree_panels->AttributePanel()->GetDock()->setHidden(false);
	m_ptree_panels->GeometricPanel()->GetDock()->setHidden(false);
	m_ptree_panels->ParametersPanel()->GetDock()->setHidden(false);
}

} // namespace
