#ifndef SIMPT_EDITOR_POLYGON_UTILS_H_INCLUDED
#define SIMPT_EDITOR_POLYGON_UTILS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PolygonUtils.
 */

#include <QLineF>
#include <QPolygonF>

namespace SimPT_Editor {

/**
 * Functions to manipulate polygons.
 */
class PolygonUtils
{
public:
	/**
	 * Checks whether the vertices of a polygon are ordered clockwise or counterclockwise.
	 * The polygon should be open, valid and simple.
	 *
	 * @param	polygon		The given polygon.
	 * @return	True if the vertices are clockwise.
	 */
	static bool IsClockwise(const QPolygonF& polygon);

	/**
	 * Checks whether a polygon is simple.
	 * The polygon should be open and valid.
	 *
	 * @param	polygon		The given polygon.
	 * @return	True if the polygon is simple.
	 */
	static bool IsSimplePolygon(const QPolygonF& polygon);

	/**
	 * Opens the polygon, ie. if the last point is equal to the first, a new polygon without the last point is returned, else the original polygon is returned.
	 * The polygon should be valid.
	 *
	 * @param	polygon		The polygon to open.
	 * @return	The opened polygon if it was closed, or the same if it was already open.
	 */
	static QPolygonF OpenPolygon(const QPolygonF &polygon);

	/**
	 * Checks if the order of the polygon's points is clockwise and if so, reverts it, else the original polygon is returned.
	 * Clockwise and counterclockwise order of points is checked for in a standard right-handed coordinate system.
	 * The polygon should be simple, valid and open.
	 *
	 * @param	polygon		The polygon to make counterclockwise.
	 * @return	The polygon in counterclockwise direction.
	 */
	static QPolygonF Counterclockwise(const QPolygonF &polygon);

	/**
	 * Calculate the area of a polygon.
	 * The polygon should be open, valid and simple.
	 *
	 * @param	polygon		The given polygon.
	 * @return	The area of the polygon.
	 */
	static double CalculateArea(const QPolygonF& polygon);

	/**
	 * Clip a polygon with another polygon and return the intersecting subpolygons.
	 * The polygons should be open, valid and simple.
	 *
	 * @param	polygon1	The first polygon.
	 * @param	polygon2	The second polygon.
	 * @return	The subpolygons, which are also open.
	 */
	static std::list<QPolygonF> ClipPolygon(const QPolygonF& polygon1, const QPolygonF& polygon2);

	/**
	 * Slice an open, simple and valid polygon in multiple polygons with a given line.
	 * The endpoints of the line should be located outside the polygon.
	 *
	 * If there are no (useful) intersections, the original polygon will be returned.
	 *
	 * @param	polygon		The given polygon.
	 * @param	cut		The given line.
	 * @return	The subpolygons, which are also open. These are in the same direction ((counter-)clockwise) as the given polygon.
	 */
	static std::list<QPolygonF> SlicePolygon(const QPolygonF& polygon, QLineF cut);

private:
	/**
	 * Computes the sign of the z-component of the cross product to determine in which direction line 2 turns, seen from line 1.
	 * Line 1 and line 2 are considered vectors (the order of their points matters!)
	 *
	 * @param	line1		The first line.
	 * @param	line2		The second line.
	 * @return	1 if line2 turns left , -1 if right, 0 if the two lines are parallel
	 */
	static int Turn(const QLineF &line1, const QLineF &line2);

	static constexpr double g_accuracy = 1.0e-12;	//Needed for floating-point errors introduced by Qt intersections.
};

} // namespace

#endif // end_of_include_guard
