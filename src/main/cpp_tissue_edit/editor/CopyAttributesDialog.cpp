/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CopyAttributesDialog
 */

#include "CopyAttributesDialog.h"

#include "ptree/PTreeUtils.h"

#include <QDialogButtonBox>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QVBoxLayout>

class QWidget;

namespace SimPT_Editor {

using boost::property_tree::ptree;

CopyAttributesDialog::CopyAttributesDialog(const ptree& pt, QWidget* parent)
	: QDialog(parent), m_table_view(new QTableView()), m_model(new QStandardItemModel(m_table_view))
{
	m_model->setHorizontalHeaderLabels(QStringList() << "Key" << "Data");

	InitializeModel(pt);
	SetupGui();
}

CopyAttributesDialog::~CopyAttributesDialog()
{
	delete m_table_view;
}

ptree CopyAttributesDialog::SelectedAttributes()
{
	ptree pt;

	for (int i = 0; i < m_model->rowCount(); i++) {
		QStandardItem* key = m_model->item(i, 0);
		QStandardItem* data = m_model->item(i, 1);

		if (key->checkState() == Qt::Checked) {
			pt.add(key->text().toStdString(), data->text().toStdString());
		}
		else {
			pt.add(key->text().toStdString(), "");
		}
	}

	return pt;
}

void CopyAttributesDialog::InitializeModel(const ptree& pt)
{
	std::list<std::pair<std::string, std::string>> entries = SimPT_Sim::Util::PTreeUtils::Flatten(pt);
	for (auto pair : entries) {
		QStandardItem* key = new QStandardItem();
		key->setData(QString::fromStdString(pair.first), Qt::DisplayRole);
		key->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
		key->setCheckable(true);
		key->setEditable(false);

		QStandardItem* data = new QStandardItem();
		data->setData(QString::fromStdString(pair.second), Qt::DisplayRole);
		data->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
		data->setEditable(false);

		m_model->appendRow(QList<QStandardItem*>() << key << data);
	}
}

void CopyAttributesDialog::SetupGui()
{
	setWindowTitle("Choose cell attributes");
	setMinimumWidth(400);

	QVBoxLayout* layout = new QVBoxLayout();

	m_table_view = new QTableView();
	m_table_view->setModel(m_model);
	m_table_view->setHorizontalScrollMode(QTableView::ScrollPerPixel);
	m_table_view->setSelectionBehavior(QAbstractItemView::SelectRows);
	m_table_view->setShowGrid(false);
	m_table_view->horizontalHeader()->setStretchLastSection(true);
	m_table_view->horizontalHeader()->setHighlightSections(false);
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	m_table_view->horizontalHeader()->setSectionsClickable(false);
#else
	m_table_view->horizontalHeader()->setClickable(false);
#endif
	m_table_view->verticalHeader()->setHidden(true);
	m_table_view->resizeColumnsToContents();
	layout->addWidget(m_table_view);

	QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
	layout->addWidget(buttonBox);

	setLayout(layout);
}

} // namespace
