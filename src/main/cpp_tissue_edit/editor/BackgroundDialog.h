#ifndef SIMPT_EDITOR_BACKGROUND_DIALOG_H_INCLUDED
#define SIMPT_EDITOR_BACKGROUND_DIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for BackgroundDialog.
 */

#include <QDialog>
#include <QPixmap>

class QCheckBox;
class QGraphicsPixmapItem;
class QLineEdit;

namespace SimPT_Editor {

class TransformationWidget;

/**
 * Dialog for setting the background image. Can be used as modeless dialog.
 */
class BackgroundDialog : public QDialog
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	item		The item to load the image into
	 * @param	parent		The parent widget
	 */
	BackgroundDialog(QGraphicsPixmapItem *item, QWidget *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~BackgroundDialog();

private slots:
	void BrowseImage();
	void UpdateVisibility(bool visible);
	void UpdateTransformation();

private:
	void SetupGui();

private:
	QGraphicsPixmapItem*   m_item;
	QPixmap                m_pixmap;

	QLineEdit*             m_file_path;
	QCheckBox*             m_visible;
	TransformationWidget*  m_transformation;
};

} // namespace

#endif // end-of-include-guard
