/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for LeafControlLogic
 */

#include "EditControlLogic.h"

#include "bio/Cell.h"
#include "bio/Edge.h"
#include "bio/Mesh.h"
#include "bio/Node.h"
#include "bio/Wall.h"
#include "ptree/PTreeUtils.h"

#include <boost/iterator/iterator_facade.hpp>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/detail/ptree_implementation.hpp>
#include <array>
#include <cassert>
#include <list>
#include <memory>
#include <QLineF>
#include <string>
#include <vector>

namespace SimPT_Sim { class Edge; }

namespace SimPT_Editor {

using namespace std;
using namespace boost::property_tree;
using namespace SimPT_Sim;
using namespace SimPT_Sim::Util;

EditControlLogic::EditControlLogic(unsigned int chemical_count)
	: m_mesh(nullptr),
	  m_moving_node(false),
	  m_ptree_dirty(false)
{
	m_ptree.put_child("parameters", ptree());
	m_ptree.put_child("mesh", BuildTwoSquaresMesh(chemical_count).ToPtree());
	m_mesh = new EditableMesh(m_ptree.get_child("mesh"));
}

EditControlLogic::EditControlLogic(const ptree& pt)
	: m_mesh(new EditableMesh(pt.get_child("mesh"))),  m_moving_node(false),
	  m_ptree_dirty(false)
{
	// TODO (joeri): init m_ptree with 'pt' instead??
	if (auto parameters = pt.get_child_optional("parameters")) {
		m_ptree.put_child("parameters", parameters.get());
	} else {
		m_ptree.put_child("parameters", ptree());
	}
	m_ptree.put_child("mesh", m_mesh->GetMesh()->ToPtree());
}

EditControlLogic::~EditControlLogic()
{
	delete m_mesh;
}

Mesh EditControlLogic::BuildTwoSquaresMesh(unsigned int chemical_count)
{
	Mesh mesh(chemical_count);

	auto node_0 = mesh.BuildNode(0, {{ +5.0,   0.0 }}, true);
	mesh.BuildNode(1, {{ +5.0, +10.0 }}, true);
	mesh.BuildNode(2, {{ -5.0, +10.0 }}, true);
	auto node_3 = mesh.BuildNode(3, {{ -5.0,   0.0 }}, true);
	mesh.BuildNode(4, {{ -5.0, -10.0 }}, true);
	mesh.BuildNode(5, {{ +5.0, -10.0 }}, true);

	Cell* cell_0  = mesh.BuildCell(0, {{ 0, 1, 2, 3 }});
	Cell* cell_1  = mesh.BuildCell(1, {{ 0, 3, 4, 5 }});
	Cell* cell_bp = mesh.BuildBoundaryPolygon({{ 0, 1, 2, 3, 4, 5}});

	Wall* wall_0 = mesh.BuildWall(0, node_3, node_0, cell_1, cell_bp);
	Wall* wall_1 = mesh.BuildWall(1, node_0, node_3, cell_0, cell_bp);
	Wall* wall_2 = mesh.BuildWall(2, node_3, node_0, cell_0, cell_1);

	cell_0->GetWalls().push_back(wall_1);
	cell_0->GetWalls().push_back(wall_2);
	cell_1->GetWalls().push_back(wall_0);
	cell_1->GetWalls().push_back(wall_2);
	cell_bp->GetWalls().push_back(wall_0);
	cell_bp->GetWalls().push_back(wall_1);

	mesh.ConstructNeighborList(mesh.GetBoundaryPolygon());
	mesh.UpdateNodeOwningNeighbors(mesh.GetBoundaryPolygon());
	for (auto cell : mesh.GetCells()) {
		mesh.ConstructNeighborList(cell);
		mesh.UpdateNodeOwningNeighbors(cell);
	}

	return mesh;
}

void EditControlLogic::ChangeAttributes(const ptree& pt)
{
	if (!m_selected_nodes.empty()) {
		for (Node* n : m_selected_nodes) {
			ptree node_pt = n->NodeAttributes::ToPtree();
			n->NodeAttributes::ReadPtree(UpdatePTree(node_pt, pt));
		}
	}
	else if (!m_selected_walls.empty()) {
		for (Wall* w : m_selected_walls) {
			ptree wall_pt = w->WallAttributes::ToPtree();
			w->WallAttributes::ReadPtree(UpdatePTree(wall_pt, pt));
		}
	}
	else if (!m_selected_cells.empty()) {
		for (Cell* c : m_selected_cells) {
			ptree cell_pt = c->CellAttributes::ToPtree();
			c->CellAttributes::ReadPtree(UpdatePTree(cell_pt, pt));
		}
	}
	m_ptree_dirty = true;
	emit Modified();
}

void EditControlLogic::CopyAttributes(const ptree& pt)
{
	if (!m_selected_cells.empty()) {
		for (Cell* c : m_selected_cells) {
			ptree new_pt = pt;
			ptree cell_pt = c->CellAttributes::ToPtree();
			PTreeUtils::FillPTree(new_pt, cell_pt);
			c->CellAttributes::ReadPtree(new_pt);
		}
	}
	else if (!m_selected_nodes.empty()) {
		for (Node* n : m_selected_nodes) {
			ptree new_pt = pt;
			ptree node_pt = n->NodeAttributes::ToPtree();
			PTreeUtils::FillPTree(new_pt, node_pt);
			n->NodeAttributes::ReadPtree(new_pt);
		}
	}
	else if (!m_selected_walls.empty()) {
		for (Wall* w : m_selected_walls) {
			ptree new_pt = pt;
			ptree wall_pt = w->WallAttributes::ToPtree();
			PTreeUtils::FillPTree(new_pt, wall_pt);
			w->WallAttributes::ReadPtree(new_pt);
		}
	}
	else {
		assert(false && "No entities were selected.");
	}

	m_ptree_dirty = true;
	emit Modified();
	emit SelectionChanged();
}

Cell* EditControlLogic::CreateCell(Node* node1, Node* node2, const QPointF& newPoint)
{
	auto newCell = m_mesh->CreateCell(node1, node2, newPoint);

	if (newCell != nullptr) {
		SelectCells({newCell});
		m_ptree_dirty = true;
		emit Modified();
	}

	return newCell;
}

bool EditControlLogic::DeleteCell(Cell* cell)
{
	if (cell->HasBoundaryWall()) {
		DeleteCells({cell});
		return true;
	}
	else {
		return false;
	}
}

void EditControlLogic::DeleteCells(const list<Cell*>& cells)
{
	m_mesh->DeleteCells(cells);
	Deselect();
	m_ptree_dirty = true;
	emit Modified();
}

bool EditControlLogic::DeleteNode(Node* node)
{
	if (m_mesh->DeleteTwoDegreeNode(node)) {
		Deselect();
		m_ptree_dirty = true;
		emit Modified();
		return true;
	}
	else {
		return false;
	}
}

void EditControlLogic::Deselect()
{
	m_selected_nodes.clear();
	m_selected_walls.clear();
	m_selected_cells.clear();
	emit SelectionChanged();
	emit StatusInfoChanged("No items selected.");
}

const ptree& EditControlLogic::GetParameters() const
{
	return m_ptree.get_child("parameters");
}

bool EditControlLogic::MoveNode(Node* node, double x, double y)
{
	if (m_mesh->DisplaceNode(node, x, y)) {
		m_ptree_dirty = true;
		m_moving_node = true;
		emit Moved(node, x, y);
		return true;
	}
	else {
		return false;
	}
}

const ptree& EditControlLogic::ToPTree()
{
	if (m_ptree_dirty) {
		m_ptree.put_child("mesh", m_mesh->GetMesh()->ToPtree());
	}

	return m_ptree;
}

void EditControlLogic::ReplaceCell(Cell* cell, list<QPolygonF> newCells)
{
	m_mesh->ReplaceCell(cell, newCells);
	Deselect();
	m_ptree_dirty = true;
	emit Modified();
}

void EditControlLogic::SelectCells(const std::list<Cell*>& cells)
{
	m_selected_cells = cells;
	// Gives better info for the StatusInfoChanged signal.
	m_selected_cells.sort([](Cell* c1, Cell* c2){return c1->GetIndex() < c2->GetIndex();});
	emit SelectionChanged();

	std::string info = "Cells selected: "
		+ to_string(m_selected_cells.front()->GetIndex());
	for (auto it = next(m_selected_cells.begin()); it != m_selected_cells.end(); it++) {
		info += ", " + to_string((*it)->GetIndex());
	}
	emit StatusInfoChanged(info);
}

void EditControlLogic::SelectEdges(const std::list<Edge>& edges)
{
	m_selected_walls.clear();
	for (const Edge& edge : edges) {
		Wall* w = m_mesh->GetMesh()->FindWall(edge);
		if (w != nullptr
			&& std::find(m_selected_walls.begin(), m_selected_walls.end(), w) == m_selected_walls.end()) {
			m_selected_walls.push_back(w);
		}
	}
	// Gives better info for the StatusInfoChanged signal.
	m_selected_walls.sort([](Wall* w1, Wall* w2){return w1->GetIndex() < w2->GetIndex();});
	emit SelectionChanged();

	std::string info = "Walls selected: "
		+ to_string(m_selected_walls.front()->GetIndex());
	for (auto it = next(m_selected_walls.begin()); it != m_selected_walls.end(); it++) {
		info += ", " + to_string((*it)->GetIndex());
	}
	emit StatusInfoChanged(info);
}

void EditControlLogic::SelectNodes(const std::list<Node*>& nodes)
{
	assert(nodes.size() > 0 && "No nodes were selected.");

	m_selected_nodes = nodes;
	// Gives better info for the StatusInfoChanged signal.
	m_selected_nodes.sort([](Node* n1, Node* n2){return n1->GetIndex() < n2->GetIndex();});
	emit SelectionChanged();

	string info = "Nodes selected: "
		+ to_string(m_selected_nodes.front()->GetIndex());
	for (auto it = next(m_selected_nodes.begin()); it != m_selected_nodes.end(); it++) {
		info += ", " + to_string((*it)->GetIndex());
	}
	emit StatusInfoChanged(info);
}

void EditControlLogic::SetParameters(const boost::property_tree::ptree& pt)
{
	m_ptree.put_child("parameters", pt);
	emit Modified();
}

vector<Cell*> EditControlLogic::SplitCell(Node* node1, Node* node2)
{
	vector<Cell*> cells = m_mesh->SplitCell(node1, node2);

	if (cells.size() > 1) {
		m_ptree_dirty = true;

		emit Modified();
	}

	return cells;
}

vector<Cell*> EditControlLogic::SplitCell(Cell* cell,
						Node* node1, Node* node2, bool modified)
{
	vector<Cell*> cells = m_mesh->SplitCell(cell, node1, node2);

	if (cells.size() > 1) {
		m_ptree_dirty = true;

		if (modified) {
			emit Modified();
		}
	}

	return cells;
}

Node* EditControlLogic::SplitEdge(const Edge& edge)
{
	m_ptree_dirty = true;
	emit Modified();
	return m_mesh->SplitEdge(edge);
}

void EditControlLogic::StopMoveNode()
{
	if (m_moving_node) {
		m_moving_node = false;
		emit Modified();
		emit StoppedMoving();
	}
}

ptree EditControlLogic::UpdatePTree(ptree pt1, ptree pt2)
{
	assert(pt1.data() != "?" && "The data of the first ptree should be known completely.");

	ptree pt;
	if (pt1.data() != pt2.data()) {
		pt = ptree(pt2.data());
	}
	else {
		pt = ptree(pt1.data());
	}

	if (pt1.size() == 0) {
		return pt;
	}
	else {
		ptree::const_iterator it1 = pt1.begin();
		ptree::const_iterator it2 = pt2.begin();
		while (it1 != pt1.end() && it2 != pt2.end()) {
			if (it1->first == it2->first) {
				if (it2->second.data() != "?") {
					pt.add_child(it1->first, UpdatePTree(it1->second, it2->second));
					++it1;
					++it2;
				}
				else if (pt1.count(it1->first) < pt2.count(it2->first)) {
					it2++;
				}
				else {
					pt.add_child(it1->first, it1->second);
					++it1;
					++it2;
				}
			}
			else if (it1->first < it2->first) {
				if (it1->second.data() != "?") {
					pt.add_child(it1->first, it1->second);
				}
				++it1;
			}
			else { //it1->first > it2->first
				if (it2->second.data() != "?") {
					pt.add_child(it2->first, it2->second);
				}
				++it2;
			}
		}

		for (ptree::const_iterator it = it1; it != pt1.end(); ++it) {
			if (it1->second.data() != "?") {
				pt.add_child(it->first, it->second);
			}
		}
		for (ptree::const_iterator it = it2; it != pt2.end(); ++it) {
			if (it2->second.data() != "?") {
				pt.add_child(it->first, it->second);
			}
		}

		return pt;
	}
}

} // namespace
