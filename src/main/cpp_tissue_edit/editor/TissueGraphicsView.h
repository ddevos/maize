#ifndef SIMPT_EDITOR_TISSUE_GRAPHICS_VIEW_H_INCLUDED
#define SIMPT_EDITOR_TISSUE_GRAPHICS_VIEW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for GraphicsView of tissue.
 */

#include "EditableCellItem.h"
#include "EditableEdgeItem.h"
#include "EditableNodeItem.h"

#include "exporters/BitmapGraphicsExporter.h"
#include "gui/PanAndZoomView.h"

#include "model/ComponentTraits.h"
#include "model/ComponentFactoryProxy.h"

#include <boost/property_tree/ptree.hpp>
#include <QGraphicsScene>
#include <list>
#include <memory>
#include <string>

class QGraphicsPixmapItem;
class QWidget;

namespace SimPT_Editor {

class BackgroundDialog;
class EditableCellItem;
class EditableEdgeItem;
class EditableNodeItem;
class EditControlLogic;
class SelectByIDWidget;
class TissueSlicer;

using namespace SimShell;
using namespace SimShell::Gui;

/**
 * Different modes for the graphicsview.
 */
enum struct Mode {
	DISPLAY,          //mode to display properties of the entities (immutable entities)
	NODE,             //mode to edit nodes
	NODE_COPY,        //submode to copy node attributes
	EDGE,             //mode to edit edges
	EDGE_COPY,        //submode to copy edge attributes
	CELL,             //mode to edit cells
	CELL_COPY,        //submode to copy cell attributes
	CELL_CREATE,      //submode to create a cell
	CELL_SLICE,       //submode to slice a cell
	NONE
};

/**
 * The graphical view on the tissue.
 */
class TissueGraphicsView : public PanAndZoomView
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 *
	 * @param	parent		The parent of this object.
	 */
	TissueGraphicsView(QWidget* parent);

	/**
	 * Destructor.
	 */
	virtual ~TissueGraphicsView();

	/**
	 * Adds the items in the tissue to the scene.
	 *
	 * @param	tissue		The given tissue.
	 * @param	mode		The mode which the view will be initialized with. (default = DISPLAY)
	 */
	void Initialize(std::shared_ptr<EditControlLogic> tissue, Mode mode = Mode::DISPLAY);

	/**
	 * Checks whether the selected (editable) items are at the boundary of the cell complex.
	 *
	 * @return	True if the selected items are at the boundary.
	 */
	bool SelectedIsAtBoundary() const;

	/**
	 * Checks whether the selected (editable) items are deletable.
	 * A cell or node is deletable if they are respectively at the boundary and of degree two in a cell with more than 3 nodes.
	 *
	 * @return	True if the selected items are deletable.
	 */
	bool SelectedIsDeletable() const;

	/**
	 * Checks whether two selected nodes can split a single cell.
	 * This means the nodes shouldn't be directly adjacent, but still should be part of the same cell (excluding the boundary polygon).
	 *
	 * @return	True if the selected nodes can split a cell.
	 */
	bool SelectedIsSplitable() const;

	/**
	 * Adds a cell to the scene.
	 * The endpoints should be listed in order of connection.
	 *
	 * @param	endpoints	The endpoints of the cell.
	 * @param	edges		The edges of the cell.
 	 * @param	cell		The logical representation of the cell.
	 */
	EditableCellItem* AddCell(std::list<EditableNodeItem*> endpoints, std::list<EditableEdgeItem*> edges, SimPT_Sim::Cell* cell);

	/**
	 * Adds an edge to the scene.
	 * Remember that the order of the endpoints doesn't matter.
	 *
	 * @param	endpoint1	The first endpoint of the edge.
	 * @param	endpoint2	The second endpoint of the edge.
	 */
	EditableEdgeItem* AddEdge(EditableNodeItem* endpoint1, EditableNodeItem* endpoint2);

	/**
	 * Adds a node to the scene.
	 *
	 * @param	node		The logical representation of the node.
	 */
	EditableNodeItem* AddNode(SimPT_Sim::Node* node);

	/**
	 * Returns the current mode.
	 */
	Mode GetMode() const;

	/**
	 * Returns the widget for selecting items by ID.
	 */
	SelectByIDWidget* GetSelectByIDWidget() const;

	/**
	 * Set the editing mode of the view.
	 * Invariant: When the mode is set, only items of the corresponding type can be selected.
	 */
	void SetMode(Mode mode);

	/**
	 * Generates a regular cell pattern in the selected cell.
	 */
	void GenerateRegularPattern();

	/**
	 * Generates a cell pattern using Voronoi Tesselation in the selected cell.
	 */
	void GenerateVoronoiPattern();

	/**
	 * Set the colorizer map for coloring the cells in 'NONE'-mode.
	 *
	 * @param	parameters	The parameters to initialize the colorizer map.
	 */
	void SetColorComponent(const boost::property_tree::ptree& parameters);

	/**
	 * Cleans the scene.
	 * The background image is kept, as the scene isn't its owner.
	 */
	void Cleanup();

public slots:
	/**
	 * Cancel the current action. (only available in CELL_CREATE and CELL_SLICE mode)
	 */
	void CancelAction();

	/**
	 * Copy attributes action (only available in CELL mode).
	 * The user will be asked to give a source cell and attributes. Those attributes will be copied to the selected cells.
	 */
	void CopyAttributes();

	/**
	 * Create a cell.
	 */
	void CreateCell();

	/**
	 * Delete the selected item.
	 */
	void DeleteItem();

	/**
	 * Split the selected edge. (only available in EDGE mode)
	 */
	void SplitEdge();

	/**
	 * Split a cell through the selected nodes. (only available in NODE mode)
	 */
	void SplitCell();

	/**
	 * Update the moved node in the scene.
	 */
	void UpdateMovedNode();

	/**
	 * Opens the dialog to set the background image of the workspace.
	 */
	void SetBackground();

	/**
	 * Set the preferences of the display mode ('NONE').
	 * The selected preference will determine the coloring of the cells in this mode.
	 */
	void SetDisplayModePreferences();

	/**
	 * Set background of cells transparent.
	 *
	 * @param	transparent		True if the cells should be transparent.
	 */
	void SetTransparentCells(bool transparent);

signals:
	/**
	 * Emitted when the mode has changed. (only emitted when changed to a global mode [NODE, EDGE or CELL]).
	 */
	void ModeChanged();

	/**
	 * Emitted when the selection has changed. (only emitted when in a global mode [NODE, EDGE or CELL]).
	 *
	 * @param	unsigned int		The number of items selected.
	 */
	void ItemsSelected(unsigned int count);

	/**
	 * Emitted when certain info about the view has changed.
	 * This includes the four basic modes ('DISPLAY', 'NODE', 'EDGE', 'CELL').
	 *
	 * @param	info		The information in string form.
	 */
	void StatusInfoChanged(const std::string& info);

private slots:
	/**
	 * Updates on a change of the selection
	 */
	void UpdateSelection();

	/**
	 * Finish the slice action.
	 */
	void FinishSliceAction();

	/**
	 * Select the items with the given IDs in the current mode. (Only modes 'NODE', 'EDGE' and 'CELL' supported)
	 * If the IDs of walls are given, all the edges associated with those walls will be selected.
	 * The view will also be adjusted to the selected items (see EnsureVisible() for more information).
	 *
	 * @param	ids			The given IDs.
	 * @param	keepItemsSelected	False if the currently selected items should be deselected.
	 */
	void SelectItems(const std::list<unsigned int>& ids, bool keepItemsSelected = false);

private:
	/**
	 * Delete the given node. (Only nodes of degree two can be deleted.)
	 *
	 * @param	node		The given node.
	 */
	void DeleteNode(EditableNodeItem* node);

	/**
	 * Delete the given cell. (Only boundary cells can be deleted.)
	 *
	 * @param	cell		The given cell.
	 */
	void DeleteCell(EditableCellItem* cell);

	/**
	 * Ensure that the given items are visible in the scene (with the default qt margin for ensuring visibility).
	 *
	 * @param	items		The given items.
	 */
	void EnsureVisible(const std::vector<QGraphicsItem*>& items);

	/**
	 * Get the represented item associated with the given node.
	 *
	 * @param	node		The given node.
	 */
	EditableNodeItem* GetEditableNode(SimPT_Sim::Node* node) const;

	/**
	 * Set the flags of the items associated with the current mode.
	 *
	 * @param	enable		When false, all flags will be set to false.
	 */
	void SetEditableItemFlags(bool enable = true);

	/**
	 * Checks for graphical consistency (no nodes on top of each other).
	 */
	bool IsConsistent() const;

	/**
	 * Reimplemented to allow reverting when an invalid displacement has occured.
	 */
	virtual void mouseMoveEvent(QMouseEvent* event);

	/**
	 * Reimplemented to allow deselection of the selected item.
	 */
	virtual void mousePressEvent(QMouseEvent* event);

	/**
	 * Reimplemented to update the position of the logical node.
	 */
	virtual void mouseReleaseEvent(QMouseEvent* event);

	/**
	 * Reimplemented to do nothing. (gives unwanted behaviour)
	 */
	virtual void mouseDoubleClickEvent(QMouseEvent* event);

private:
	std::shared_ptr<EditControlLogic>      m_tissue;
	std::list<EditableCellItem*>           m_cells;
	std::list<EditableEdgeItem*>           m_edges;
	std::list<EditableNodeItem*>           m_nodes;
	QGraphicsPixmapItem*                   m_background_item;
	QGraphicsScene                         m_scene;
	Mode                                   m_mode;

private:
	bool                                   m_cells_transparent;

private:
	std::shared_ptr<SimPT_Sim::ComponentFactoryProxy>  m_factory;
	boost::property_tree::ptree                        m_ptree;
	QString                                            m_colorizer_pref;

private:
	TissueSlicer*                          m_tissue_slicer;
	BackgroundDialog*                      m_background_dialog;
	SelectByIDWidget*                      m_select_by_id;

private:
	static constexpr double g_node_radius = 1;
};

} // namespace

#endif // end_of_include_guard
