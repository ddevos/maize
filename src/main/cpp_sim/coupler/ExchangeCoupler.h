#ifndef BOUNDARYC_COUPLER_H_
#define BOUNDARYC_COUPLER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ExchangeCoupler.
 */

#include "ICoupler.h"

#include "sim/CoreData.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <utility>
#include <vector>
#include <tuple>

namespace SimPT_Sim {

/**
 * ExchangeCoupler
 */
class ExchangeCoupler : public ICoupler
{
public:
	ExchangeCoupler();

	virtual ~ExchangeCoupler() {}

	/**
	 * @see	ICoupler::Initialize.
	 *
	 * The parameter ptree should look something like this.
	 *
	 * <chemical_from>[The chemical from which should be transferred]</chemical_from>
	 * <chemical_to>[The chemical to which should be transferred]</chemical_to>
	 * <transfer_cells_array>
	 *	<transfer_cells> (for every pair of transfer cells)
	 *		<cell_from>[The cell from which should be transferred]</cell_from>
	 *		<cell_to>[The cell to which should be transferred]</cell_to>
	 *	</transfer_cells>
	 *	[...]
	 * </transfer_cells_array>
	 */
	virtual void Initialize(
		const boost::property_tree::ptree& parameters, const CoreData& from, const CoreData& to);

	/**
	 * @see	ICoupler::Exec.
	 */
	virtual void Exec();

private:
	CoreData   m_cd;         ///< CoreData of the coupled sim itself.
	CoreData   m_from;       ///< CoreData of the <from> simulator.
	CoreData   m_to;         ///< CoreData of the <to> simulator.

	/// Indices of chemical to be transferred from source to target.
	std::pair<unsigned int, unsigned int> m_transfer_chemicals;

	/// Every cell pair represents the indices of source cell and target cell.
	std::vector<std::tuple<unsigned int, unsigned int, double>> m_transfer_cells;
};

} // namespace

#endif // end-of-include-guard
