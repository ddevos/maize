/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ExchangeCoupler.
 */

#include "ExchangeCoupler.h"

#include "bio/Cell.h"
#include "sim/Sim.h"

#include <boost/property_tree/ptree.hpp>
#include <unordered_map>

using namespace std;
using namespace boost::property_tree;

namespace SimPT_Sim {

ExchangeCoupler::ExchangeCoupler()
{
}

void ExchangeCoupler::Initialize( const ptree& params,
        const CoreData& from, const CoreData& to)
{
	m_from = from;
	m_to   = to;
	m_transfer_chemicals = { params.get<unsigned int>("chemical_from"), params.get<unsigned int>("chemical_to")};

	for (const auto& transfer_cells : params.get_child("transfer_cells_array")) {
		const auto from  = transfer_cells.second.get<unsigned int>("cell_from");
		const auto to    = transfer_cells.second.get<unsigned int>("cell_to");
		const auto D     = transfer_cells.second.get<double>("diffusion");
		m_transfer_cells.push_back(make_tuple(from, to, D));
	}
}

void ExchangeCoupler::Exec()
{
	assert(m_from.m_mesh->GetNumChemicals() >= m_transfer_chemicals.first + 1
				&& "Mesh to transfer chemicals from has too few chemicals");
	assert(m_to.m_mesh->GetNumChemicals() >= m_transfer_chemicals.second + 1
				&& "Mesh to transfer chemicals to has too few chemicals");

	const auto& from_cells  = m_from.m_mesh->GetCells();
	const auto& to_cells    = m_to.m_mesh->GetCells();
	auto& from_map          = *m_from.m_coupled_sim_transfer_data;
	auto& to_map            = *m_to.m_coupled_sim_transfer_data;

	for (auto c : m_transfer_cells) {
		assert((from_cells.size() >= get<0>(c) + 1) && "No cell with from index");
		assert((to_cells.size()   >= get<1>(c) + 1) && "No cell with to index");

		const Cell& from_cell = *(from_cells[get<0>(c)]);
		const Cell& to_cell   = *(to_cells[get<1>(c)]);

		double value;


		// Update to transfer map
		value= ( from_cell.GetChemical(m_transfer_chemicals.first) ) / (from_cell.GetArea());
		to_map[get<1>(c)] = make_tuple (value, get<2>(c));

		// Update from transfer map
		value= ( to_cell.GetChemical(m_transfer_chemicals.second) ) / (to_cell.GetArea());

		from_map[get<0>(c)] = make_tuple (value, get<2>(c));

	}
}

} // namespace
