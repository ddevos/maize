/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for OdeintFactories0Map.
 */

#include "OdeintFactories0Map.h"

#include <boost/functional/value_factory.hpp>
#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;
using boost::value_factory;

namespace {

/**
 * Template generates odeint solvers with fixed time step.
 */
template<typename System, typename State, typename Stepper>
class Solver0
{
public:
	void operator()(System system, State& start_state,
		double start_time, double end_time, double dt)
	{
		Stepper stepper;
		integrate_const(stepper,
			system, start_state, start_time, end_time, dt);
	}
};

} // namespace


namespace SimPT_Sim {

OdeintFactories0Map::OdeintFactories0Map()
: MapType({

	// the constant stepsize integrators
	make_pair("adams_bashforth",
		value_factory<Solver0<System, State, adams_bashforth<5, State>>>()),
	make_pair("bulirsch_stoer",
		value_factory<Solver0<System, State, bulirsch_stoer<State>>>()),
	make_pair("euler",
		value_factory<Solver0<System, State, euler<State>>>()),
	make_pair("modified_midpoint",
		value_factory<Solver0<System, State, modified_midpoint<State>>>()),
	make_pair("runge_kutta4",
		value_factory<Solver0<System, State, runge_kutta4<State>>>()),
})
{}

} // namespace
