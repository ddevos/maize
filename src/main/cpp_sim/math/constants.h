#ifndef MATH_CONSTANTS_H_INCLUDED
#define MATH_CONSTANTS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Math constants.
 */

#include <limits>
#include <type_traits>

namespace SimPT_Sim {
namespace Util {

/// Math constant pi.
inline constexpr double pi() { return 3.14159265358979323846264338327950288L; }

/// Math constant 2*pi.
inline constexpr double two_pi() { return 2.0L * 3.14159265358979323846264338327950288L; }

/// Small real number.
inline constexpr double small_real() { return std::numeric_limits<float>::min(); }

} // Util
} // SimPT_Sim

#endif // end_of_include_guard
