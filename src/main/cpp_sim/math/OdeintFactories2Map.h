#ifndef ODEINT_FACTORIES2_MAP_H_INCLUDED
#define ODEINT_FACTORIES2_MAP_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for OdeintFactories2Map.
 */

#include "math/OdeintTraits.h"
#include "util/misc/FunctionMap.h"

#include <functional>
#include <memory>
#include <vector>

namespace SimPT_Sim {

/**
 * Stores factory functions for odeint integrator algorithm.
 */
class OdeintFactories2Map :
	public SimPT_Sim::Util::FunctionMap<OdeintTraits<>::Solver(double&, double&)>
{
public:
	using MapType = SimPT_Sim::Util::FunctionMap<OdeintTraits<>::Solver(double&, double&)>;

public:
	OdeintFactories2Map();

private:
	using State  = OdeintTraits<>::State;
	using System = OdeintTraits<State>::System;
	using Solver = OdeintTraits<State>::Solver;
};

} // VLEaf2_Sim

#endif // end_of_include_guard
