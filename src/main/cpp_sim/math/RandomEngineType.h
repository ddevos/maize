#ifndef RANDOM_ENGINE_TYPE_H_INCLUDED
#define RANDOM_ENGINE_TYPE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of RandomEngineType
 */

#include <string>

namespace SimPT_Sim {
namespace RandomEngineType {

/// Info.
struct Info
{
	Info() : seed(0), state(""), type("") {};
	unsigned int seed;
	std::string  state;
	std::string  type;

};

/// Enumerates type ids.
enum class TypeId {
	// We are using a sensible subset of engines provided by the trng library
	lcg64, lcg64_shift, mrg2, mrg3, yarn2, yarn3

	// Before using Tina's RNG std C++ random engines were used. They are
	// listed here in comment as a reference:
	// minstd_rand0, minstd_rand, mt19937, mt19937_64, ranlux24_base,
	// ranlux48_base, knuth_b
};

/// Check whether type with name s exists.
bool IsExisting(std::string s);

/// Check whether type with id b exists.
bool IsExisting(TypeId b);

/// Convert a type id to corresponding name.
std::string ToString(TypeId b);

/// Convert a string with name to type id.
TypeId FromString(std::string s);

} // namespace
} // namespace

#endif // end-of-include-guard
