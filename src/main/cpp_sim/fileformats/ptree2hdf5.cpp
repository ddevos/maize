/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of ptree2hdf5.
 */

// Code only available on platforms with libhdf5
#ifdef USE_HDF5

#include "ptree2hdf5.h"
#include <sstream>
#include <iostream>

#include <boost/xpressive/xpressive.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace std;
using namespace boost::xpressive;

using namespace boost::property_tree;
using boost::lexical_cast;

namespace SimPT_Shell {

//template <typename T>
//bool from_string(T& t, const std::string& s, std::ios_base& (*f)(std::ios_base&) = std::dec)
//{
    //std::istringstream iss(s);
    //return !(iss >> f >> t).fail();
//}


/**
 * Returns true if the provided string represents a floating point number, false otherwise.
 */
bool isFloat(const std::string& str) {
    // A signed +/- floating point number with a dot either at beginning, end or in the middle of the digits
    // E.g: 1.0 1.5 .5 -.001 +1. all match but a normal integer does not
    // The exponential part is optional
    // TODO: needs a low-priority fix to match expressions like 1e+02
    //       i.e: without decimal point but including an exponential part

    const sregex match_float = sregex::compile( "^[+-]?(\\d*\\.\\d+|\\d+\\.\\d*)([eE][-+]?\\d+)?$");
    return regex_match( str, match_float);
}


/**
 * Returns true if the provided string represents an integer number, false otherwise.
 */
bool isInteger(const std::string& str) {
    // A signed +/- integer with one or more digits
    const sregex match_integer = sregex::compile("^[+-]?\\d+$");
    return regex_match( str, match_integer);
}


void ptree2hdf5(const ptree& pt, hid_t h5_id) {
    // Loop over the ptree's (key, value) pairs
    for (auto sub_pt_kv : pt) {
        // Name of the key
        string key = sub_pt_kv.first;

        // Ignore XML comments
        if (key != "<xmlcomment>") {
            // Only proceed if no HDF5 link named 'key' exists already
            // (this would mean the ptree is malformed)
            if(!H5Lexists(h5_id, key.c_str(), H5P_DEFAULT) && !H5Aexists(h5_id, key.c_str())) {

                // String representation of the value associated with the key
                string value = sub_pt_kv.second.get_value<string>();

                if (key == "value_array") {
                    // TODO: handle this properly
                    //cout << "VALUE_ARRAY!" << endl;
                }
                // We're in a ptree node (key, value) without children
                if (value != "") {
                    if (isFloat(value)) {
                        // Create & write a simple scalar HDF5 attribute of type H5T_NATIVE_DOUBLE
                        double value_double = lexical_cast<double>(value);
                        hid_t attr_dspace = H5Screate(H5S_SCALAR);
                        hid_t attr_id = H5Acreate(h5_id, key.c_str(), H5T_IEEE_F64LE, attr_dspace, H5P_DEFAULT, H5P_DEFAULT);
                        H5Awrite(attr_id, H5T_NATIVE_DOUBLE, &value_double);
                        H5Aclose(attr_id);
                        H5Sclose(attr_dspace);
                    } else if (isInteger(value)) {
                        // Create & write a simple scalar HDF5 attribute of type H5T_NATIVE_INT
                        int value_int = lexical_cast<int>(value);
                        hid_t attr_dspace = H5Screate(H5S_SCALAR);
                        hid_t attr_id = H5Acreate(h5_id, key.c_str(), H5T_STD_I32LE, attr_dspace, H5P_DEFAULT, H5P_DEFAULT);
                        H5Awrite(attr_id, H5T_NATIVE_INT, &value_int);
                        H5Aclose(attr_id);
                        H5Sclose(attr_dspace);
                    } else {
                        // Create HDF5 attribute of type H5T_STRING
                        const char* value_string = value.c_str();
                        hid_t attr_dspace = H5Screate(H5S_SCALAR);
                        hid_t attr_dtype = H5Tcopy(H5T_C_S1);
                        H5Tset_size(attr_dtype, H5T_VARIABLE);
                        hid_t attr_id = H5Acreate(h5_id, key.c_str(), attr_dtype, attr_dspace, H5P_DEFAULT, H5P_DEFAULT);
                        H5Awrite(attr_id, attr_dtype, &value_string);
                        H5Aclose(attr_id);
                        H5Sclose(attr_dspace);
                        H5Tclose(attr_dtype);
                    }

                // We're in a ptree subtree node (key, value), where value is a subtree
                } else {
                    // Create a HDF5 subgroup with key's name
                    hid_t h5_sub_id = H5Gcreate(h5_id, key.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

                    // Dive in recursively into subtree
                    const ptree& sub_pt = pt.get_child(key);
                    ptree2hdf5(sub_pt, h5_sub_id);

                    // Close HDF5 subgroup
                    H5Gclose(h5_sub_id);
                }
            } else {
                // TODO: silently ignore, but should be catched by properly saving value_arrays
                //cout << "Duplicate key names '" << key << endl;
            }
        }
    }
}

// Callback used by H5Aiterate_by_name when traversing all attributes in a group
//
// Signature:
// herr_t (*H5A_operator2_t)( hid_t location_id, const char *attr_name, const H5A_info_t *ainfo, void *op_data)
//
// The operation receives the location identifier for the group or dataset being
// iterated over, location_id; the name of the current object attribute, attr_name;
// the attributes info struct, ainfo; and a pointer to the operator data passed into H5Aiterate_by_name, op_data.
herr_t attribute_info(hid_t loc_id, const char* attr_name, const H5A_info_t* , void* data) {
	// Open currently visited attribute
	hid_t attr_id = H5Aopen(loc_id, attr_name, H5P_DEFAULT);
	hid_t attr_dtype = H5Aget_type(attr_id);
	hid_t attr_dspace = H5Aget_space(attr_id);

	// Must be 0 (a single scalar) or 1 (a vector) other cases will be ignored
	const int ndims = H5Sget_simple_extent_ndims(attr_dspace);

	// The currently visited atribute is a candidate to be put in the ptree passed as raw data
	ptree* pt_attr = static_cast<ptree*>(data);

	// Check type class of attribute, either integer, real or string
	// (other types are silently ignored)
	H5T_class_t attr_class = H5Tget_class(attr_dtype);
	if (H5T_INTEGER == attr_class) {
		if (0 == ndims) {              // A scalar int
			int attr_value = 0;
			H5Aread(attr_id, H5T_NATIVE_INT, &attr_value);
			pt_attr->put(attr_name, attr_value);
		} else if (1 == ndims) {       // A vector of ints
			hsize_t length = 0;
			H5Sget_simple_extent_dims(attr_dspace, &length, 0);
			vector<int> attr_values(length);
			H5Aread(attr_id, H5T_NATIVE_INT, attr_values.data());
			for (int attr_value : attr_values) {
				pt_attr->add(string(attr_name) + ".value_array.value", attr_value);
			}
		}
	} else if (H5T_FLOAT == attr_class) {
		if (0 == ndims) {              // A scalar double
			double attr_value = 0.0;
			H5Aread(attr_id, H5T_NATIVE_DOUBLE, &attr_value);
			pt_attr->put(attr_name, attr_value);
		} else if (1 == ndims) {       // A vector of doubles
			hsize_t length = 0;
			H5Sget_simple_extent_dims(attr_dspace, &length, 0);
			vector<double> attr_values(length);
			H5Aread(attr_id, H5T_NATIVE_DOUBLE, attr_values.data());
			for (double attr_value : attr_values) {
				pt_attr->add(string(attr_name) + ".value_array.value", attr_value);
			}
		}
	} else if (H5T_STRING == attr_class) {
		// Note that parameters of type H5T_STRING are scalars so one does not
		// check for ndims and dims.
		char* attr_value;
		hid_t attr_ntype = H5Tcopy(H5T_C_S1);
		H5Tset_size(attr_ntype, H5T_VARIABLE);
		H5Aread(attr_id, attr_ntype, &attr_value);
		H5Tclose(attr_ntype);
		//cout << " attribute: " << attr_name << " = " << attr_value <<  " [H5T_STRING]" << endl;
		pt_attr->put(attr_name, attr_value);
		H5Dvlen_reclaim (attr_ntype, attr_dspace, H5P_DEFAULT, attr_value);
	} else {
		//cout << " attribute: " << attr_name << " = don\'t know, don\'t care..." << endl;
		// None of the types we support, carry on.
	}

	// Clean up handles and signal success to the calling function
	H5Sclose(attr_dspace);
	H5Tclose(attr_dtype);
	H5Aclose(attr_id);
	return 0;
}

// Callback used by H5Ovisit to visit all subgroups of a given group recursively.
//
// Signature: 
// herr_t (*H5O_iterate_t)( hid_t g_id, const char *name, const H5O_info_t *info, void *op_data)
//
// o_id	         Object that serves as root of the iteration; same value as the H5Ovisit object_id parameter
// name	         Name of object, relative to o_id, being examined at current step of the iteration
// object_info   H5O_info_t struct containing information regarding that object
// op_data	     User-defined pointer to data required by the application in processing the object;
//               a pass-through of the op_data pointer provided with the H5Ovisit_by_name function call
herr_t visit_group(hid_t loc_id, const char *group_name, const H5O_info_t *obj_info, void *data) {
	// Only look at groups, ignore other objects
	// (also, ignore the root of the tree we're traversing)
	if (*group_name != '.' && H5O_TYPE_GROUP == obj_info->type) {
		hsize_t n = 0;   // Index to start iteration over attributes, must be set to 0
		ptree pt_attr;   // ptree that will be populated by the attributes in current group
		H5Aiterate_by_name(loc_id, group_name, H5_INDEX_NAME, H5_ITER_NATIVE, &n, &attribute_info, static_cast<void*>(&pt_attr), H5P_DEFAULT);

		// Append attributes from pt_attr as a subtree with current group name as root name
		ptree* pt_root = static_cast<ptree*>(data);
		// Since HDF5 uses '/' as a separator in path names, we need to ask
		// boost to use that instead of the default '.' to separate paths in a ptree.
		pt_root->put_child(ptree::path_type(group_name, '/'), pt_attr);
	}
	return 0;
}

ptree hdf52ptree(hid_t loc_id) {
	// property tree to be filled with subtrees (HDF5 groups) and elements (HDF5 attributes)
	ptree pt;

	// Loop recursively through all HDF5 objects starting from loc_id as root
	// Pass the ptree to be populated as a pointer to raw data.
	H5Ovisit(loc_id, H5_INDEX_NAME, H5_ITER_NATIVE, &visit_group, static_cast<void*>(&pt));

	return pt;
}

} // namespace

#endif // end_of_include_guard

