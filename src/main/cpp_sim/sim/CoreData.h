#ifndef SIMPT_SIM_CORE_DATA_H_INCLUDED
#define SIMPT_SIM_CORE_DATA_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Core data used during model execution.
 */

#include "TimeData.h"
#include "bio/Tissue.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <functional>
#include <memory>
#include <unordered_map>

namespace SimPT_Sim {

class Mesh;
class RandomEngine;

/**
 * Core data with mesh, parameters, random engine and time data.
 */
struct CoreData
{
	CoreData() {}

	using TransferMapType = std::unordered_map<unsigned int,  std::tuple<double,double> >; // tuple, first value of coupled cell, second diffusion parameter D


	std::shared_ptr<TransferMapType>               m_coupled_sim_transfer_data;    ///<
	std::shared_ptr<Mesh>                          m_mesh;                         ///<
	std::shared_ptr<boost::property_tree::ptree>   m_parameters;                   ///<
	std::shared_ptr<RandomEngine>                  m_random_engine;                ///<
	std::shared_ptr<TimeData>                      m_time_data;                    ///<
	Tissue                                         m_tissue;                       ///< For future use.

	/// Verify all pointers non-null.
	bool Check() const
	{
		return (m_mesh != nullptr) && (m_parameters != nullptr)
			&& (m_random_engine != nullptr) && (m_time_data != nullptr) ;
	}
};

} // namespace

#endif // end_of_include_guard
