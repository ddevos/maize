/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of SimState.
 */

#include "SimState.h"

#include "util/misc/XmlWriterSettings.h"

#include <boost/property_tree/xml_parser.hpp>
#include <iostream>
#include <iomanip>

using namespace std;
using boost::property_tree::ptree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim {

int SimState::GetTimeStep() const
{
	return m_time_step;
}

double SimState::GetTime() const
{
	return m_time;
}

MeshState SimState::GetMeshState() const
{
	return m_mesh_state;
}

ptree SimState::GetParameters() const
{
	return m_parameters;
}

string SimState::GetProjectName() const
{
	return m_project_name;
}

ptree SimState::GetRandomEngineState() const
{
	return m_re_state;
}

void SimState::SetTimeStep(int step)
{
	m_time_step = step;
}

void SimState::SetTime(double t)
{
	m_time = t;
}

void SimState::SetMeshState(const MeshState& mesh_state)
{
	m_mesh_state = mesh_state;
}

void SimState::SetParameters(const ptree& parameters)
{
	m_parameters = parameters;
}

void SimState::SetProjectName(string project_name)
{
	m_project_name = project_name;
}

void SimState::SetRandomEngineState(const ptree& re_state)
{
	m_re_state = re_state;
}

void SimState::PrintToStream(ostream& out) const
{
	out << "SimState at time step: " << this->m_time_step;
	out << ", time: " << this->m_time << endl;
	m_mesh_state.PrintToStream(out);
	out << "Simulation parameters:" << endl;
	write_xml(out, m_parameters, XmlWriterSettings::GetTab());
	out << "Random engine state:" << endl;
	write_xml(out, m_re_state, XmlWriterSettings::GetTab());
}

}

