#ifndef SIMPT_SIM_H_
#define SIMPT_SIM_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Sim, the actual simulator.
 */

#include "CoreData.h"
#include "bio/Mesh.h"
#include "math/RandomEngine.h"
#include "math/RandomEngineType.h"
#include "model/ComponentInterfaces.h"
#include "sim/event/SimEvent.h"
#include "sim/CoreData.h"
#include "sim/SimState.h"
#include "SimInterface.h"
#include "TimeSlicer.h"

#include "util/clock_man/Timeable.h"
#include "util/misc/Subject.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <functional>
#include <random>
#include <string>
#include <thread>
#include <mutex>

namespace SimPT_Sim {

class TimeSlicer;

/**
 * Simulator: mesh & parameters, model & algorithms.
 */
class Sim : public SimPT_Sim::SimInterface,
            public SimPT_Sim::Util::Subject<SimPT_Sim::Event::SimEvent, std::weak_ptr<const void>>,
            public std::enable_shared_from_this<Sim>
{
public:
	/// Constructor does almost no initialization work (@see Sim::Initialize).
	Sim();

	/// No copy-constructor.
	Sim(const Sim& other) =delete;

	/// Assignment operator rebuilds copy of mesh.
	Sim& operator=(const Sim& other);

	/// Destructor is virtual (@see Subject ).
	virtual ~Sim();

        /// @see SimInterface.
        CoreData& GetCoreData() override final { return m_cd; }

	/// @see SimInterface.
	const boost::property_tree::ptree& GetParameters() const;

	/// @see SimInterface.
	std::string GetProjectName() const;

	/// @see SimInterface.
	std::string GetRunDate() const;

	/// @see SimInterface.
	int GetSimStep() const;

	/// @see SimInterface.
	double GetSimTime() const;

	/// Provide sim state in format suitable for i/o.
	SimState GetState() const;

	/// Return a status message (time, steps, cellcount).
	std::string GetStatusMessage() const;

	/// @see SimInterface.
	Timings GetTimings() const;

	/// Initialize with full configuration (global info, parameters,
	/// random engine, mesh) i.e. complete setup prior to first use.
	void Initialize(const boost::property_tree::ptree& pt);

	/// Initialize with full configuration (global info, parameters,
	/// random engine, mesh).i.e. complete setup prior to first use.
	void Initialize(const SimState& sim_state);

	/// @see SimInterface.
	bool IsAtTermination() const;

	/// @see SimInterface.
	bool IsStationary() const;

	/// @see SimInterface.
	void Reinitialize(const boost::property_tree::ptree& p);

	/// Resets the execution timings.
	void ResetTimings();

        /// Return a SimTimeSlicer to proceed through a time step using time slices.
        std::unique_ptr<TimeSlicer> TimeSlicing();

	/// @see SimInterface.
	void TimeStep();

	/// Serialize into ptree.
	boost::property_tree::ptree ToPtree() const;

private:
	/// Make Timeslicer our friend.
	friend class TimeSlicer;

        /// Only to be called from the TimeSlicer, hence the lock_guard arg.
        void TimeSliceGo(const std::lock_guard<std::mutex>&, double time_slice, SimPhase phase = SimPhase::NONE);

	/// Only to be called from the TimeSlicer, hence the lock_guard arg.
	void TimeSliceSetup(const std::lock_guard<std::mutex>&);

        /// Only to be called from the TimeSlicer, hence the lock_guard arg.
        void TimeSliceWrapup(const std::lock_guard<std::mutex>&);

private:
	/// Initialization of random engine (cannot be reinitialized).
	bool RandomEngineInitialize(const boost::property_tree::ptree& pt);

	/// Initialization of random engine (cannot be reinitialized).
	bool RandomEngineInitialize(const SimState& sim_state);

private:
	bool                   m_is_stationary;    ///< True when simulation becomes stationary.
	CoreData               m_cd;               ///< Core data (params, mesh, sim_time,...).
	std::string            m_project_name;     ///< Name that identifies the simulation project.
	std::string            m_run_date;         ///< Date this run of simulation started.
	TimeEvolverComponent   m_time_evolver;     ///< Time propagation component.
	std::mutex             m_timestep_mutex;   ///< Locked during time step.
	Timings                m_timings;          ///< Timing durations for parts of the computations.
};

} // namespace

#endif // end_of_include_guard
