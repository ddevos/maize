#ifndef PTREE_SETTINGS_H_INCLUDED
#define PTREE_SETTINGS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file.
 * Xml writer settings class
 */

#include <boost/property_tree/xml_parser.hpp>
#include <boost/version.hpp>

namespace SimPT_Sim {
namespace Util {

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

/**
 * Settings for xml output of Property Trees (ptree).
 */
class XmlWriterSettings
{
public:
#if BOOST_VERSION >= 105600
	static xml_writer_settings<typename ptree::key_type> GetTab()
	{
		return xml_writer_make_settings<ptree::key_type>('\t', 1);
	}
#else
	static xml_writer_settings<char> GetTab()
	{
		return xml_writer_make_settings('\t', 1);
	}
#endif
};

} // namespace
} // namespace

#endif // end-of-include-guard
