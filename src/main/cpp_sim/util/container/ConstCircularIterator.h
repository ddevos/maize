#ifndef CONTAINER_CONST_CIRCULAR_ITERATOR_H_
#define CONTAINER_CONST_CIRCULAR_ITERATOR_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ConstCircularIterator class and helper functions.
 */

#include "CircularIteratorImpl.h"

#include <iterator>
#include <type_traits>

namespace SimPT_Sim {
namespace Container {

/**
 * Alias template defines const circular iterator.
 * @see Impl_::CircularIterator
 */
 template<typename T>
 using ConstCircularIterator = Impl_::CircularIterator<T,
			typename std::add_const<typename T::value_type>::type,
			typename std::add_const<typename T::value_type>::type*,
			typename std::add_const<typename T::value_type>::type&>;

/**
 * Helper produces const circular iterator whose range
 * corresponds to the begin and end iterators of a container.
 * @param  c    container
 * @return      CircularIterator pointing to beginning of container
 */
template<typename T>
ConstCircularIterator<typename T::const_iterator> make_const_circular(const T& c)
{
	return ConstCircularIterator<typename T::const_iterator>(std::begin(c), std::end(c), std::begin(c));
}

/**
 * Helper produces const circular iterator whose range
 * corresponds to the begin and end iterators of a container.
 * @param  c    container
 * @return      CircularIterator pointing to beginning of container
 */
template<typename T>
ConstCircularIterator<typename T::const_iterator> make_const_circular(const T& c, typename T::const_iterator i)
{
	return ConstCircularIterator<typename T::const_iterator>(std::begin(c), std::end(c), i);
}

/**
 * Helper produces non-const circular iterator with
 * specified range and initial value.
 * @param  b     iterator pointing to beginning of range
 * @param  e     iterator pointing to end of range
 * @param  i     initial position of the iterator
 */
template<typename T>
ConstCircularIterator<T> make_const_circular(T b, T e, T i)
{
	return ConstCircularIterator<T>(b, e, i);
}

/**
 * Helper produces non-const circular iterator with
 * specified range, with initial value at the start of the range.
 * @param  b     iterator pointing to beginning of range
 * @param  e     iterator pointing to end of range
 */
template<typename T>
ConstCircularIterator<T> make_const_circular(T b, T e)
{
	return ConstCircularIterator<T>(b, e, b);
}

/**
 * Helper yields the position the iterator would have if
 * moved forward (in circular fashion) by 1 position.
 * @param  i    current iterator position
 * @return      one position forward
 */
template<typename T>
ConstCircularIterator<T> next(ConstCircularIterator<T> c)
{
	ConstCircularIterator<T>  tmp(c);
	return ++tmp;
}

/**
 * Helper yields the position the iterator would have if
 * moved backward (in circular fashion) by 1 position.
 * @param  i    current iterator position
 * @return      one position backward
 */
template<typename T>
ConstCircularIterator<T> prev(ConstCircularIterator<T> c)
{
	ConstCircularIterator<T>  tmp(c);
	return --tmp;
}

} // namespace
} // namespace

#endif // end-of-include-guard
