#ifndef UTIL_CLOCK_MAN_TIMEABLE_H_INCLUDED
#define UTIL_CLOCK_MAN_TIMEABLE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence. You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Timeable.
 */

#include "ClockTraits.h"

#include <chrono>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Utility class to for timing.
 */
template<typename C = std::chrono::system_clock, typename D = typename C::duration>
class Timeable : public ClockTraits<C, D>
{
public:
	using Timings = typename ClockTraits<C, D>::CumulativeTimings;

public:
	/// Destructor virtual for polymorphic class.
	virtual ~Timeable() {}

	/// Timings in duration units specified above.
	virtual Timings GetTimings() const = 0;
};

} // namespace
} // namespace

#endif // end-of-include-guard
