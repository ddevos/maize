#ifndef UTIL_CLOCK_MAN_STOPWATCH_H_INCLUDED
#define UTIL_CLOCK_MAN_STOPWATCH_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence. You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition of Stopwatch.
 */

#include "Utils.h"

#include <chrono>
#include <string>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Provides a stopwatch interface to time: it accumulates time between start/stop pairs.
 */
template<typename T = std::chrono::system_clock>
class Stopwatch
{
public:
	typedef T TClock;

	/// Constructor initializes stopwatch.
	Stopwatch(std::string name = "stopwatch", bool running = false)
			: m_accumulated(T::duration::zero()), m_name(name), m_running(running)
	{
		if (m_running) {
			m_last_start = T::now();
		}
	}

	/// Starts stopwatch if it was stopped.
	Stopwatch& Start()
	{
		if (!m_running) {
			m_running = true;
			m_last_start = T::now();
		}
		return *this;
	}

	/// Stops the stopwatch if it was running.
	Stopwatch& Stop()
	{
		if (m_running) {
			m_accumulated += (T::now() - m_last_start);
			m_running = false;
		}
		return *this;
	}

	/// Resets stopwatch i.e. stopwatch is stopped and time accumulator is cleared.
	Stopwatch& Reset()
	{
		m_accumulated = T::duration::zero();
		m_running = false;
		return *this;
	}

	/// Reports whether stopwatch has been started.
	bool IsRunning() const
	{
		return (m_running);
	}

	/// Return name of this stopwatch
	std::string GetName() const
	{
		return m_name;
	}

	/// Returns the accumulated value without altering the stopwatch state.
	typename T::duration Get() const
	{
		auto fTemp = m_accumulated;
		if (m_running) {
			fTemp += (T::now() - m_last_start);
		}
		return fTemp;
	}

	/// Returns string representation of readout
	std::string ToString() const
	{
		using namespace std;
		using namespace std::chrono;

		string colon_string;
		typedef typename TClock::period TPeriod;
		if (ratio_less_equal<TPeriod, micro>::value) {
			microseconds d = duration_cast < microseconds > (Get());
			colon_string = Utils::ToColonString(d);
		} else if (ratio_less_equal<TPeriod, milli>::value) {
			milliseconds d = duration_cast < milliseconds > (Get());
			colon_string = Utils::ToColonString(d);
		} else {
			seconds d = duration_cast < seconds > (Get());
			colon_string = Utils::ToColonString(d);
		}
		return colon_string;
	}

private:
	typename T::duration       m_accumulated;
	typename T::time_point     m_last_start;
	std::string                m_name;
	bool                       m_running;
};

/**
 * Insert accumulated time into output stream without altering stopwatch state.
 */
template<typename T>
std::ostream&
operator<<(std::ostream& oss, Stopwatch<T> const& stopwatch) {
	return (oss << stopwatch.ToString());
}

} // namespace
} // namespace

#endif  // end of include guard
