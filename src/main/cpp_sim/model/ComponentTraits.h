#ifndef MODEL_COMPONENT_TRAITS_H_INLUDED
#define MODEL_COMPONENT_TRAITS_H_INLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Model component .
 */

#include "model/ComponentInterfaces.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <string>
#include <map>

// Notice: we want to treat each component as a different type,
// irrespective of the fact that they  happen to be implemented by
// the same type of std::function (which messes up specialization
// because the components are then strictly speaking of the same type.
// Hence the use of the Tag classes to disambiguate.

namespace SimPT_Sim {

using boost::property_tree::ptree;

class CellChemistryTag {};                    ///< Used in tag forwarding.
class CellDaughtersTag {};                    ///< Used in tag forwarding.
class CellColorTag {};                        ///< Used in tag forwarding.
class CellHousekeepTag {};                    ///< Used in tag forwarding.
class CellSplitTag {};                        ///< Used in tag forwarding.
class DeltaHamiltonianTag {};                 ///< Used in tag forwarding.
class HamiltonianTag {};                      ///< Used in tag forwarding.
class MoveGeneratorTag {};                    ///< Used in tag forwarding.
class CellToCellTransportTag {};              ///< Used in tag forwarding.
class CellToCellTransportBoundaryTag {};      ///< Used in tag forwarding.
class TimeEvolverTag {};                      ///< Used in tag forwarding.
class WallChemistryTag {};                    ///< Used in tag forwarding.

template<typename T>
struct ComponentTraits {};                    ///< Primary template never gets instantiated.

/// Traits for CellChemistry components.
template<>
struct ComponentTraits<CellChemistryTag>
{
        using ComponentType = CellChemistryComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.cell_chemistry"; }
};

/// Traits for CellColor components.
template<>
struct ComponentTraits<CellColorTag>
{
        using ComponentType = CellColorComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const ptree&)>>;
        static const std::string Label() { return "cell_color"; }
};

/// Traits for CellDaughters components.
template<>
struct ComponentTraits<CellDaughtersTag>
{
        using ComponentType = CellDaughtersComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.cell_daughters"; }
};

/// Traits for CellHousekeep components.
template<>
struct ComponentTraits<CellHousekeepTag>
{
        using ComponentType = CellHousekeepComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.cell_housekeep"; }
};

/// Traits for CellSplit components.
template<>
struct ComponentTraits<CellSplitTag>
{
        using ComponentType = CellSplitComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.cell_split"; }
};

/// Traits for CellToCellTransport components.
template<>
struct ComponentTraits<CellToCellTransportTag>
{
        using ComponentType = CellToCellTransportComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.cell2cell_transport"; }
};

/// Traits for CellToCellTransportBoundaryTag components.
template<>
struct ComponentTraits<CellToCellTransportBoundaryTag>
{
        using ComponentType = CellToCellTransportBoundaryComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.cell2cell_transport_boundary"; }
};

/// Traits for DeltaHamiltonian components.
template<>
struct ComponentTraits<DeltaHamiltonianTag>
{
        using ComponentType = DeltaHamiltonianComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.mc_hamiltonian"; }
};

/// Traits for Hamiltonian components.
template<>
struct ComponentTraits<HamiltonianTag>
{
        using ComponentType = HamiltonianComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.mc_hamiltonian"; }
};

/// Traits for MoveGenerator components.
template<>
struct ComponentTraits<MoveGeneratorTag>
{
        using ComponentType = MoveGeneratorComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.mc_move_generator"; }
};

/// Traits for TimeEvolver components.
template<>
struct ComponentTraits<TimeEvolverTag>
{
        using ComponentType = TimeEvolverComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.time_evolver"; }
};

/// Traits for WallChemistry components.
template<>
struct ComponentTraits<WallChemistryTag>
{
        using ComponentType = WallChemistryComponent;
        using MapType = std::map<const std::string, const std::function<ComponentType (const CoreData&)>>;
        static const std::string Label() { return "model.wall_chemistry"; }
};

} // namespace

#endif // end_of_include_guard
