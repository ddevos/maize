/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Proxy for dealing with the factories.
 */

#include "ComponentFactoryProxy.h"

#include "sim/CoreData.h"

#include "models/Blad/components/ComponentFactory.h"
#include "models/Default/components/ComponentFactory.h"


#include <stdexcept>

namespace SimPT_Sim {

using namespace std;

template<typename T>
typename ComponentTraits<T>::ComponentType
ComponentFactoryProxy::CreateHelper(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        auto f = m_factory->Create(cd, T());
        if (f == nullptr && default_ok) {
                f = m_default->Create(cd, T());
        }
        if (f == nullptr && throw_ok) {
                throw runtime_error("ComponentFactoryProxy::Create> Failed for " + ComponentTraits<T>::Label());
        }

        return f;
}

template<typename T>
vector<string>
ComponentFactoryProxy::ListHelper(bool default_ok) const
{
        auto vec = m_factory->List(T());
        if (default_ok) {
                const auto v = m_default->List(T());
                for (const auto& e : v) {
                        vec.push_back(e);
                }
        }
        return vec;
}

ComponentFactoryProxy::ComponentFactoryProxy(shared_ptr<ComponentFactoryBase> d, shared_ptr<ComponentFactoryBase> f)
        : m_default(d), m_factory(f)
{
}

shared_ptr<ComponentFactoryProxy>  ComponentFactoryProxy::Create(const string& group_name, bool throw_ok)
{
        const auto d = make_shared<SimPT_Default::ComponentFactory>() ;
        shared_ptr<ComponentFactoryBase> f { nullptr };
        if ( group_name == "Default" ) {
                f = make_shared<SimPT_Default::ComponentFactory>();
        } else if ( group_name == "Blad" ) {
                f = std::make_shared<SimPT_Blad::ComponentFactory>();
        }

        if (f == nullptr && throw_ok) {
                throw runtime_error(
                        "ComponentFactoryProxy::ComponetFactoryProxy()> No such factory available for " + group_name);
        }

        return shared_ptr<ComponentFactoryProxy>(new ComponentFactoryProxy(d, f));
}

CellChemistryComponent ComponentFactoryProxy::CreateCellChemistry(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<CellChemistryTag>(cd, default_ok, throw_ok);
}


CellColorComponent ComponentFactoryProxy::CreateCellColor(const string& select, const ptree& pt, bool default_ok, bool throw_ok) const
{
        auto f = m_factory->Create(select, pt, CellColorTag());
        if (f == nullptr && default_ok) {
                f = m_default->Create(select, pt, CellColorTag());
        }
        if (f == nullptr && throw_ok) {
                throw runtime_error("ComponentFactoryProxy::Create> Failed for " + ComponentTraits<CellColorTag>::Label());
        }

        return f;
}

CellDaughtersComponent ComponentFactoryProxy::CreateCellDaughters(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<CellDaughtersTag>(cd, default_ok, throw_ok);
}

CellHousekeepComponent ComponentFactoryProxy::CreateCellHousekeep(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<CellHousekeepTag>(cd, default_ok, throw_ok);
}

CellSplitComponent ComponentFactoryProxy::CreateCellSplit(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<CellSplitTag>(cd, default_ok, throw_ok);
}

CellToCellTransportComponent ComponentFactoryProxy::CreateCellToCellTransport(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<CellToCellTransportTag>(cd, default_ok, throw_ok);
}

DeltaHamiltonianComponent ComponentFactoryProxy::CreateDeltaHamiltonian(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<DeltaHamiltonianTag>(cd, default_ok, throw_ok);
}

HamiltonianComponent ComponentFactoryProxy::CreateHamiltonian(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<HamiltonianTag>(cd, default_ok, throw_ok);
}

MoveGeneratorComponent ComponentFactoryProxy::CreateMoveGenerator(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<MoveGeneratorTag>(cd, default_ok, throw_ok);
}

TimeEvolverComponent ComponentFactoryProxy::CreateTimeEvolver(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<TimeEvolverTag>(cd, default_ok, throw_ok);
}

WallChemistryComponent ComponentFactoryProxy::CreateWallChemistry(const CoreData& cd, bool default_ok, bool throw_ok) const
{
        return CreateHelper<WallChemistryTag>(cd, default_ok, throw_ok);
}

vector<string>  ComponentFactoryProxy::ListCellChemistry(bool default_ok) const
{
        return ListHelper<CellChemistryTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListCellColor(bool default_ok) const
{
        return ListHelper<CellColorTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListCellDaughters(bool default_ok) const
{
        return ListHelper<CellDaughtersTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListCellHousekeep(bool default_ok) const
{
        return ListHelper<CellHousekeepTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListCellSplit(bool default_ok) const
{
        return ListHelper<CellSplitTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListCellToCellTransport(bool default_ok) const
{
        return ListHelper<CellToCellTransportTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListDeltaHamiltonian(bool default_ok) const
{
        return ListHelper<DeltaHamiltonianTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListHamiltonian(bool default_ok) const
{
        return ListHelper<HamiltonianTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListMoveGenerator(bool default_ok) const
{
        return ListHelper<MoveGeneratorTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListTimeEvolver(bool default_ok) const
{
        return ListHelper<TimeEvolverTag>(default_ok);
}

vector<string>  ComponentFactoryProxy::ListWallChemistry(bool default_ok) const
{
        return ListHelper<WallChemistryTag>(default_ok);
}

} // namespace
