#ifndef WALL_H_INCLUDED
#define WALL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Wall.
 */

#include "bio/WallAttributes.h"
#include "util/container/SegmentedVector.h"

#include <boost/property_tree/ptree.hpp>
#include <iosfwd>
#include <tuple>
#include <vector>

namespace SimPT_Editor {
	class EditableMesh;
}

namespace SimPT_Sim {

class Cell;
class Edge;
class NeighborNodes;
class Node;

/**
 * A cell wall, runs between cell corner points and consists of wall elements.
 *
 * The end points (m_n1, m_n2) of the wall are ordered by their order in the first cell
 * and indicate begin and end point (i.e. are both are included in the wall)
 * If the wall is the boundary wall, then m_c2 (never m_c1) should be the boundary polygon.
 */
class Wall : public WallAttributes
{
public:
	///
	virtual ~Wall() {}

	///
	Cell* GetC1() const { return m_c1; }

	///
	Cell* GetC2() const { return m_c2; }

	///
	std::vector<Edge> GetEdges() const;

	///
	unsigned int GetIndex() const { return m_wall_index; }

	///
	std::array<double, 3> GetInfluxVector(Cell* c) const;

	///
	Node* GetN1() const { return m_n1; }

	///
	Node* GetN2() const { return m_n2; }
	
	///
	//double GetWallStrength() const { return m_wall_strength; }
	
	///
	std::array<double, 3> GetWallVector(Cell* c) const;

	/// Returns (and calculates, if length marked as dirty) the length along all nodes.
	double GetLength() const;

	/**
	 * Checks whether edge is part of this wall.
	 * @param    edge    the edge.
	 * @return           true iff the wall includes the edge.
	 */
	bool IncludesEdge(const Edge& edge) const;

	/**
	 * Checks whether edge is part of this wall.
	 * @param    edge    the edge.
	 * @return           true iff the wall includes the edge.
	 */
	bool IncludesNode(const Node* node) const;

	/**
	 * Checks whether a wall is at the boundary of the mesh.
	 * The cells are used as a reference, so CorrectWall is recommended when necessary.
	 * @return	True if the wall is at the boundary of the cell complex.
	 */
	bool IsAtBoundary() const;

	///
	bool IsIntersectingWithDivisionPlane(const std::array<double, 3>& p1,
					const std::array<double, 3>& p2) const;

	/// True if the Wall adheres to the SAM (shoot apical meristem)
	bool IsSam() const;

	///
	std::ostream& Print(std::ostream& os) const;

	///
	virtual void ReadPtree(boost::property_tree::ptree const& wall_pt);

	///
	void SetC1(Cell* c) { m_c1 = c; }

	///
	void SetC2(Cell* c) { m_c2 = c; }

	///
	void SetN1(Node* n) { m_n1 = n; }

	///
	void SetN2(Node* n) { m_n2 = n; }

	/// Sets the 'dirty bit' i.e. length will recalculated on first use.
	void SetLengthDirty() const { m_length_dirty = true; }

	/**
 	 * Convert the wall to a ptree.
	 * The format of a wall ptree is as follows:
	 *
	 * <id>[the index of the wall]</id>
	 * <c1>[the index of the first neighbor cell]</c1>
	 * <c2>[the index of the second neighbor cell]</c2>
	 * <n1>[the index of the first endpoint]</n1>
	 * <n2>[the index of the second endpoint]</n2>
	 * <length>[the length of the wall]</length>
	 * <attributes>
	 * 	WallAttributes::ToPtree()
	 * </attributes>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	friend class Mesh;
	template<typename T, size_t N>
	friend class SimPT_Sim::Container::SegmentedVector;
	friend class SimPT_Editor::EditableMesh;

private:
	Wall(unsigned int index, Node* n1, Node* n2, Cell* c1, Cell* c2, unsigned int num_chem);
	Wall(const Wall& src);
	Wall& operator=(Wall const&);

private:
	/// Does the actual calculation of the wall length
	void CalculateLength() const;

private:
	Cell*             m_c1;                ///<
	Cell*             m_c2;                ///<
	Node*             m_n1;                ///<
	Node*             m_n2;                ///<
	unsigned int      m_wall_index;        ///<

private:
	mutable double    m_length;            ///< Length is tracked lazily.
	mutable bool      m_length_dirty;      ///< Marks length dirty.
};


std::ostream& operator<<(std::ostream& os, const Wall& w);

}

#endif // end_of_include_guard
