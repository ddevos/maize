/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CellAttributes.
 */

#include "CellAttributes.h"

#include "BoundaryType.h"
#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

#include <iostream>

namespace SimPT_Sim {

using namespace std;
using namespace boost::property_tree;
using boost::optional;
using namespace SimPT_Sim::Util;

CellAttributes::CellAttributes(unsigned int chem_count)
	: m_boundary_type(BoundaryType::None), m_cell_type(0),
	  m_chem(chem_count), m_dead(false), m_div_counter(0), m_div_time(10000), m_fixed(false),
	  m_solute(0.0), m_stiffness(0.0), m_target_area(1.0), m_target_length(0.0)
{
}

double CellAttributes::GetChemical(unsigned int c) const
{
	assert( (c < m_chem.size()) && "CellAttributes::GetChemical> chemical index out of bounds!");
	return m_chem[c];
}

ostream& CellAttributes::Print(ostream& os) const
{
	os << "CellAttributes: {" << endl;
	os << "\t chemicals: [ ";
	for (auto const& c : m_chem) {
		os << c << " , ";
	}
	os << " ]" << endl;
	os << "\t div_counter  = " << m_div_counter << endl;
	os << "\t cell_type    = " << m_cell_type << endl;
	os << "}";
	return os;
}

void CellAttributes::ReadPtree(const ptree& cell_pt)
{
	try {
		m_boundary_type      = FromString<BoundaryType>(cell_pt.get<string>("boundary", "None"));
		m_cell_type          = cell_pt.get<unsigned int>("type");
		m_dead               = cell_pt.get<bool>("dead");
		m_div_counter        = cell_pt.get<unsigned int>("division_count", 0U);
		m_fixed              = cell_pt.get<bool>("fixed");
		m_solute             = cell_pt.get<double>("solute");
		m_stiffness          = cell_pt.get<double>("stiffness");
		m_target_area        = cell_pt.get<double>("target_area");
		m_target_length      = cell_pt.get<double>("target_length");

		optional<ptree const&> chemical_array_pt = cell_pt.get_child_optional("chemical_array");
		if (chemical_array_pt) {
			assert( (m_chem.size() == chemical_array_pt->size())
				&& "size of chemical_array in xml file does not match chemical_count");
			unsigned int chem_i = 0;
			for (auto const& chem_v : *chemical_array_pt) {
				m_chem[chem_i++] = chem_v.second.get_value<double>();
			}
		}
	}
	catch(exception& e) {
		const string here = string(VL_HERE) + " exception:\n";
		throw Exception(here + e.what());
	}
}

void CellAttributes::SetChemical(unsigned int c, double conc)
{
	assert( (c < m_chem.size()) && "CellAttributes::SetChemical> chemical index out of bounds!");
	m_chem[c] = conc;
}

void CellAttributes::SetChemicals(vector<double> const& chem)
{
	assert((chem.size() == m_chem.size()) && "CellAttributes::SetChemicals> vector size inconsistent!");
	m_chem = chem;
}

ptree CellAttributes::ToPtree() const
{
	ptree ret;
	ret.put("boundary", ToString(m_boundary_type));
	ret.put("dead", m_dead);
	ret.put("division_count", m_div_counter);
	ret.put("fixed", m_fixed);
	ret.put("solute", m_solute);
	ret.put("stiffness", m_stiffness);
	ret.put("target_area", m_target_area);
	ret.put("target_length", m_target_length);
	ret.put("type", m_cell_type);

	for (const auto& chem : m_chem) {
		ret.add("chemical_array.chemical", chem);
	}

	return ret;
}

ostream& operator<<(ostream& os, CellAttributes const& c)
{
    c.Print(os);
    return os;
}

} // namespace
