/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of PBMBuilder.
 */

#include "PBMBuilder.h"

#include "Cell.h"
#include "Mesh.h"
#include "Node.h"
#include "WallType.h"

#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <array>
#include <iostream>
#include <list>

namespace SimPT_Sim { class Wall; }

using namespace std;
using namespace SimPT_Sim;
using namespace boost::property_tree;
using boost::optional;

namespace SimPT_Sim {

std::shared_ptr<Mesh> PBMBuilder::Build(const ptree& mesh_pt)
{
	auto cells_pt      = mesh_pt.get_child("cells");
	const auto& nodes_pt      = mesh_pt.get_child("nodes");
	const auto& walls_pt      = mesh_pt.get_child("walls");

	// Set up empty mesh.
	unsigned int num_chem     = cells_pt.get<unsigned int>("chemical_count");
	m_mesh = make_shared<Mesh>(num_chem);

	// Nodes
	BuildNodes(nodes_pt);

	// Cells
	BuildCells(cells_pt);
	BuildBoundaryPolygon(cells_pt);
	assert((m_mesh->GetCells().size() == cells_pt.get<unsigned int>("count")) && "CellCount does match with file!");

	// Allocate Walls (we need to have read the cells before constructing walls)
	BuildWalls(walls_pt);
	ConnectCellsToWalls(cells_pt);

	m_mesh->ConstructNeighborList(m_mesh->GetBoundaryPolygon());
	m_mesh->UpdateNodeOwningNeighbors(m_mesh->GetBoundaryPolygon());
	for (auto cell : m_mesh->GetCells()) {
		m_mesh->ConstructNeighborList(cell);
		m_mesh->UpdateNodeOwningNeighbors(cell);
	}

	return m_mesh;
}

void PBMBuilder::BuildBoundaryPolygon(const ptree& cells_pt)
{
	const auto& boundary_pt = cells_pt.get_child("boundary_polygon");
	{
		vector<unsigned int> node_ids;
		auto const& node_array_pt = boundary_pt.get_child("node_array");
		for (auto const& node_v : node_array_pt) {
			auto const& node_pt = node_v.second;
			node_ids.push_back(node_pt.get_value<unsigned int>());
		}
		auto boundary_polygon = m_mesh->BuildBoundaryPolygon(node_ids);
		boundary_polygon->ReadPtree(boundary_pt);
	}
}

void PBMBuilder::BuildCells(const ptree& cells_pt)
{
	for (auto cell_v : cells_pt.get_child("cell_array")) {
		auto cell_pt = cell_v.second;
		unsigned int id = cell_pt.get<unsigned int>("id");

		vector<unsigned int> node_ids;
		auto const& node_array_pt = cell_pt.get_child("node_array");
		for (auto const& node_v : node_array_pt) {
			auto const& node_pt = node_v.second;
			node_ids.push_back(node_pt.get_value<unsigned int>());
		}

		auto new_cell = m_mesh->BuildCell(id, node_ids);
		new_cell->ReadPtree(cell_pt);
	}
}

void PBMBuilder::BuildNodes(const ptree& nodes_pt)
{
	for (const auto& node_v : nodes_pt.get_child("node_array")) {
		const ptree& node_pt = node_v.second;
		unsigned int id = node_pt.get<unsigned int>("id");
		array<double, 3> arr {{ node_pt.get<double>("x"), node_pt.get<double>("y"), 0.0 }};
		const bool at_boundary = node_pt.get<bool>("boundary");
		auto new_node = m_mesh->BuildNode(id, arr, at_boundary);
		new_node->ReadPtree(node_pt);
	}
}

void PBMBuilder::BuildWalls(const ptree& walls_pt)
{
	optional<const ptree&> wall_array_pt = walls_pt.get_child_optional("wall_array");
	if (wall_array_pt) {
		for (const auto& wall_v : *wall_array_pt) {
			const ptree& wall_pt = wall_v.second;
			const unsigned int wall_id = wall_pt.get<unsigned int>("id");

			auto& mesh_cells = m_mesh->GetCells();
			int const c1_id = wall_pt.get<int>("c1");
			int const c2_id = wall_pt.get<int>("c2");
			auto c1 = (c1_id != -1) ? mesh_cells[c1_id] : m_mesh->GetBoundaryPolygon();
			auto c2 = (c2_id != -1) ? mesh_cells[c2_id] : m_mesh->GetBoundaryPolygon();

			auto& mesh_nodes = m_mesh->GetNodes();
			unsigned int const n1_id = wall_pt.get<unsigned int>("n1");
			unsigned int const n2_id = wall_pt.get<unsigned int>("n2");
			auto n1 = mesh_nodes[n1_id];
			auto n2 = mesh_nodes[n2_id];

			auto wall = m_mesh->BuildWall(wall_id, n1, n2, c1, c2);
			wall->ReadPtree(wall_pt);
		}
	}
}

void PBMBuilder::ConnectCellsToWalls(const ptree& cells_pt)
{
	auto& mesh_cells = m_mesh->GetCells();
	auto& mesh_walls = m_mesh->GetWalls();

	const ptree& cell_array_pt = cells_pt.get_child("cell_array");
	for (const auto & cell_v : cell_array_pt) {
		const unsigned int id = cell_v.second.get<unsigned int>("id");
		optional<const ptree&> wall_array_pt = cell_v.second.get_child_optional("wall_array");
		if (wall_array_pt) {
			for (const auto& wall_v : *wall_array_pt) {
				const unsigned int w_id = wall_v.second.get_value<unsigned int>();
				const auto eql = [w_id](Wall* w) {
					return w_id == w->GetIndex();
				};
				auto it = find_if(mesh_walls.begin(), mesh_walls.end(), eql);
				assert((it != mesh_walls.end()) && "Cannot find the cell wall!");
				mesh_cells[id]->GetWalls().push_back((*it));
			}
		}
	}

	optional<const ptree&> bp_wall_array_pt = cells_pt.get_child_optional("boundary_polygon.wall_array");
	if (bp_wall_array_pt) {
		for (const auto& wall_v : *bp_wall_array_pt) {
			const unsigned int w_id = wall_v.second.get_value<unsigned int>();
			const auto eql = [w_id](Wall* w) {
				return w_id == w->GetIndex();
			};
			auto it = find_if(mesh_walls.begin(), mesh_walls.end(), eql);
			assert((it != mesh_walls.end()) && "Cannot find the boundary wall!");
			m_mesh->GetBoundaryPolygon()->GetWalls().push_back((*it));
		}
	}

	for (auto &wall : m_mesh->GetWalls()) {
		m_mesh->UpdateNodeOwningWalls(wall);
	}
}

}
