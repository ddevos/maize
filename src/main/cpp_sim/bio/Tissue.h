#ifndef TISSUE_H_INCLUDED
#define TISSUE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Tissue components used during model execution.
 */

#include <memory>

namespace SimPT_Sim {

class AttributeStore;
class Mesh;

/**
 * Tissue data with mesh, cell attributes, node attributes and wall attributes.
 */
class Tissue
{
public:
	Tissue() = default;

	std::shared_ptr<SimPT_Sim::Mesh>   m_mesh;
	std::shared_ptr<AttributeStore>    m_cell_attributes;
	std::shared_ptr<AttributeStore>    m_node_attributes;
	std::shared_ptr<AttributeStore>    m_wall_attributes;

	bool Check() const
	{
		return (m_mesh != nullptr) && (m_cell_attributes != nullptr)
			&& (m_node_attributes != nullptr) && (m_wall_attributes != nullptr);
	}
};

} // namespace

#endif // end_of_include_guard
