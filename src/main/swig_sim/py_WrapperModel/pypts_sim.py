# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.

import logging
import numpy as np
import scipy as sp
import scipy.integrate as spi
from pypts.tissue import Tissue

class ModelDiffusion(object):
    """
    This is a simple system of cell-based equations that models basic diffusion
    of a chemical morphogen substance in a static network of cells.
    """

    # The default set of parameters for the model
    params_default = {
        'mu_0': 0.01,                     # chem_0 decay
        'mu_1': 0.002,                    # chem_1 decay
        'rho_0': 1.0,                     # chem_0 production
        'rho_1': 0.0,                     # chem_1 production
        'D_0': 100.0,                     # chem_0 diffusion
        'D_1': 100.0,                     # chem_1 diffusion
        'chem0_prod_threshold': 2.0       # produce chem_0 iff chem_1
                                          # concentr. is below this threshold
    }

    def __init__(self, tissue, params={}):
        """
        Init. the model with a dictionary of parameters and a tissue of cells
        The default set of parameters is updated with any additional parameters
        passed as an argument.
        """
        self.tissue = tissue
        self.params = self.params_default
        self.params.update(params)

    def get_variables(self):
        """
        Gathers the variables from the system into one large vector and returns
        that one.
        """
        # Get values of cell variables
        # first num_cells values are chem0, last num_cells values are chem1
        c_chem0 = self.tissue.cells_attributes['chem_0']
        c_chem1 = self.tissue.cells_attributes['chem_1']
        return np.concatenate((c_chem0, c_chem1), axis=0)

    def set_variables(self, u):
        """
        Stores the system's variables inside the tissue
        """
        n_c = self.tissue.num_cells
        # first n_c values are chem0, last n_c values are chem1
        self.tissue.cells_attributes['chem_0'] = u[:n_c]
        self.tissue.cells_attributes['chem_1'] = u[n_c:]

    def get_residual(self, u):
        """
        Evaluates the function f(u) = du/dt = 'Whatever equations'
        where u = [c_chem0, c_chem1] is the vector of cell-based morphogen
        values of total length 2*num_cells
        """
        # Get short notations for model parameters
        mu_0 = self.params['mu_0'] # Decay coefficient
        mu_1 = self.params['mu_1'] # Decay coefficient
        rho_0 = self.params['rho_0'] # Production coefficient
        rho_1 = self.params['rho_1'] # Production coefficient
        D_0 = self.params['D_0'] # Diffusion coefficient
        D_1 = self.params['D_1'] # Diffusion coefficient
        thr = self.params['chem0_prod_threshold']
        src_cells = self.params['src_cells']

        # Number of cells
        n_c = self.tissue.num_cells
        n_w = self.tissue.num_walls

        # Cell-based morphogen values extracted from u
        c_chem0 = u[:n_c]
        c_chem1 = u[n_c:]

        # Stores the calculated values d(c_chem0)/dt and d(c_chem1)/dt
        dc_chem0dt = np.zeros(n_c)
        dc_chem1dt = np.zeros(n_c)

        # Precalc wall lengths for efficiency
        walls_length = np.zeros(n_w)
        for w_idx in self.tissue.walls_idx:
            walls_length[w_idx] = self.tissue.get_length_of_wall(w_idx)

        # Precalc cell areas for efficiency
        cells_areas = np.zeros(n_c)
        for c_idx in self.tissue.cells_idx:
            cells_areas[c_idx] = self.tissue.get_area_of_cell(c_idx)

        # Loop over the cells (using index values)
        for c_idx in self.tissue.cells_idx:
            if c_idx not in src_cells:
                # Chem 0 is produced iff chem 1 is below a specific threshold
                c_chem0_production = 0.0
                if (c_chem1[c_idx] / cells_areas[c_idx]) < thr:
                    c_chem0_production = cells_areas[c_idx] * rho_0
                c_chem1_production = 0.0
                # Chem 0 and 1 decay
                c_chem0_decay = mu_0 * c_chem0[c_idx]
                c_chem1_decay = mu_1 * c_chem1[c_idx]
                # Diffusion, needs looping over nbrs
                c_chem0_passive_trans = 0.0
                c_chem1_passive_trans = 0.0
                
                # Accumulate pass. mgen transport over neighbors (indexed by j)
                # (loop over current cell's walls and its corresp. neighbor
                # cells simultaneously since access to both is necessary)
                for w_id,c_jd in zip(self.tissue.cells_walls[c_idx],
                                     self.tissue.cells_cells[c_idx]):
                    # Ignore boundary polygon as neighbor
                    if -1 != c_jd:
                        w_idx = self.tissue.walls_idx[w_id]
                        c_jdx = self.tissue.cells_idx[c_jd]
                        l_ij = walls_length[w_idx]
                        c_chem0_passive_trans += l_ij * (
                            c_chem0[c_jdx] / cells_areas[c_jdx] -
                            c_chem0[c_idx] / cells_areas[c_idx]
                        )
                        c_chem1_passive_trans += l_ij * (
                            c_chem1[c_jdx] / cells_areas[c_jdx] -
                            c_chem1[c_idx]/cells_areas[c_idx]
                        )

                # Total mgen for current cell:
                dc_chem0dt[c_idx] = (
                    c_chem0_production -
                    c_chem0_decay +
                    D_0 * c_chem0_passive_trans
                )
                dc_chem1dt[c_idx] = (
                    c_chem1_production -
                    c_chem1_decay +
                    D_1 * c_chem1_passive_trans
                )
            else:
                dc_chem0dt[c_idx] = 0.0
                dc_chem1dt[c_idx] = 0.0

        return np.concatenate((dc_chem0dt, dc_chem1dt), axis=0)

