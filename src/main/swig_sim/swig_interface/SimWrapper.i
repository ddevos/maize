/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * SWIG interface for the SimWrapper.
 */

%module simPT

%include "std_string.i"
%include "SimState.i"
%{
#include "sim/SimWrapper.h"
%}

%include "../../cpp_sim/sim/SimWrapper.h"
%template(ResultSimState) SimPT_Sim::SimWrapperResult<SimPT_Sim::SimState>;
%template(ResultString)   SimPT_Sim::SimWrapperResult<std::string>;
%template(ResultVoid)     SimPT_Sim::SimWrapperResult<void>;
