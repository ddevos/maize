#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

set( CMAKE_CXX_FLAGS       "${CMAKE_CXX_FLAGS} -w" )

#============================================================================
# Build static lib
#============================================================================
set( SOURCES
         lcg64.cc
         lcg64_shift.cc
         minstd.cc
         mrg2.cc
         mrg3.cc
         mrg3s.cc
         mrg4.cc
         mrg5.cc
         mrg5s.cc
         mt19937.cc
         mt19937_64.cc
         yarn2.cc
         yarn3.cc
         yarn3s.cc
         yarn4.cc
         yarn5.cc
         yarn5s.cc
)

add_library( trng4 STATIC ${SOURCES} )	

#############################################################################
