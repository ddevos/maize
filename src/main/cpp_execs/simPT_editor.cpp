/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for the simPT tissue editor.
 */


#include "../cpp_simshell/common/InstallDirs.h"
#include "editor/TissueEditor.h"
#include "util/clock_man/Timeable.h"

#include <QApplication>
#include <QMessageBox>

#include <iostream>
#include <stdexcept>

using namespace std;
using namespace SimPT_Sim::ClockMan;

int main(int argc, char **argv)
{
	int exit_status = EXIT_SUCCESS;

	// qApp: closing last window quits app and exits app process.
	QApplication app(argc, argv, true);
	qRegisterMetaType<std::string>("std::string");
	qRegisterMetaType<Timeable<>::Stopclock::TClock::duration>("Timeable<>::Stopclock::TClock::duration");
	QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));

	// Icon search path.
	QStringList search_paths = QIcon::themeSearchPaths();
	search_paths.push_back(QString::fromStdString(SimShell::InstallDirs::GetDataDir() + "/icons"));
	QIcon::setThemeSearchPaths(search_paths);
	QIcon::setThemeName("Tango");

	try {
		SimPT_Editor::TissueEditor editor;
		editor.show();
		exit_status = app.exec();
	}
	catch (exception& e) {
		cerr << e.what() << endl;
		QString qmess = QString("Exception caught: %1").arg(e.what());
		QMessageBox::critical(0, "Critical Error", qmess,
			QMessageBox::Abort, QMessageBox::NoButton, QMessageBox::NoButton);
		exit_status = EXIT_FAILURE;
	}
	catch (...) {
		cerr << "Unknown exception." << endl;
		QString qmess = QString("Unknown exception.");
		QMessageBox::critical(0, "Critical Error", qmess);
		exit_status = EXIT_FAILURE;
	}

	return exit_status;
}
