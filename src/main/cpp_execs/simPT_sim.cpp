/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for main program gui and cli mode.
 */

#include "modes/mode_manager.h"

#include <boost/functional/value_factory.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace Modes;
using namespace std;

struct sim {
	static const ModeManager<sim>::MapType    modes;
	static const std::string                  default_mode;
	static const std::string                  application_name;
};

const ModeManager<sim>::MapType sim::modes =
	{{
		make_pair("gui", boost::value_factory<simPTGUIMode>()),
		make_pair("cli", boost::value_factory<simPTCLIMode>())
	}};

const std::string sim::default_mode      = "gui";
const std::string sim::application_name  = "simPT_sim";

int main(int argc, char** argv)
{
	return ModeManager<sim>::main(argc,argv);
}
