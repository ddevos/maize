/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ExplorationProgress
 */

#include "ExplorationProgress.h"

#include "Exploration.h"
#include "ExplorationTask.h"
#include "FileExploration.h"
#include "ParameterExploration.h"
#include "SimResult.h"
#include "SimTask.h"

#include <cassert>

using boost::property_tree::ptree;

namespace SimPT_Parex {

const unsigned int ExplorationProgress::g_max_tries;

ExplorationProgress::ExplorationProgress(const Exploration* exploration)
	: m_exploration(exploration), m_tasks(m_exploration->GetNumberOfTasks())
{
	assert(exploration != nullptr && "The given exploration isn't initialized.");

	m_task_counts[TaskState::Waiting] = m_tasks.size();
	for (unsigned int i = 0; i < m_tasks.size(); ++i) {
		m_send_queue.push_back(i);
	}
}

ExplorationProgress::ExplorationProgress(const ptree& pt)
	: m_exploration(nullptr)
{
	ReadPtree(pt);
}

ExplorationProgress::~ExplorationProgress()
{
	delete m_exploration;
}

void ExplorationProgress::CancelWaitingTask(unsigned int id)
{
	assert(id < m_tasks.size() && "This task doesn't exist.");
	assert(m_tasks[id].GetState() == TaskState::Waiting && "This task isn't in a 'Waiting' state.");

	m_send_queue.remove(id);
	ChangeState(m_tasks[id], TaskState::Cancelled);
}

void ExplorationProgress::ChangeState(ExplorationTask& task, TaskState state)
{
	m_task_counts[state] += 1;
	m_task_counts[task.GetState()] -= 1;
	task.ChangeState(state);

	emit Updated();
}

const ExplorationTask& ExplorationProgress::GetTask(unsigned int id) const
{
	assert(id < m_exploration->GetNumberOfTasks() && "Id/Index out of bounds");

	return m_tasks[id];
}

unsigned int ExplorationProgress::GetTaskCount(const TaskState& state) const
{
	auto found = m_task_counts.find(state);

	if (found == m_task_counts.end()) {
		return 0;
	}
	else {
		return found->second;
	}
}

void ExplorationProgress::GiveBack(const SimTask* task)
{
	assert(m_tasks[task->GetId()].GetState() == TaskState::Running && "This task isn't in a 'Running' state.");

	unsigned int id = task->GetId();
	ChangeState(m_tasks[id], TaskState::Waiting);
	m_running_tasks.remove(id);
	m_send_queue.push_front(id);

	delete task;
}

void ExplorationProgress::HandleResult(const SimResult& result)
{
	ExplorationTask& taskInfo = m_tasks.at(result.GetTaskId());

	switch (result.GetResult()) {
	case SimResult::ResultType::Success:
		taskInfo.setNodeThatContainsResult(result.GetNodeIP(), result.GetNodePort());
		
		ChangeState(taskInfo, TaskState::Finished);
		break;

	case SimResult::ResultType::Failure:
		if (taskInfo.GetNumberOfTries() < g_max_tries) {
			m_send_queue.push_back(result.GetTaskId());
			ChangeState(taskInfo, TaskState::Waiting);
			taskInfo.IncrementTries();
		} else {
			ChangeState(taskInfo, TaskState::Failed);
		}
		break;

	case SimResult::ResultType::Stopped:
		ChangeState(taskInfo, TaskState::Cancelled);
		break;
	}
}

bool ExplorationProgress::IsFinished() const
{
	return m_send_queue.empty() && m_running_tasks.empty();
}

SimTask* ExplorationProgress::NextTask()
{
	if (m_send_queue.size() == 0)
		return nullptr;

	unsigned int id = m_send_queue.front();
	m_send_queue.pop_front();
	m_running_tasks.push_back(id);

	SimTask* task = m_exploration->CreateTask(id);
	ChangeState(m_tasks[id], TaskState::Running);

	return task;
}

void ExplorationProgress::ReadPtree(const ptree& pt)
{
	delete m_exploration;

	m_exploration = nullptr;
	if (pt.find("parameter_exploration") != pt.not_found()) {
		m_exploration = new ParameterExploration(pt.get_child("parameter_exploration"));
	} else if (pt.find("file_exploration") != pt.not_found()) {
		m_exploration = new FileExploration(pt.get_child("file_exploration"));
	} else {
		assert(false && "Exploration type unknown.");
	}

	ptree tasks_pt = pt.get_child("tasks");
	m_tasks = std::vector<ExplorationTask>(tasks_pt.size());
	m_task_counts.clear();

	unsigned int i = 0;
	for (const auto& pair : tasks_pt) {
		ExplorationTask task(pair.second);

		m_tasks[i] = task;
		m_task_counts[task.GetState()] += 1;

		if (task.GetState() == TaskState::Running) {
			m_running_tasks.push_back(i);
		}
		i++;
	}

	m_send_queue.clear();
	const auto &send_queue_pt = pt.get_child_optional("send_queue");
	if (send_queue_pt) {
		for (const auto& pair : send_queue_pt.get()) {
			m_send_queue.push_back(pair.second.get_value<unsigned int>());
		}
	}
}

void ExplorationProgress::ResendCancelledTask(unsigned int id)
{
	assert(id < m_tasks.size() && "This task doesn't exist.");
	assert(m_tasks[id].GetState() == TaskState::Cancelled && "This task isn't in a 'Cancelled' state.");

	m_send_queue.push_back(id);
	ChangeState(m_tasks[id], TaskState::Waiting);
}

ptree ExplorationProgress::ToPtree() const
{
	ptree pt;

	if (dynamic_cast<const ParameterExploration*>(m_exploration) != nullptr) {
		pt.put_child("parameter_exploration", m_exploration->ToPtree());
	} else if (dynamic_cast<const FileExploration*>(m_exploration) != nullptr) {
		pt.put_child("file_exploration", m_exploration->ToPtree());
	} else {
		assert(false && "Exploration type unknown.");
	}

	for (const ExplorationTask& task : m_tasks) {
		pt.add_child("tasks.task", task.ToPtree());
	}
	for (unsigned int task_id : m_send_queue) {
		pt.add("send_queue.task_id", task_id);
	}

	return pt;
}

} // namespace
