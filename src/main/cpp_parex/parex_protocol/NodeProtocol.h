#ifndef SIMPT_PAREX_NODE_PROTOCOL_H_
#define SIMPT_PAREX_NODE_PROTOCOL_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NodeProtocol
 */

#include "Protocol.h"

class QTcpSocket;

namespace SimPT_Parex {

class Connection;
class SimResult;
class SimTask;

/**
 * Class that implements the node version of the protocol.
 */
class NodeProtocol : public Protocol
{
	Q_OBJECT
public:
	/// Constructor
	NodeProtocol(QTcpSocket *socket, QObject *parent = 0);

	///Destructor
	virtual ~NodeProtocol();

public slots:
	/// Sends the result of a simulation back to the server
	void SendSimResult(const SimResult& result);

signals:
	/// Emitted to indicate an exploration must be deleted from the disk.
	void Delete(const std::string &name) const;

	/// Signal emitted when a SimTask is received
	void TaskReceived(const SimTask *task) const;

	/// Signal emitted when a SimTask is successfully sent.
	void SuccessfullySent() const;

	/// Emitted to indicate the current task must be stopped.
	void StopTask() const;

protected:
	/// Receive a ptree message (implementats Protocol::ReceivePtree).
	virtual void ReceivePtree(const boost::property_tree::ptree &reader);
};

} // namespace

#endif // end-of-include-guard
