/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Exploration.
 */

#include "Exploration.h"

using boost::property_tree::ptree;

namespace SimPT_Parex {

Exploration::Exploration(const std::string& name, const ptree& preferences)
	: m_name(name), m_preferences(preferences)
{
}

Exploration::Exploration(const ptree& pt)
{
	ReadPtree(pt);
}

Exploration::Exploration(const Exploration& other)
	: m_name(other.m_name), m_preferences(other.m_preferences)
{
}

Exploration& Exploration::operator=(const Exploration& other)
{
	if (this != &other) {
		m_name = other.m_name;
		m_preferences = other.m_preferences;
	}

	return *this;
}

ptree Exploration::ToPtree() const
{
	ptree pt;
	pt.put("name", m_name);
	pt.put_child("preferences", m_preferences);
	return pt;
}

void Exploration::ReadPtree(const ptree& pt)
{
	m_name = pt.get<std::string>("name");
	m_preferences = pt.get_child("preferences");
}

} // namespace
