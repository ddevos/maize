#ifndef SIMPT_PAREX_SIM_RESULT_H_INCLUDED
#define SIMPT_PAREX_SIM_RESULT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SimResult.
 */

#include <string>

namespace SimPT_Parex {

/**
 * A container class for the final result of a simulation.
 */
class SimResult
{
public:
	enum class ResultType
	{
		Success = 0,
		Failure = 1,
		Stopped = 2
	};

	/// Default constructor, needed by Qt signals
	SimResult();

	/// A basic constructor
	SimResult(const std::string& explorationName, int taskId, ResultType result,
	        const std::string& nodeIP="", int nodePort=0);

	/// Returns the name of the exploration of the finished task
	std::string GetExplorationName() const { return m_exploration_name; }

	/// Return the id of the finished task
	int GetTaskId() const { return m_task_id; }

	/// Check if the simulation was successfully completed.
	ResultType GetResult() const { return m_success; }

	/// Returns the IP of the node that has the result
	std::string GetNodeIP() const { return m_node_ip; }

	/// Returns the Port of the TCP server of the node that has the result
	int GetNodePort() const { return m_node_port; }

private:
	std::string     m_exploration_name;
	ResultType      m_success;
        int             m_task_id;

private:
	std::string     m_node_ip;
	int             m_node_port;
};

} // namespace

#endif // end-of-include-guard
