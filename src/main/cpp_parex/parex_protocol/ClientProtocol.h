#ifndef SIMPT_PAREX_CLIENT_PROTOCOL_H_
#define SIMPT_PAREX_CLIENT_PROTOCOL_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ClientProtocol
 */

#include "Protocol.h"

#include <vector>
#include <QObject>

namespace SimPT_Parex {

class Exploration;
class ExplorationProgress;
class ServerInfo;

/**
 * Client side of the protocol
 */
class ClientProtocol: public Protocol
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param socket	The socket to connect through
	 */
	ClientProtocol(ServerInfo *server, QObject *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~ClientProtocol() {}

	/**
	 * Send exploration
	 *
	 * @param exploration		The Exploration to send
	 */
	bool SendExploration(const Exploration *exploration);

	/**
	 * Request a stop and delete of the given exploration
	 *
	 * @param name		The name of the exploration to stop and delete
	 */
	bool DeleteExploration(const std::string &name);

	/**
	 * Request a list of current Explorations
	 */
	bool RequestExplorationNames();

	/**
	 * Subscribe for regular updates
	 *
	 * @param exploration_name	The name of the exploration to subscribe to
	 */
	bool SubscribeUpdates(const std::string &explorationName);

	/**
	 * Unsubscribe for regular updates
	 *
	 * @param exploration_name 	The name of the exploration to unsubscribe
	 */
	bool UnsubscribeUpdates(const std::string &explorationName);

	/**
	 * Request to stop a task
	 */
	bool StopTask(const std::string &name, int task);

	/**
	 * Request to restart a task
	 */
	bool RestartTask(const std::string &name, int task);

signals:
	/**
	 * Signal emitted when exploration names are received
	 */
	void ExplorationNames(const std::vector<std::string> &names) const;

	/**
	 * Signal emitted when new status update is received
	 */
	void ExplorationStatus(const ExplorationProgress* exploration) const;

	/**
	 * Signal emitted when subscribed exploration has been deleted
	 */
	void ExplorationDeleted() const;

protected:
	/**
	 * Receive a ptree message
	 * Implementation of Protocol::ReceivePtree
	 *
	 * @param	reader	The received pt
	 */
	virtual void ReceivePtree(const boost::property_tree::ptree &reader);
};

} // namespace

#endif // end-of-include-guard
