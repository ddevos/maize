/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimResult.
 */

#include "SimResult.h"

namespace SimPT_Parex {

SimResult::SimResult()
	: m_exploration_name(""), m_success(ResultType::Failure), m_task_id(-1), m_node_port(0)
{
}

SimResult::SimResult(const std::string &explorationName, int taskId,
                ResultType result, const std::string &nodeIP, int nodePort)
	: m_exploration_name(explorationName), m_success(result), m_task_id(taskId),
	          m_node_ip(nodeIP), m_node_port(nodePort)
{
}

} // namespace
