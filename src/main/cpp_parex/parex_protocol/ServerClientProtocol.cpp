/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ServerClientProtocol
 */

#include "ServerClientProtocol.h"

#include "Exploration.h"
#include "ExplorationProgress.h"
#include "FileExploration.h"
#include "ParameterExploration.h"
#include "Protocol.h"
#include "util/misc/StringUtils.h"

class QTcpSocket;

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Parex {

ServerClientProtocol::ServerClientProtocol(QTcpSocket *socket, QObject *parent)
	: Protocol(socket, parent)
{
}

ServerClientProtocol::~ServerClientProtocol()
{
}

void ServerClientProtocol::SendAck(const std::string &name)
{
	ptree writer;
	writer.put("Exploration.Name", name);
	SendPtree(writer);
}

void ServerClientProtocol::SendExplorationNames(const std::vector<std::string> &names)
{
	ptree writer;
	writer.put("Control.ExplorationNames.size", names.size());
	int i = 0;
	for (auto name : names) {
		writer.put("Control.ExplorationNames.exploration" + to_string(i) + ".name", name);
		i++;
	}
	SendPtree(writer);
}

void ServerClientProtocol::SendStatus(const ExplorationProgress &status)
{
	ptree writer;
	writer.put_child("Control.Status.exploration_progress", status.ToPtree());
	SendPtree(writer);
}

void ServerClientProtocol::SendStatusDeleted(const std::string &name)
{
	ptree writer;
	writer.put("Control.Status.name", name);
	writer.put("Control.Status.deleted", "");
	SendPtree(writer);
}

void ServerClientProtocol::ReceivePtree(const ptree &reader)
{
	if (reader.find("parameter_exploration") != reader.not_found()) {
		Exploration* exploration = new ParameterExploration(reader.get_child("parameter_exploration"));
		exploration->SetName(exploration->GetName() + QDateTime::currentDateTime().toString("_yyyy_MM_dd_HH_mm_ss_zzz").toStdString());

		emit ExplorationReceived(exploration);
	} else if (reader.find("file_exploration") != reader.not_found()) {
		Exploration* exploration = new FileExploration(reader.get_child("file_exploration"));
		exploration->SetName(exploration->GetName() + QDateTime::currentDateTime().toString("_yyyy_MM_dd_HH_mm_ss_zzz").toStdString());

		emit ExplorationReceived(exploration);
	} else if (reader.find("Control") != reader.not_found()) {
		ptree control_pt = reader.get_child("Control");

		if (control_pt.find("Subscribe") != control_pt.not_found()) {
			emit Subscribe(control_pt.get<std::string>("Subscribe"));
		} else if (control_pt.find("Unsubscribe") != control_pt.not_found()) {
			emit Unsubscribe(control_pt.get<std::string>("Unsubscribe"));
		} else if (control_pt.find("Stop") != control_pt.not_found()) {
			emit StopTask(control_pt.get<std::string>("Stop.name"), control_pt.get<int>("Stop.id"));
		} else if (control_pt.find("Start") != control_pt.not_found()) {
			emit RestartTask(control_pt.get<std::string>("Start.name"), control_pt.get<int>("Start.id"));
		} else if (control_pt.find("Delete") != control_pt.not_found()) {
			emit DeleteExploration(control_pt.get<std::string>("Delete"));
		} else if (control_pt.find("RequestExplorations") != control_pt.not_found()) {
			emit ExplorationNamesRequested();
		}
	}
}

} // namespace
