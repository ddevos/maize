#ifndef SIMPT_PAREX_SIM_TASK_H_
#define SIMPT_PAREX_SIM_TASK_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SimTask.
 */

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <QObject>

namespace SimPT_Parex {

/**
 * Contains all information needed for a transmitable simulation task.
 */
class SimTask: public QObject
{
	Q_OBJECT
public:
	/// Default constructor.
	SimTask();

	/// Constructor for only tree and name (string format).
	SimTask(int, std::string tree, std::string name);

	/// Constructor for tree, workspace and name (string format).
	SimTask(int, std::string tree, std::string workspace, std::string name);

	/// Constructor for only tree and name (ptree format).
	SimTask(int, const boost::property_tree::ptree& simulation, std::string name);

	/// Constructor from tree, workspace and name (ptree format).
	SimTask(int, const boost::property_tree::ptree& simulation,
		const boost::property_tree::ptree& workspace, std::string name);

	/// Copy-Constructor. Needed to register as Qt meta object (by qRegisterMetaType),
	/// as the default copy constructor is deleted because QObject(const QObject&) is private.
	SimTask(const SimTask&);

	/// Destructor.
	virtual ~SimTask();

        /// Get the name of the exploration.
        std::string GetExploration() const;

        /// Get the id of the task in the exploration.
        int GetId() const;

        /// Get the workspace settings in ptree representation.
        boost::property_tree::ptree GetWorkspace() const;

	/// Returns a pTree representing the SimTask.
	boost::property_tree::ptree ToPtree() const;

	/// Get the simulation in string representation (ptree xml).
	std::string ToString() const;

	/// Get the workspace settings in string representation (ptree xml).
	std::string WorkspaceToString() const;

private:
	int 				m_task_id;
	std::string 			m_name;
	boost::property_tree::ptree 	m_simulation;
	boost::property_tree::ptree	m_workspace;
};

} // namespace

#endif // end-of-include-guard
