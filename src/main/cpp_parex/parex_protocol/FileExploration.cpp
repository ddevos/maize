/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for FileExploration.
 */

#include "FileExploration.h"

using boost::property_tree::ptree;
using namespace std;

namespace SimPT_Parex {

FileExploration::FileExploration(const string& name, const vector<pair<string, ptree>>& files, const ptree& preferences)
	: Exploration(name, preferences)
{
	for (const auto& pair : files) {
		m_file_names.push_back(pair.first);
		m_file_contents.push_back(pair.second);
	}
}

FileExploration::FileExploration(const ptree& pt)
	: Exploration(pt)
{
	ReadPtree(pt);
}

FileExploration::FileExploration(const FileExploration& other)
	: Exploration(other), m_file_names(other.m_file_names), m_file_contents(other.m_file_contents)
{
}

FileExploration& FileExploration::operator=(const FileExploration& other)
{
	if (this != &other) {
		this->operator=(other);

		m_file_names = other.m_file_names;
		m_file_contents = other.m_file_contents;
	}
	return *this;
}

FileExploration* FileExploration::Clone() const
{
	return new FileExploration(*this);
}

std::vector<std::string> FileExploration::GetParameters() const
{
	return {"file"};
}

std::vector<std::string> FileExploration::GetValues(unsigned int index) const
{
	assert(index < GetNumberOfTasks() && "The given index doesn't exist.");

	return { m_file_names[index] };
}

unsigned int FileExploration::GetNumberOfTasks() const
{
	return m_file_names.size();
}

SimTask* FileExploration::CreateTask(unsigned int index) const
{
	assert(index < GetNumberOfTasks() && "The given index doesn't exist.");


	return new SimTask(index, m_file_contents[index], GetPreferences(), GetName());
}

ptree FileExploration::ToPtree() const
{
	ptree pt = this->Exploration::ToPtree();

	auto index_of = [this](const string& elem){
		return distance(m_file_names.begin(),
			find(m_file_names.begin(), m_file_names.end(), elem));
	};

	for (const string& name : m_file_names) {
		ptree file_pt;
		file_pt.put("name", name);
		file_pt.put_child("contents", m_file_contents[index_of(name)]);
		pt.add_child("files.file", file_pt);
	}

	return pt;
}

void FileExploration::ReadPtree(const ptree& pt)
{
	assert(pt.find("files") != pt.not_found() && "No files were found in this exploration.");

	m_file_names.clear();
	m_file_contents.clear();

	for (const auto& pair : pt.get_child("files")) {
		m_file_names.push_back(pair.second.get<std::string>("name"));
		m_file_contents.push_back(pair.second.get_child("contents"));
	}
}

} // namespace
