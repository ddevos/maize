#ifndef SIMPT_PAREX_CLIENT_HANDLER_H_
#define SIMPT_PAREX_CLIENT_HANDLER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ClientHandler
 */

#include <QObject>
#include <memory>
#include <string>

class QTcpSocket;

namespace SimPT_Parex {

class Exploration;
class ExplorationManager;
class ServerClientProtocol;

/**
 * Connection from the server to one client, handles client messages and requests
 */
class ClientHandler: public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 * @param	socket			The tcp socket of the connection to the client
	 * @param	explorationManager	The exploration manager of the server
	 * @param	parent			The QObject parent of this object
	 */
	ClientHandler(QTcpSocket *socket, const std::shared_ptr<ExplorationManager> &explorationManager, QObject *parent = 0);

private slots:
	void DeleteExploration(const std::string &name);

	void DisplayError(const std::string &error) const;

	void Refresh();

	void RegisterExploration(const Exploration *exploration);

	void SendExplorationNames();

	void Subscribe(const std::string &name);

	void Unsubscribe(const std::string &name);

private:
	ServerClientProtocol*                 m_protocol;
	std::shared_ptr<ExplorationManager>   m_exploration_manager;
	std::string                           m_subscribed_exploration;
};

} // namespace

#endif // end-of-include-guard
