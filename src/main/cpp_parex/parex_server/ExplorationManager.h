#ifndef SIMPT_PAREX_EXPLORATION_MANAGER_H_
#define SIMPT_PAREX_EXPLORATION_MANAGER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ExplorationManager
 */

#include <QObject>
#include <deque>
#include <map>
#include <unordered_map>
#include <vector>

namespace SimPT_Parex {

class Exploration;
class ExplorationProgress;
class SimTask;
class WorkerRepresentative;

/**
 * Collects all explorations and works with WorkerPool to actually execute them.
 */
class ExplorationManager: public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 */
	ExplorationManager();

	/**
	 * Destructor
	 */
	virtual ~ExplorationManager();


	/**
	 * Lookup the exploration by name.
	 */
	const ExplorationProgress *GetExplorationProgress(const std::string &explorationName);

	/**
	 * Get a list of all registered explorations.
	 */
	std::vector<std::string> GetExplorationNames();

signals:
	/**
	 * Emitted whenever a change to an exploration is done.
	 */
	void Updated();

public slots:
	/**
	 * Request that the exploration will be simulated somewhere
	 * @param	exploration	The exploration to register (this object will take ownership of the pointer)
	 */
	void RegisterExploration(const Exploration *exploration);

	/**
	 * Delete a given Exploration
	 */
	void DeleteExploration(const std::string &name);

	/**
	 * Stop a node from runnning a specific task
	 */
	void StopTask(const std::string &name, int id);

	/**
	 * Resend a specific task that has been stopped
	 */
	void RestartTask(const std::string &name, int id);

private slots:
	void BackUp();

	void NewWorkerAvailable();

	void WorkerDisconnected();

	void WorkerFinished();

	void WorkerReconnected(WorkerRepresentative*);

private:
	///
	void Read_Backup();

	///
	bool WorkAvailable();

	/// Return the ExplorationInfo element with given name from given deque
	ExplorationProgress *GetExploration(const std::string &name, const std::deque<ExplorationProgress*>&);

	/// Delete ExplorationInfo element with given name from given deque
	void RemoveExploration(const std::string &name, std::deque<ExplorationProgress*>&);

private:
	std::deque<ExplorationProgress*>             m_running_explorations;
	std::deque<ExplorationProgress*>             m_done_explorations;
	std::map<WorkerRepresentative*, SimTask*>    m_running_tasks;

};

} // namespace

#endif // end-of-include-guard
