/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Simulator.
 */

#include "Simulator.h"

#include "../../cpp_simptshell/cli/CliSimulationTask.h"
#include "cli/CliController.h"
#include "workspace/Workspace.h"
#include "util/misc/Exception.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QSystemSemaphore>

#include <memory>
#include <sstream>
#include <string>

namespace {
	const QString C_SEMKEY = "__NODE_SIM_SEMAPHORE__";
	const std::string C_WORKSPACE_PATH = "parex_result_";
}

namespace SimPT_Parex {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace SimPT_Shell;

// http://john.nachtimwald.com/2010/06/08/qt-remove-directory-and-its-contents/
void remDir(QString dir)
{
	QDir d(dir);
	auto files = d.entryInfoList(QDir::NoDotAndDotDot
		| QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files);
	for (auto file : files){
		if (file.isDir()) {
			remDir(file.absoluteFilePath());
		} else if (file.isFile()){
			QFile::remove(file.absoluteFilePath());
		}
	}
	d.rmdir(dir);

}

shared_ptr<Ws::CliWorkspace> SetupWorkspace(const SimTask& task, const std::string& project_name,
	const std::string& workspace_path, const std::string& file)
{
	// Create directory for workspace
	QDir dir;
	QString path = QString::fromStdString(workspace_path);
	if (!dir.exists(path)) {
		dir.mkdir(path);
	}

	// Create workspace (if necessary)
	shared_ptr<Ws::CliWorkspace> w;
	try {
		w = make_shared<Ws::CliWorkspace>(workspace_path); // may throw
	} catch (Exception& e) {
		Ws::CliWorkspace::Init(workspace_path);
		w = make_shared<Ws::CliWorkspace>(workspace_path); // won't throw
	}

	// Create project
	Ws::CliWorkspace::ProjectIterator p;
	try {
		p = w->New("project", project_name);
	} catch (Exception& e) {
		cerr << e.what() << endl;
		return nullptr;
	}

	// Add tissue file to project
	write_xml(workspace_path + "/" + project_name + "/" + file, task.ToPtree());
	try {
		p->second->Add(file);
	} catch (Exception& e) {
		cerr << e.what() << endl;
		return nullptr;
	}

	return w;
}

Simulator::Simulator()
	: m_controller(nullptr), m_stopped(false)
{
}

Simulator::~Simulator()
{
}

void Simulator::Delete(const std::string& name)
{
	QSystemSemaphore m(C_SEMKEY, 1);
	if (!m.acquire()) {
		std::cerr << "SEMAPHORE ERROR: " << QSystemSemaphore::SystemSemaphoreError() << std::endl;
	}
	std::string temp = C_WORKSPACE_PATH + name;
	remDir(QDir(temp.c_str()).absolutePath());
	m.release();
}

void Simulator::SolveTask(const SimTask &task)
{

	std::cout << "Starting Task.." << std::endl;

	const unsigned int num_steps = 0;
	const bool num_steps_set = false;
	std::string workspace_path = C_WORKSPACE_PATH + task.GetExploration();
	std::stringstream ss;
	ss << "Simulation" << task.GetId();
	std::string project_name = ss.str();
	const std::string sim_file = "SimLeaf.xml";
	const bool file_set = true;

	SimResult::ResultType success = SimResult::ResultType::Failure;
	m_stopped = false;

	QSystemSemaphore m(C_SEMKEY, 1);
	if (!m.acquire()) {
		std::cerr << "SEMAPHORE ERROR: " << QSystemSemaphore::SystemSemaphoreError() << std::endl;
	}
	auto workspace = SetupWorkspace(task, project_name, workspace_path, sim_file);

	m.release();
	if (!workspace) {
		std::cerr << "ERROR: Unable to setup workspace. Aborting simulation" << std::endl;
		emit TaskSolved(SimResult(task.GetExploration(), task.GetId(), SimResult::ResultType::Failure));
		return;
	}

	SimPT_Shell::CliSimulationTask ctask(project_name, file_set, sim_file, num_steps_set, num_steps);
	m_controller = SimPT_Shell::CliController::Create(workspace, false);
	if (m_controller) {
		try {
			int result = m_controller->Execute(ctask);
			if (result == 0) {
				success = SimResult::ResultType::Success;
			}
		}
		catch (std::exception &e) {
			std::cerr << "Error while simulating: " << e.what() << std::endl;
		}
		catch (...) {
			std::cerr << "Problem" << std::endl;
		}
	} else {
		std::cerr << "Error set-up Controller" << std::endl;
	}

	if (m_stopped) {
		success = SimResult::ResultType::Stopped;
		std::string s = workspace_path + "/" + project_name;
		QString d = s.c_str();
		remDir(QDir(d).absolutePath());
	}

	emit TaskSolved(SimResult(task.GetExploration(), task.GetId(), success));

	m_controller = nullptr;
}

void Simulator::StopTask()
{
	m_stopped = true;
	if (m_controller) {
		m_controller->Terminate();
	}
}

} // namespace
