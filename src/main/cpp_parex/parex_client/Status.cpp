/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Status
 */

#include "Status.h"

#include "parex_protocol/Exploration.h"
#include "parex_protocol/ExplorationProgress.h"

#include <QVBoxLayout>
#include <QColor>
#include <QString>
#include <QTextEdit>

#include <sstream>

namespace SimPT_Parex {

Status::Status()
	: m_progress()
{
	QVBoxLayout* layout = new QVBoxLayout;

	m_view = new QTextEdit;
	m_view->setReadOnly(true);
	m_view->setText("No information");

	layout->addWidget(m_view);

	setLayout(layout);
}

void Status::Clear()
{
	m_progress.reset();
	UpdateView();
}

void Status::UpdateStatus(const std::shared_ptr<const ExplorationProgress> &progress)
{
	m_progress = progress;
	UpdateView();
}

void Status::UpdateView()
{
	if (!m_progress) {
		m_view->setText("No information available");
	} else {
		std::stringstream output;

		output << "Name: " << m_progress->GetExploration().GetName() << std::endl << std::endl;
		output << "Progress:" << std::endl;
		output << "\t" << m_progress->GetTaskCount(TaskState::Finished) << "/" << m_progress->GetExploration().GetNumberOfTasks() << std::endl;

		if (m_progress->IsFinished()) {
			output << "\t" << "Done" << std::endl;
		} else {
			output << "\t" << m_progress->GetTaskCount(TaskState::Running) << " running" << std::endl;
		}

		if (m_progress->GetTaskCount(TaskState::Failed) != 0) {
			output << "An error occurred, check the task overview for more information." << std::endl;
		}

		output << std::endl;
		output << "Tasks canceled: " << m_progress->GetTaskCount(TaskState::Cancelled);

		m_view->clear();
		m_view->setText(QString::fromStdString(output.str()));
	}
}

void Status::Connected()
{
	QString text = m_view->toPlainText();
	m_view->setTextColor(QColor("black"));
	m_view->setText(text);
}

void Status::Disconnected()
{
	QString text = m_view->toPlainText();
	m_view->setTextColor(QColor("gray"));
	m_view->setText(text);
}

} // namespace
