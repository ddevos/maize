#ifndef SIMPT_PAREX_SERVER_DIALOG_H_INCLUDED
#define SIMPT_PAREX_SERVER_DIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ServerDialog
 */

#include <QDialog>
#include <QString>
#include <QComboBox>
#include <boost/property_tree/xml_parser.hpp>
#include <vector>

namespace SimPT_Parex {

class ServerInfo;

/**
 * Dialog for connecting to server
 */
class ServerDialog : public QDialog
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param path		Path to serverList.xml
	 */
	ServerDialog(std::string path);

	/**
	 * Destructor
	 */
	virtual ~ServerDialog();

	/**
	 * Return the server to connect to
	 * @return	The server info
	 */
	ServerInfo* GetServer();

private:
	QLineEdit* 	m_name;
	QLineEdit*	m_address;
	QLineEdit* 	m_port;

	std::string	m_path;

	std::vector<ServerInfo*>	m_servers;
	ServerInfo*			m_current_server;
	ServerInfo*			m_last_server;

	QComboBox*	m_server_list;

	/**
	 * Load the server list from file to the ComboBox
	 */
	void LoadServerList();

	/**
	 * Save current server list to file
	 *
	 * @param last		Set if most recently connected has to be saved
	 */
	void SaveServerList(bool last);

private slots:

	/**
	 * Store current values for returing
	 */
	void Connect();

	/**
	 * Update selected server
	 *
	 * @param index		Number of server selected
	 */
	void SelectionChanged(int index);

	/**
	 * Update name
	 * @param name		New name
	 */
	void UpdateName(const QString name);

	/**
	 * Update address
	 * @param address	New address
	 */
	void UpdateAddress(const QString address);

	/**
	 * Update port
	 * @param port		New port number
	 */
	void UpdatePort(const QString port);

	/**
	 * Save server to list or update existing server
	 */
	void SaveServer();

	/**
	 * Delete server from the list
	 */
	void DeleteServer();
};

} // namespace

#endif // end_of_include_guard
