/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Client
 */

#include "Client.h"

#include "gui/WorkspaceWizard.h"
#include "parex_protocol/ClientProtocol.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_protocol/Protocol.h"
#include "parex_protocol/ServerInfo.h"
#include "parex_protocol/ParameterExploration.h"

#include "ExplorationSelection.h"
#include "ExplorationWizard.h"
#include "ServerDialog.h"
#include "Status.h"
#include "TaskOverview.h"

#include "workspace/CliWorkspace.h"

#include <QCloseEvent>
#include <QCoreApplication>
#include <QDir>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QMenuBar>
#include <QMessageBox>
#include <QStatusBar>
#include <QToolBar>

#include <memory>

namespace SimPT_Shell { namespace Ws { class WorkspaceFactory; }}

namespace SimPT_Parex {

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Shell;

Client::Client()
	: m_client(nullptr), m_last_exploration(), m_settings("simPT Consortium", "Parex Client")
{
	// Set variables for QSettings
	QCoreApplication::setOrganizationName("SimPT Consortium");
	QCoreApplication::setApplicationName("Parex Client");

	setWindowTitle("Parameter Exploration - Client");

	setMinimumWidth(750);

	QHBoxLayout* layout = new QHBoxLayout;

	m_status = new Status();
	layout->addWidget(m_status);

	// Actions
	action_connect = new QAction("&Connect...", this);
	action_subscribe = new QAction("&Subscribe", this);
	action_start_exploration = new QAction("&Start Exploration...", this);
	action_delete_exploration = new QAction("&Delete exploration...", this);
	action_download_exploration = new QAction("&Download exploration...", this);
	action_workspace_wizard = new QAction("&Workspace...", this);
	action_task_overview = new QAction("&Task overview", this);

	action_subscribe->setDisabled(true);
	action_start_exploration->setDisabled(true);
	action_delete_exploration->setDisabled(true);
	action_download_exploration->setDisabled(true);
	action_task_overview->setDisabled(true);


	action_download_exploration->setVisible(false);

	action_connect->setShortcut(QKeySequence("Ctrl+C"));
	action_subscribe->setShortcut(QKeySequence("Ctrl+U"));
	action_start_exploration->setShortcut(QKeySequence("Ctrl+E"));
	action_delete_exploration->setShortcut(QKeySequence("Ctrl+Shift+E"));
	action_workspace_wizard->setShortcut(QKeySequence("Ctrl+W"));
	action_task_overview->setShortcut(QKeySequence("Ctrl+T"));

	// Menu
	QMenuBar* menu = menuBar();

	QMenu* menu_server = new QMenu("&Server", menu);
	menu_server->addAction(action_connect);
	menu_server->addSeparator();
	menu_server->addAction(action_subscribe);
	menu_server->addAction(action_task_overview);
	menu->addMenu(menu_server);

	QMenu* menu_exploration = new QMenu("&Exploration", menu);
	menu_exploration->addAction(action_start_exploration);
	menu_exploration->addAction(action_delete_exploration);
	menu_exploration->addAction(action_download_exploration);
	menu->addMenu(menu_exploration);

	QMenu* menu_workspace = new QMenu("&Workspace", menu);
	menu_workspace->addAction(action_workspace_wizard);
	menu->addMenu(menu_workspace);

	// Toolbar
	QToolBar* toolbar = new QToolBar(this);
	toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	toolbar->setMovable(false);
	toolbar->addAction(action_connect);
	toolbar->addAction(action_subscribe);
	toolbar->addSeparator();
	toolbar->addAction(action_start_exploration);
	toolbar->addAction(action_delete_exploration);
	toolbar->addAction(action_download_exploration);
	toolbar->addSeparator();
	toolbar->addAction(action_task_overview);
	addToolBar(toolbar);

	connect(action_connect, SIGNAL(triggered()), this, SLOT(Connect()));
	connect(action_subscribe, SIGNAL(triggered()), this, SLOT(Subscribe()));
	connect(action_start_exploration, SIGNAL(triggered()), this, SLOT(StartExploration()));
	connect(action_delete_exploration, SIGNAL(triggered()), this, SLOT(DeleteExploration()));
	connect(action_download_exploration, SIGNAL(triggered()), this, SLOT(DownloadExploration()));

	connect(action_workspace_wizard, SIGNAL(triggered()), this, SLOT(SetWorkspace()));
	connect(action_task_overview, SIGNAL(triggered()), this, SLOT(ShowTaskOverview()));

	// Status bar
	m_statusbar = new QStatusBar(this);
	m_statusbar->clearMessage();

	m_connection_button = new QPushButton("Not Connected");
	m_connection_button->setFlat(true);
	connect(m_connection_button, SIGNAL(clicked()), action_connect, SLOT(trigger()));
	m_statusbar->addPermanentWidget(m_connection_button);
	setStatusBar(m_statusbar);

	QWidget* central = new QWidget;
	central->setLayout(layout);
	setCentralWidget(central);

	m_task_view = new TaskOverview();
	connect(m_task_view, SIGNAL(StopTask(int)), this, SLOT(StopTask(int)));
	connect(m_task_view, SIGNAL(StartTask(int)), this, SLOT(RestartTask(int)));

	// Set Workspace
	if (m_settings.contains("workspace"))
		m_workspace_path = m_settings.value("workspace").toString().toStdString();

	InitWorkspace();
}

Client::~Client()
{

}

void Client::InitWorkspace()
{
	if (m_workspace_path == "" || !QDir(QString(m_workspace_path.c_str())).exists()) {
		auto f = std::make_shared<SimPT_Shell::Ws::WorkspaceFactory>();
		SimShell::Gui::WorkspaceWizard wizard(f);
		if (wizard.exec() == QDialog::Accepted)
			m_workspace_path = wizard.GetWorkspaceDir();
		else
			return;
	}
}

void Client::Connect()
{
	ServerDialog* dialog = new ServerDialog(m_workspace_path);
	if (dialog->exec() == QDialog::Accepted) {
		auto server = dialog->GetServer();
		m_client = new ClientProtocol(server, this);
		if (!m_client->IsConnected()) {
			QMessageBox::critical(this, "Connection Error", "Could not connect to server.",
						QMessageBox::Abort);
			delete m_client;
			m_client = nullptr;
		} else {
			connect(m_client, SIGNAL(Ended()), this, SLOT(Disconnected()));
			connect(m_client, SIGNAL(ExplorationStatus(const ExplorationProgress*)),
				this, SLOT(UpdateStatus(const ExplorationProgress*)));
			connect(m_client, SIGNAL(ExplorationDeleted()), this, SLOT(ExplorationDeleted()));
			Connected(server->GetName().toStdString());
		}
	}
}

void Client::Disconnect()
{
	assert(m_client != nullptr);

	if (QMessageBox::question(this, "Disconnect", "Are you sure you want to disconnect from the server?",
		QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
		Disconnected();
	}
}

void Client::Subscribe()
{
	assert(m_client != nullptr);

	ExplorationSelection select_exploration(true);
	connect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
		&select_exploration, SLOT(UpdateExplorations(std::vector<std::string>)));

	m_subscribed_exploration = "";
	if (!m_client->RequestExplorationNames()) {
		QMessageBox::critical(this, "Connection Error", "Not connected to server.",
			QMessageBox::Abort);
		return;
	}

	if (select_exploration.exec() == QDialog::Accepted) {
		if (select_exploration.Selected()) {
			m_subscribed_exploration = select_exploration.GetSelection().at(0);
			if (!m_client->SubscribeUpdates(select_exploration.GetSelection().at(0)))
				QMessageBox::critical(this, "Connection Error", "Not connected to server.",
						QMessageBox::Abort);
		}
	}

	disconnect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
		&select_exploration, SLOT(UpdateExplorations(std::vector<std::string>)));
 }

void Client::StartExploration()
{
	assert(m_client != nullptr);

	// First check if client is connected
	if (!m_client->IsConnected()) {
		QMessageBox::critical(this, "Connection Error", "Not connected to server.",
			QMessageBox::Abort);
		return;
	}

	std::shared_ptr<const ParameterExploration> exploration
			= std::dynamic_pointer_cast<const ParameterExploration>(m_last_exploration);
	ExplorationWizard wizard(Ws::CliWorkspace(m_workspace_path).GetPreferences(), exploration, this);
	if (wizard.exec() == QDialog::Accepted) {
		/* Send to server */
		m_last_exploration = wizard.GetExploration();
		if (!m_client->SendExploration(m_last_exploration.get())) {
			QMessageBox::critical(this, "Connection Error",
			        "Not connected to server. Exploration will be saved.", QMessageBox::Abort);
		} else {
			QMessageBox::information(this, "Exploration sent",
			        "Exploration successfully sent.", QMessageBox::Ok);
		}
	}
}

void Client::DeleteExploration()
{
	assert(m_client != nullptr);

	ExplorationSelection select_exploration;
	connect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
		&select_exploration, SLOT(UpdateExplorations(std::vector<std::string>)));

	if (!m_client->RequestExplorationNames()) {
		QMessageBox::critical(this, "Connection Error", "Not connected to server.",
			QMessageBox::Abort);
		return;
	}

	if (select_exploration.exec() == QDialog::Accepted) {
		if (select_exploration.Selected()) {
			if (QMessageBox::question(this, "Delete exploration",
			        "Are you sure you want to delete the exploration(s) from the server?",
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
				for (auto exploration : select_exploration.GetSelection()) {
					if (!m_client->DeleteExploration(exploration)) {
						QMessageBox::critical(this, "Connection Error", "Not connected to server.",
							QMessageBox::Abort);
						return;
					}
				}
			}
		}
	}

	disconnect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
		&select_exploration, SLOT(UpdateExplorations(std::vector<std::string>)));
}

void Client::DownloadExploration()
{
	assert(m_client != nullptr);

	ExplorationSelection select_exploration;
	connect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
		&select_exploration, SLOT(UpdateExplorations(std::vector<std::string>)));

	if (!m_client->RequestExplorationNames()) {
		QMessageBox::critical(this, "Connection Error", "Not connected to server.",
			QMessageBox::Abort);
		return;
	}

	if (select_exploration.exec() == QDialog::Accepted) {
		if (select_exploration.Selected()) {
		/*	if (QMessageBox::question(this, "Delete exploration", "Are you sure you want to delete the exploration(s) from the server?",
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
				for (auto exploration : select_exploration.GetSelection()) {
					if (!m_client->DeleteExploration(exploration)) {
						QMessageBox::critical(this, "Connection Error", "Not connected to server.",
							QMessageBox::Abort);
						return;
					}
				}
			}*/
		}
	}

	disconnect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
		&select_exploration, SLOT(UpdateExplorations(std::vector<std::string>)));
}

void Client::StopTask(int task)
{
	assert(m_client != nullptr);

	if (!m_client->StopTask(m_subscribed_exploration, task))
		QMessageBox::critical(this, "Connection Error", "Not connected to server.",
			QMessageBox::Abort);
}

void Client::RestartTask(int task)
{
	assert(m_client != nullptr);

	if (!m_client->RestartTask(m_subscribed_exploration, task))
		QMessageBox::critical(this, "Connection Error", "Not connected to server.",
			QMessageBox::Abort);}

void Client::ShowTaskOverview()
{
	m_task_view->show();
}

void Client::SetWorkspace()
{
	m_workspace_path = "";
	InitWorkspace();
}

void Client::Connected(std::string name)
{
	m_connection_button->setText("Connected with: " + QString(name.c_str()));

	if (m_subscribed_exploration != "") {
		connect(m_client, SIGNAL(ExplorationNames(std::vector<std::string>)),
			this, SLOT(ConnectExplorationNames(std::vector<std::string>)));
		m_client->RequestExplorationNames();
	}

	action_connect->setText("&Disconnect...");
	disconnect(action_connect, SIGNAL(triggered()), this, SLOT(Connect()));
	connect(action_connect, SIGNAL(triggered()), this, SLOT(Disconnect()));
	action_subscribe->setDisabled(false);
	action_start_exploration->setDisabled(false);
	action_delete_exploration->setDisabled(false);
	action_download_exploration->setDisabled(false);

	m_status->Connected();
}

void Client::ConnectExplorationNames(const std::vector<std::string> &names)
{
	assert(m_client != nullptr);

	for (auto name : names) {
		if (name == m_subscribed_exploration) {
			m_client->SubscribeUpdates(name);
			return;
		}
	}
	// If not found: clear status
	UpdateStatus(nullptr);
}

void Client::Disconnected()
{
	delete m_client;
	m_client = nullptr;

	m_connection_button->setText("Not Connected");
	if (m_task_view->isVisible()) {
		m_task_view->close();
	}

	action_connect->setText("&Connect...");
	disconnect(action_connect, SIGNAL(triggered()), this, SLOT(Disconnect()));
	connect(action_connect, SIGNAL(triggered()), this, SLOT(Connect()));
	action_subscribe->setDisabled(true);
	action_start_exploration->setDisabled(true);
	action_delete_exploration->setDisabled(true);
	action_download_exploration->setDisabled(true);
	action_task_overview->setDisabled(true);

	m_status->Disconnected();
}

void Client::UpdateStatus(const ExplorationProgress *status)
{
	std::shared_ptr<const ExplorationProgress> progress(status);
	m_subscribed_exploration = progress->GetExploration().GetName();
	m_status->UpdateStatus(progress);
	m_task_view->UpdateExploration(progress);
	action_task_overview->setDisabled(false);
}

void Client::ExplorationDeleted()
{
	assert(m_client != nullptr);

	if (m_subscribed_exploration != "") {
		m_client->UnsubscribeUpdates(m_subscribed_exploration);
	}
	m_subscribed_exploration = "";
	m_status->UpdateStatus(nullptr);
	m_task_view->UpdateExploration(nullptr);
	action_task_overview->setDisabled(true);
}

} // namespace
