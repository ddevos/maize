#ifndef VIEWERS_FILE_VIEWER_PREFERENCES_H_INCLUDED
#define VIEWERS_FILE_VIEWER_PREFERENCES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Specialization of ViewerPreferences for FileViewer.
 */

#include "workspace/MergedPreferences.h"

#include <string>
#include <sstream>

namespace SimPT_Shell {

/**
 * Preferences for a file viewer.
 */
struct FileViewerPreferences
{
	FileViewerPreferences() : m_stride(0) {}

	void Update(const SimShell::Ws::Event::MergedPreferencesChanged& e) {
		m_stride = e.source->Get<int>("stride");
		m_file   = e.source->Get<std::string>("file");
	}

	int          m_stride;
	std::string  m_file;
};

} // namespace

#endif // end_of_include_guard
