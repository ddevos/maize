#ifndef VIEW_XML_VIEWER_H_INCLUDED
#define VIEW_XML_VIEWER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for XmlViewer.
 */

#include "exporters/XmlExporter.h"
#include "exporters/XmlGzExporter.h"
#include "viewers/common/TextViewer.h"
#include "sim/event/CoupledSimEvent.h"

using namespace SimPT_Sim::Event;

namespace SimPT_Shell {

// Update function specialization
template <>
template <>
void TextViewer<XmlExporter, XmlGzExporter>::Update<CoupledSimEvent>(const SimPT_Sim::Event::CoupledSimEvent&);

// Viewer for export to XML files.
extern template class TextViewer<XmlExporter, XmlGzExporter>;
using XmlViewer = TextViewer<XmlExporter, XmlGzExporter>;

} // namespace

#endif // end_of_include_guard
