#ifndef GUI_CONVERTER_WINDOW_H_INCLUDED
#define GUI_CONVERTER_WINDOW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ConverterWindow
 */

#include "converter/IConverterFormat.h"

#include <QDialog>
#include <QRegExp>
#include <QString>

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace SimPT_Shell {
namespace Ws {
class StartupFileBase;
} /* namespace Ws */
} /* namespace simPT_Shell */

class QComboBox;
class QLineEdit;
class QPushButton;

namespace SimPT_Shell {
namespace Ws {
class Project;
}

class ConversionList;

/**
 * Main widget class for the graphical user interface of the converter.
 */
class ConverterWindow: public QDialog
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 */
	ConverterWindow(Ws::Project*,
		std::vector<IConverterFormat*> formats,
		std::shared_ptr<SimShell::Ws::MergedPreferences> prefs,
		const std::string& window_title,
		QWidget* parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~ConverterWindow();

	/**
	 * Refresh list of input files.
	 */
	void Refresh();

private slots:
	void CheckConversionEnabled();
	void BrowseExportPath();
	void Convert();
	void SLOT_ExportFormatChanged(const QString&);
	void SLOT_OptionsTriggered();

private:
	Ws::Project*                      m_project;
	std::vector<IConverterFormat*>         m_formats;
	std::shared_ptr<SimShell::Ws::MergedPreferences>  m_preferences;

	ConversionList*  m_conversion_list;
	QComboBox*       m_export_format;
	QPushButton*     m_options_button;
	QLineEdit*       m_export_path;
	QLineEdit*       m_export_name;
	QPushButton*     m_convert_button;

	std::map<int, std::vector<std::string>> m_step_to_name_map;
	std::map<int, std::shared_ptr<Ws::StartupFileBase>> m_step_to_file_map;

	static const QRegExp g_export_name_regex;
};

} // namespace

#endif // end-of-include-guard
