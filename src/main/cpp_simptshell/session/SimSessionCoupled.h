#ifndef SIM_SESSION_COUPLED_H_INCLUDED
#define SIM_SESSION_COUPLED_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SimSessionCoupled.
 */

#include "session/SimSession.h"
#include "session/ISession.h"
#include "util/misc/Subject.h"

#include <chrono>
#include <memory>
#include <string>
#include <vector>

class QThread;
namespace SimShell{ namespace Ws { class IWorkspace; }}
namespace SimPT_Sim { class CoupledSim; }

namespace SimPT_Shell {
namespace Session {

/**
 * Collection of instances associated with an opened/running coupled project.
 * Owner of Sim object and viewers.
 */
class SimSessionCoupled : public ISession
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param directory          	The working directory of the project.
	 * @param project_info          Ptree with info to construct simulator from.
	 * @param prefs         	Initial preferences for viewers.
	 * @param workspaceModel	Workspace of sub projects of this coupled project.
	 * @throws Exception if unable to open project (e.g. due to wrong parameters).
	 */
	SimSessionCoupled (
		const std::shared_ptr<MergedPreferences>& prefs,
		const ptree& project_info,
		const std::shared_ptr<SimShell::Ws::IWorkspace>& workspaceModel);

	/// Destructor virtual, just in case.
	virtual ~SimSessionCoupled();

	/// @see Session::ISession
	virtual std::shared_ptr<RootViewerType> CreateRootViewer(
		SimShell::Gui::Controller::AppController* parent = nullptr);

	/// @see Session::ISession
	virtual ExportersType GetExporters();

	/// @see Session::ISession
	virtual const ptree& GetParameters() const;

	/// Return the status message.
	std::string GetStatusMessage() const;

	/// Get timing information from simulation and viewers.
	virtual Timings GetTimings() const;

	/// @see Session::ISession
	virtual void ForceExport();

	virtual void RefreshPreferences() {}

	/// @see Session::ISession
	virtual void SetParameters(const ptree&);

	/// @see Session::ISession
	virtual void StartSimulation(int steps = -1);

	/// @see Session::ISession
	virtual void StopSimulation();

public slots:
	/// @see Session::ISession
	virtual void TimeStep();
private slots:
	virtual void ExecuteViewUnit(QString);

signals:
	void ExecuteWorkUnit();

private:
	/**
	 * Reads the list of subprojects from the project ptree
	 * @param	pt     The ptree of coupled simulation ptree
	 * @return	       The list of names of subprojects
	 * @throws	        Ptree exceptions if the ptree is invalid
	 */
	std::vector<std::pair<std::string, std::string>>
	ReadSubprojectList(const boost::property_tree::ptree& pt);

	/**
	 * Opens a subproject
	 * @param	projectName	The (workspace) name of the project to open
	 * @param	file		The tissue file to load (or "" to load the last file in project)
	 * @param	workspaceModel	The workspace model needed to open the subproject
	 * @return	The loaded project
	 * @throws	Exception if something goes wrong
	 */
	std::shared_ptr<SimShell::Ws::IProject> OpenSubproject(
		const std::string& projectName,
		const std::string& file,
		const std::shared_ptr<SimShell::Ws::IWorkspace>& workspaceModel);

private:
	/// Local copy of preferences to initialize viewers.
	std::shared_ptr<SimShell::Ws::MergedPreferences>      m_preferences;

	/// The sub projects of this coupled project.
	std::map<std::string, std::shared_ptr<SimShell::Ws::IProject>>  m_projects;

	/// The (coupled) simulator of the coupled project.
	std::shared_ptr<SimPT_Sim::CoupledSim>               m_sim;

	/// Count down to 0 when we run a limited number of steps.
	int                                                   m_steps_limit;


	/// Timing durations for parts of the computations.
	Timings                                               m_timings;

	bool                                m_running;          ///< Whether simulation is currently running.


	std::mutex                          m_parameters_mutex; ///< Locked while parameters are being set.
	std::mutex                          m_viewers_mutex;    ///< Locked while viewers are being notified.

	QThread*                            m_sim_thread;       ///< Simulation thread.

	struct {
		bool   updated;       ///< Whether parameters were updated and need saving to disk.
		ptree  pt;            ///< Ptree containing parameters.
	} m_parameter_buffer; 

};

} // namespace
} // namespace

#endif // end_of_include_guard
