/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimSessionCoupled.
 */

#include "session/SimSessionCoupled.h"

#include "session/SimSession.h"
#include "session/SimWorker.h"
#include "viewer/RootViewerNode.h"
#include "viewers/LogWindowViewer.h"
#include "sim/CoreData.h"
#include "sim/CoupledSim.h"
#include "sim/Sim.h"
#include "util/misc/Exception.h"
#include "viewer/ViewerNode.h"
#include "workspace/IFile.h"
#include "workspace/IProject.h"
#include "workspace/IWorkspace.h"

#include <QTimer>
#include <QThread>

using namespace std;
using namespace SimPT_Sim;
using SimPT_Sim::Util::Exception;

namespace SimShell {namespace Viewer {
	template <>
	struct viewer_is_widget<SimPT_Shell::LogWindowViewer>
	{
		static const bool value = true;
	};
}}


namespace SimPT_Shell {
namespace Session {

SimSessionCoupled::SimSessionCoupled(
	const std::shared_ptr<MergedPreferences>& prefs,
	const ptree& project_info,
	const std::shared_ptr<SimShell::Ws::IWorkspace>& workspaceModel
	)
	:
	  m_preferences(prefs),
	  m_sim(make_shared<CoupledSim>()),
	  m_steps_limit(0),
	  m_running(false),
	  m_sim_thread(new QThread(this)),
	  m_parameter_buffer({ false, ptree() })
{
	auto projects = ReadSubprojectList(project_info);
	if (projects.empty()) {
		throw Exception("Failed to open coupled project: subproject list is empty");
	}

	for (const auto& project : projects) {
		m_projects[project.first] =
			OpenSubproject(project.first, project.second, workspaceModel);
	}

	vector<shared_ptr<Sim>> sims;
	for (const auto& project : m_projects) {
		sims.push_back(static_pointer_cast<ISimSession>(project.second->GetSession())->GetSim());
	}
	m_sim->Initialize(project_info, sims);
}

SimSessionCoupled::~SimSessionCoupled()
{
	// Close sub projects
	for (auto project : m_projects) {
		if (project.second->IsOpened()) {
			project.second->Close();
		}
	}
}

std::shared_ptr<SimSessionCoupled::RootViewerType>
SimSessionCoupled::CreateRootViewer(SimShell::Gui::Controller::AppController* parent)
{
	SimShell::Viewer::IViewerNode::ChildrenMap m;
	m["Log Dock Window"] = make_shared<SimShell::Viewer::ViewerNode<LogWindowViewer, SimPT_Sim::CoupledSim>>(
								m_preferences->GetChild("viewers.logwindow"), parent);
	for (auto project : m_projects) {
		auto project_root_viewer = project.second->GetSession()->CreateRootViewer(parent);
		m[project.first] = make_shared<SimShell::Viewer::SubjectViewerNodeWrapper<CoupledSim>>(project_root_viewer);
	}
	return make_shared<SimShell::Viewer::SubjectNode<CoupledSim>>(m_preferences->GetChild("viewers"), m_sim, move(m));
}

SimSessionCoupled::ExportersType
SimSessionCoupled::GetExporters()
{
	// TODO Think about nesting the exporters of the subprojects into this list
	return {};
}

const ptree& SimSessionCoupled::GetParameters() const
{
	return m_sim->GetParameters();
}

std::string SimSessionCoupled::GetStatusMessage() const
{
	// TODO Multiple lines: suitable for Qt status bar?
	stringstream ss;
	ss << "Coupled simulation:";

	for (auto project : m_projects) {
		ss << std::endl;
		ss << '\t' << project.first
			<< " : " << static_pointer_cast<ISimSession>(
				project.second->GetSession())->GetStatusMessage();
	}
	return ss.str();
}

SimSessionCoupled::Timings SimSessionCoupled::GetTimings() const
{
	auto timings = m_sim->GetTimings();
	timings.Merge(m_timings);
	return timings.GetRecords<Duration>();
}

void SimSessionCoupled::ForceExport()
{
	lock_guard<mutex> parameters_guard(m_viewers_mutex);

	for (auto project : m_projects) {
		project.second->GetSession()->ForceExport();
	}
}

std::shared_ptr<SimShell::Ws::IProject> SimSessionCoupled::OpenSubproject(
	const string& project_name,
	const string& file_name,
	const shared_ptr<SimShell::Ws::IWorkspace>& workspaceModel)
{
	shared_ptr<SimShell::Ws::IProject> p;
	try {
		auto project_it = workspaceModel->Find(project_name); // may throw
		if (project_it == workspaceModel->end() ) {
			throw Exception("Failed to open subproject " + project_name+": Not Found");
		}
		if (!file_name.empty()) {
			project_it->second->Open(file_name);
		} else {
			project_it->second->Open();
		}
		p = project_it->second.Project();
	} catch (std::runtime_error& e) {
		throw Exception("Failed to open subproject " + project_name + ": " + e.what());
	}

	if (!p || !p->IsOpened()) {
		throw Exception("Failed to open subproject " + project_name);
	}

	return p;
}

void SimSessionCoupled::SetParameters(const ptree& pt)
{
	lock_guard<mutex> parameters_guard(m_parameters_mutex);
	m_parameter_buffer.pt = pt;
	m_parameter_buffer.updated = true;
}

void SimSessionCoupled::StartSimulation(int steps)
{
	assert(steps >= -1 && "Steps should be positive or -1");
	m_steps_limit = steps;

	// Create worker instance
	auto sim_worker = new SimWorker(m_sim);
	sim_worker->moveToThread(m_sim_thread);

	// The signals & slots magic
	connect(m_sim_thread, SIGNAL(started()), sim_worker, SLOT(Work()));
	connect(this, SIGNAL(ExecuteWorkUnit()), sim_worker, SLOT(Work()));
	connect(sim_worker, SIGNAL(Worked(QString)), this, SLOT(ExecuteViewUnit(QString)));
	connect(m_sim_thread, SIGNAL(finished()), sim_worker, SLOT(deleteLater()));

	// Get the ball rolling
	m_running = true;
	if (m_parameter_buffer.updated) {
		m_sim->Reinitialize(m_parameter_buffer.pt);
		// Notify viewers (preferences can't be changed during notification).
		{
			lock_guard<mutex> viewers_guard(m_viewers_mutex);
			m_sim->Notify(SimPT_Sim::Event::CoupledSimEvent(m_sim, m_sim->GetSimStep(),
			                                SimPT_Sim::Event::CoupledSimEvent::Type::Forced));
		}
		m_parameter_buffer.updated = false;
	}
	m_sim_thread->start();
	emit InfoMessage(GetStatusMessage(), InfoMessageReason::Started);
}

void SimSessionCoupled::StopSimulation()
{

	m_running = false;
	m_sim_thread->quit();
	m_sim_thread->wait();
	m_steps_limit = -1;
	emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stopped);
}

std::vector<std::pair<std::string, std::string>>
SimSessionCoupled::ReadSubprojectList(const ptree& pt)
{
	vector<pair<string, string>> projects;
	const auto& projects_pt = pt.get_child("vleaf2.coupled_project.subproject_array");
	for (auto pair : projects_pt) {
		const auto& project_pt = pair.second;
		projects.push_back(make_pair(
			project_pt.get<std::string>("name"),
			project_pt.get<std::string>("file", "")
			));
	}
	return projects;
}

void SimSessionCoupled::ExecuteViewUnit(QString worker_message)
{
	if (worker_message != "") {
		throw Exception(worker_message.toStdString());
	}
	{
		lock_guard<mutex> viewers_guard(m_viewers_mutex);
		// TODO Refactor to protected function in interface for projects?
		// TODO Strides, forced export (on parameter change) for coupled simulation?
		Stopclock viewersChrono("viewers", true);
		for (auto project : m_projects) {
			auto sim = static_pointer_cast<ISimSession>(project.second->GetSession())->GetSim();
			SimPT_Sim::Event::SimEvent e(sim, sim->GetSimStep(),
							SimPT_Sim::Event::SimEvent::Type::Stepped);
			sim->Notify(e);
		}
		SimPT_Sim::Event::CoupledSimEvent e(m_sim, m_sim->GetSimStep(),
							SimPT_Sim::Event::CoupledSimEvent::Type::Stepped);
		m_sim->Notify(e);
		m_timings.Record(viewersChrono.GetName(), viewersChrono.Get());
	}
	emit InfoMessage(GetStatusMessage(), InfoMessageReason::Stepped);
	--m_steps_limit;

	if (m_running) {
		if ((m_steps_limit == 0) || m_sim->IsAtTermination()) {
			StopSimulation();
		} else {
			{ // Check if parameters were changed.
				lock_guard<mutex> parameters_guard(m_parameters_mutex);
				if (m_parameter_buffer.updated) {
					m_sim->Reinitialize(m_parameter_buffer.pt);
					// Notify viewers (preferences can't be changed during notification).
					{
						lock_guard<mutex> viewers_guard(m_viewers_mutex);
						m_sim->Notify(SimPT_Sim::Event::CoupledSimEvent(
						        m_sim, m_sim->GetSimStep(),
						        SimPT_Sim::Event::CoupledSimEvent::Type::Forced));
					}
					m_parameter_buffer.updated = false;
				}
			}
			emit ExecuteWorkUnit();
		}
	}
}

void SimSessionCoupled::TimeStep()
{
	StartSimulation(1);
}

} // namespace
} // namespace

