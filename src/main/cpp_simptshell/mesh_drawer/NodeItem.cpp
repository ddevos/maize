/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for NodeItem.
 */

#include "../../cpp_simptshell/mesh_drawer/NodeItem.h"

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOption>
#include <Qt>

namespace SimPT_Shell {

using SimPT_Sim::Node;

NodeItem::NodeItem(Node* n, double mag)
		: m_node(n)
{
	brush = Qt::darkGray;
	ellipsesize = QRectF(-1 * mag, -1 * mag, 2 * mag, 2 * mag);
}

void NodeItem::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
{
	painter->setBrush(brush);
	painter->setPen(Qt::NoPen);
	painter->drawEllipse(ellipsesize);
}

QPainterPath NodeItem::shape() const
{
	QPainterPath path;
	path.addEllipse(ellipsesize);
	return path;
}

QRectF NodeItem::boundingRect() const
{
	qreal penwidth = 0; // painter->pen()->widthF();
	return QRectF(ellipsesize.x() - penwidth / 2., ellipsesize.y() - penwidth / 2.,
			ellipsesize.width() + penwidth, ellipsesize.height() + penwidth);
}

void NodeItem::setColor()
{
	static QColor indian_red("IndianRed");
	static QColor deep_sky_blue("DeepSkyBlue");
	static QColor purple("Purple");
	Node& n = getNode();

	if (n.IsSam()) {
		setBrush(purple);
	} else {
		if (n.IsAtBoundary()) {
			setBrush(deep_sky_blue);
		} else {
			setBrush(indian_red);
		}
	}
}

} // namespace
