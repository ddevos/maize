/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CsvGzExporter.
 */

#include "CsvGzExporter.h"

#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "bio/Wall.h"
#include "math/MeshGeometry.h"
#include "sim/CoreData.h"
#include "sim/SimInterface.h"

#include <QFile>
#include <QString>
#include <QTextStream>
#include <fstream>
#include <tuple>

namespace SimPT_Shell {

using namespace std;
using namespace SimPT_Sim;

bool CsvGzExporter::Export(shared_ptr<SimPT_Sim::SimInterface> sim, string const& file_path, bool overwrite)
{
	if (ifstream(file_path).good()) {
		if (overwrite) {
			// Qt doesn't overwrite by default, must delete file first
			QFile::remove(QString::fromStdString(file_path));
		} else {
			return false; // Don't overwrite
		}
	}

	bool status = false;
	QFile file(QString::fromStdString(file_path));
	if (file.open(QIODevice::WriteOnly)) {
		const auto mesh = sim->GetCoreData().m_mesh.get();
		QTextStream stream(&file);
		StreamCellData(mesh, stream);
		StreamWallData(mesh, stream);
		StreamMeshData(mesh, stream);
		file.close();
		status = true;
	}
	return status;
}

string CsvGzExporter::GetFileExtension()
{
	return "csv.gz";
}

void CsvGzExporter::StreamCellData(const Mesh* mesh, QTextStream& csv_stream)
{
	csv_stream << "\"Cell Index\",\"Center of mass (x)\",\"Center of mass (y)\",\"Cell area\",\"Cell length\"";

	const unsigned int num_chem = mesh->GetNumChemicals();

	for (unsigned int c = 0; c < num_chem; c++) {
		csv_stream << ",\"Chemical " << c << "\"";
	}
	csv_stream << endl;
	for (auto const& cell : mesh->GetCells()) {
		auto centroid = cell->GetCentroid();
		csv_stream << cell->GetIndex() << ", " << centroid[0] << ", " << centroid[1] << ", "
				<< cell->GetArea() << ", " << get<0>(cell->GetGeoData().GetEllipseAxes());
		for (unsigned int c = 0; c < num_chem; c++) {
			csv_stream << ", " << cell->GetChemical(c);
		}
		csv_stream << endl;
	}
}

void CsvGzExporter::StreamMeshData(const Mesh* mesh, QTextStream& csv_stream)
{
	csv_stream << "\"Morph area\",\"Number of cells\",\"Number of nodes\",\"Compactness\",\"Hull area\",\"Morph circumference\",\"Hull circumference\""
			<< endl;

	auto res_tuple = MeshGeometry::Compactness(mesh);
	const double res_compactness      = get<0>(res_tuple);
	const double res_area             = get<1>(res_tuple);
	const double hull_circumference   = get<2>(res_tuple);
	const double morph_circumference  = mesh->GetBoundaryPolygon()->GetCircumference();
	const double mesh_area            = mesh->GetBoundaryPolygon()->GetArea();

	csv_stream << mesh_area << ", " << mesh->GetCells().size() << ", " << mesh->GetNodes().size()
			<< ", " << res_compactness << ", " << res_area << ", "
			<< morph_circumference << ", " << hull_circumference << endl;
}

void CsvGzExporter::StreamWallData(const Mesh* mesh, QTextStream& csv_stream)
{
	csv_stream << "\"Wall Index\",\"Cell A\",\"Cell B\",\"Length\"";

	const unsigned int num_chem = mesh->GetNumChemicals();

	for (unsigned int c = 0; c < num_chem; c++) {
		csv_stream << ",\"Transporter A:" << c << "\"";
	}
	for (unsigned int c = 0; c < num_chem; c++) {
		csv_stream << ",\"Transporter B:" << c << "\"";
	}
	csv_stream << endl;
	for (auto const& wall : mesh->GetWalls()) {
		csv_stream << wall->GetIndex() << "," << wall->GetC1()->GetIndex() << ","
				<< wall->GetC2()->GetIndex() << "," << wall->GetLength();
		for (unsigned int c = 0; c < num_chem; c++) {
			csv_stream << "," << wall->GetTransporters1(c);
		}
		for (unsigned int c = 0; c < num_chem; c++) {
			csv_stream << "," << wall->GetTransporters2(c);
		}
		csv_stream << endl;
	}
}

} // namespace
