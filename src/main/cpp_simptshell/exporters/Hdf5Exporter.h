#ifndef VIEW_HDF5_EXPORTER_H_INCLUDED
#define VIEW_HDF5_EXPORTER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Hdf5Exporter.
 */

#include <memory>

namespace SimPT_Sim {
class Sim;
}

namespace SimPT_Shell {

/**
 * Functions and data for HDF5 export of Sim objects.
 */
class Hdf5Exporter
{
public:
	/**
	 * Export mesh state to hdf5 format.
	 * @param mesh       	Mesh to export.
	 * @param filename  	Name of file.
	 * @param overwrite     Whether to overwrite if path already exists.
	 */
	static bool Export(std::shared_ptr<SimPT_Sim::Sim> mesh, std::string const& file_path, bool overwrite = true);

	/**
	 * File extension associated with this export format.
	 * @return		String containing file extension (without a ".")
	 */
	static std::string GetFileExtension();
};

} // namespace

#endif // end_of_include_guard
