/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StartupFileXmlGz.
 */

#include "../../cpp_simptshell/workspace/StartupFileXmlGz.h"

#include "fileformats/PTreeFile.h"

namespace SimPT_Shell {
namespace Ws {

using namespace std;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;

StartupFileXmlGz::StartupFileXmlGz(const string& path)
	: StartupFilePtree(path),
	  m_decompressor(path)
{
}

vector<QAction*> StartupFileXmlGz::GetContextMenuActions() const
{
	return { m_decompressor.GetActionDecompress() };
}

ptree StartupFileXmlGz::ToPtree() const
{
	ptree pt;
	PTreeFile::ReadXmlGz(m_path, pt);
	return pt;
}

} // namespace
} // namespace
