/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StartupFileHdf5.
 */

#include "workspace/StartupFileHdf5.h"

#include "session/SimSession.h"
#include "fileformats/Hdf5File.h"
#include "util/misc/Exception.h"
#include "workspace/MergedPreferences.h"

namespace SimPT_Shell {
namespace Ws {

using namespace std;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;
using namespace SimShell::Ws;

StartupFileHdf5::StartupFileHdf5(const string& path)
	: StartupFileBase(path),
	  m_reverse_index_initialized(false)
{
}

shared_ptr<SimShell::Session::ISession>
StartupFileHdf5::CreateSession(shared_ptr<IProject> proj, std::shared_ptr<IWorkspace> ws) const
{
	shared_ptr<Session::ISession> session;
	auto prefs = MergedPreferences::Create(ws, proj);
	try {
		Hdf5File tissue_file(m_path);
		// may throw
		SimPT_Sim::SimState sim_state = tissue_file.Read();
		session = make_shared<Session::SimSession>(prefs, sim_state);
	}
	catch (exception& e) {
		throw Exception("Could not open \"" + m_path + "\": " + e.what());
	}

	return session;
}

SimPT_Sim::SimState StartupFileHdf5::GetSimState(int timestep) const
{
	if (!m_reverse_index_initialized)
		InitializeReverseIndex();
	try {
		Hdf5File tissue_file(m_path);
		SimPT_Sim::SimState result = tissue_file.Read(m_reverse_index[timestep]);
		tissue_file.Close();
		return result;
	} catch (exception& e) {
		throw Exception("GetSimState(): Could not open \"" + m_path + "\": " + e.what());
	}
}

vector<int> StartupFileHdf5::GetTimeSteps() const
{
	vector<int> result;
	try {
		Hdf5File tissue_file(m_path);
		for (auto& pair : tissue_file.TimeSteps()) {
			result.push_back(pair.first);
		}
	} catch (exception& e) {
		throw Exception("GetTimeSteps(): Could not open \"" + m_path + "\": " + e.what());
	}
	return result;
}

void StartupFileHdf5::InitializeReverseIndex() const
{
	int i = 0;
	for (int t : GetTimeSteps()) {
		m_reverse_index[t] = i++;
	}
	m_reverse_index_initialized = true;
}

} // namespace
} // namespace

