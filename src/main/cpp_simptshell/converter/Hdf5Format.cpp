/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of File Converter Formats.
 */

#include "Hdf5Format.h"

#include "fileformats/Hdf5File.h"
#include "util/misc/Exception.h"

using namespace std;
using namespace SimPT_Sim;
using SimShell::Ws::MergedPreferences;
using SimPT_Sim::Util::Exception;

namespace SimPT_Shell {

void Hdf5Format::PreConvert(const string& target_path, shared_ptr<MergedPreferences>)
{
	m_file.Open(target_path + '.' + GetExtension());
	if (!m_file.IsOpen()) {
		throw Exception("Couln't open or create HDF5 file " + target_path);
	}
}

void Hdf5Format::Convert(const SimState& s)
{
	auto sim = make_shared<SimPT_Sim::Sim>();
	sim->Initialize(s);
	try {
		m_file.Write(sim);
	}
	catch (runtime_error &e) {
		throw Exception("Error writing step "
			+ to_string(s.GetTimeStep())
			+ " to " + m_file.GetFilePath() + ":\n" + e.what());
	}
}

void Hdf5Format::PostConvert()
{
	m_file.Close();
}

} // namespace
