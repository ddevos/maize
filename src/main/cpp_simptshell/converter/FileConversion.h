#ifndef SIMPT_SHELL_FILE_CONVERSION_H_INCLUDED
#define SIMPT_SHELL_FILE_CONVERSION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Conversion
 */

#include "IConverterFormat.h"
#include "workspace/Project.h"
#include "workspace/MergedPreferences.h"

#include <functional>
#include <vector>
#include <map>
#include <memory>

namespace SimPT_Shell {

/**
 * Manages file conversion operations specifications.
 */
class FileConversion {
public:
	FileConversion(Ws::Project* project,
		std::vector<int> timesteps,
		std::shared_ptr<SimShell::Ws::MergedPreferences> prefs,
		IConverterFormat* output_format,
		std::string output_path,
		std::string output_prefix);

	void Run(std::function<void(int)> progress_callback = std::function<void(int)>());

private:
	Ws::Project*                                          m_project;
	std::vector<int>                                      m_timesteps;
	std::shared_ptr<SimShell::Ws::MergedPreferences>      m_preferences;
	IConverterFormat*                                     m_output_format;
	std::string                                           m_output_path;
	std::string                                           m_output_prefix;
	std::map<int, std::shared_ptr<Ws::StartupFileBase>>   m_step_to_file_map;
};

} // namespace

#endif // end-of-include-guard
