/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of cvector graphics converter format.
 */

#include "VectorFormat.h"

using namespace std;
using namespace SimPT_Sim;
using SimShell::Ws::MergedPreferences;

namespace SimPT_Shell {

void VectorFormat::Convert(const SimState& s)
{
	auto sim = make_shared<SimPT_Sim::Sim>();
	sim->Initialize(s);
	auto prefs = make_shared<VectorGraphicsPreferences>();
	prefs->Update({m_prefs->GetChild("viewers.bitmap_graphics")});
	prefs->m_format = m_format;
	VectorGraphicsExporter::Export(sim, GeneratePath(s), true, prefs);
}

} // namespace
