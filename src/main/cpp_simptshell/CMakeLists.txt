#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build static library (gets used building, no need to install).
#============================================================================
#---------------------------- set variables ---------------------------------
set( LIB	simPTlib_shell )
set( SRC
#---------
	cli/CliController.cpp
	cli/CliWorkspaceManager.cpp
	cli/CoupledCliController.cpp
#---------
    converter/BitmapFormat.cpp
	converter/StepSelection.cpp
	converter/FileConversion.cpp
	converter/FileConverterFormats.cpp
	converter/Hdf5Format.cpp
	converter/VectorFormat.cpp
#---------	
	exporters/BitmapGraphicsExporter.cpp
	exporters/CsvExporter.cpp
	exporters/CsvGzExporter.cpp
	exporters/Hdf5Exporter.cpp
	exporters/PlyExporter.cpp
	exporters/VectorGraphicsExporter.cpp
	exporters/XmlExporter.cpp
	exporters/XmlGzExporter.cpp
#---------
    gui/AppCustomizer.cpp
	gui/ConversionList.cpp
	gui/ConverterWindow.cpp
    gui/qtmodel/StepFilterProxyModel.cpp
#---------		
	mesh_drawer/ArrowItem.cpp
	mesh_drawer/NodeItem.cpp
	mesh_drawer/WallItem.cpp
	mesh_drawer/MeshDrawer.cpp
#---------	
	session/SimWorker.cpp
	session/SimSession.cpp
	session/SimSessionCoupled.cpp
#---------	
	viewer/RootViewerNode.cpp
#---------	
	viewers/Hdf5Viewer.cpp
	viewers/LogWindowViewer.cpp
	viewers/QtViewer.cpp
	viewers/XmlViewer.cpp
#---------	
	workspace/CliWorkspace.cpp
	workspace/GuiWorkspace.cpp
	workspace/Project.cpp
	workspace/StartupFileBase.cpp
	workspace/StartupFileHdf5.cpp
	workspace/StartupFilePtree.cpp
	workspace/StartupFileXml.cpp
	workspace/StartupFileXmlGz.cpp
	workspace/Workspace.cpp
	workspace/WorkspaceFactory.cpp
	workspace/util/Compressor.cpp
)

set( MOC_HEADERS
	cli/CoupledCliController.h
	cli/CliController.h
#---------	
	gui/qtmodel/StepFilterProxyModel.h
	gui/ConversionList.h
	gui/ConverterWindow.h
#---------	
	session/SimWorker.h
	session/ISimSession.h
	session/SimSession.h
	session/SimSessionCoupled.h
#---------	
	workspace/util/Compressor.h
)
set( MOC_OUTFILES )

#------------------------------ build ---------------------------------------
if ( ${SIMPT_QT_VERSION} EQUAL 4 )
	qt4_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
elseif( ${SIMPT_QT_VERSION} EQUAL 5 )
	qt5_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
endif()
add_library( ${LIB}  ${SRC} ${MOC_OUTFILES} $<TARGET_OBJECTS:simshell> )
target_link_libraries( ${LIB}  
            simPTlib_sim ${QT_LIBRARIES} ${Boost_LIBRARIES} ${ZLIB_LIBRARIES} )
#
if( APPLE )
    set_target_properties( ${LIB}  PROPERTIES INSTALL_NAME_DIR  "@rpath" )
endif( APPLE )

#------------------------------ install -------------------------------------
install( TARGETS ${LIB}  DESTINATION  ${LIB_INSTALL_LOCATION} )

#------------------------------ unset ---------------------------------------
unset( MOC_HEADERS  )
unset( MOC_OUTFILES )
unset( SRC          )
unset( LIB          )

#############################################################################
