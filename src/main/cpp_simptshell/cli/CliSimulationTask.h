#ifndef CLI_TASK_H_
#define CLI_TASK_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for CliTask.
 */

#include <iomanip>
#include <iostream>
#include <string>

namespace SimPT_Shell {

/**
 * A CliTask represents an invocation of the program from the command line.
 */
class CliSimulationTask
{
public:
	/// Straight constructor.
	CliSimulationTask(const std::string& project, bool input_file_set = false,
		const std::string& input_file = "",
		bool num_steps_set = false, unsigned int num_steps = 0)
			: m_input_file(input_file), m_input_file_set(input_file_set),
			  m_num_steps(num_steps), m_num_steps_set(num_steps_set),
			  m_project_name(project)
	{
	}

	/// Plain getter.
	const std::string GetLeaf() const { return m_input_file; }

	/// Plain getter.
	unsigned int GetNumSteps() const { return m_num_steps; }

	/// Plain getter.
	const std::string GetProjectName() const {return m_project_name; }

	/// Plain getter.
	bool IsLeafSet() const {return m_input_file_set; }

	/// @throw Exception if task hasn't been set.
	bool IsNumStepsSet() const { return m_num_steps_set; }

private:
	std::string   m_input_file;       ///< Name of input file to start simulation.
	bool          m_input_file_set;   ///< Whether input file was specified.
	unsigned int  m_num_steps;        ///< Number of steps for simulator; used only iff m_num_steps_set.
	bool          m_num_steps_set;    ///< True iff number of steps for simulator has been specified.
	std::string   m_project_name;     ///< Name of project to execute.
};

inline
std::ostream& operator<<(std::ostream& os, const CliSimulationTask& task)
{
	os << "CliTask: project = " << task.GetProjectName() << std::endl
		<< " is_input_file_set = " << std::boolalpha << task.IsLeafSet()
		<< ", input_file = " << task.GetLeaf() << std::endl
		<< " is_num_steps_set = " << std::boolalpha << task.IsNumStepsSet()
		<< ", number of steps = " << task.GetNumSteps();

	return os;
}

} // namespace

#endif // include guard
