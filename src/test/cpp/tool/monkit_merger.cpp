/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for main program with command line interface.
 */

#include "util/clock_man/TimeStamp.h"
#include "util/misc/Exception.h"
#include "util/misc/StringUtils.h"
#include "util/misc/XmlWriterSettings.h"

#include <tclap/CmdLine.h>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>

#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace TCLAP;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim::ClockMan;

int main(int argc, char** argv)
{
	int exit_status = EXIT_SUCCESS;

	try {
		cout << "Starting up at: " << TimeStamp().ToString() << endl;
		cout << "Executing " << argv[0]<< endl << endl;

		CmdLine cmd("monkit_merger", ' ', "1.0");
		ValueArg<string>   file_Arg("f", "file", "File with monkit data", true, "", "FILE", cmd);
		cmd.parse(argc, argv);

		const string file = file_Arg.getValue();
		ptree in_pt;
		read_xml(file, in_pt, trim_whitespace);

		map<string, ptree>  merge_map;
		unsigned int count   = 0;
		unsigned int ccount  = 0;
		unsigned int cccount = 0;

		const ptree& categories_pt = in_pt.get_child("categories");
		auto r = categories_pt.equal_range("category");
		for (auto it = r.first; it != r.second; ++it) {
			++count;
			const ptree& category_pt = (*it).second;
			const string category_name = category_pt.get<string>("<xmlattr>.name");
			auto rr = category_pt.equal_range("observations");

			for (auto itt = rr.first; itt != rr.second; ++itt) {
				ccount++;
				const ptree& observations_pt = (*itt).second;
				auto rrr = observations_pt.equal_range("observation");

				for (auto ittt = rrr.first; ittt != rrr.second; ++ittt) {
					cccount++;
					const ptree& observation_pt = (*ittt).second;
					merge_map[category_name].add_child("observation", observation_pt);
				}
			}
		}

		map<string, ptree> sorted_map;
		for (const auto& e : merge_map) {
			vector<double>  vec;
			for (const auto& p : e.second) {
				const double val = p.second.get_value<double>();
				vec.push_back(val);
			}
			sort(vec.begin(), vec.end());
			for (unsigned int i = 0; i < vec.size(); ++i) {
				ptree obs;
				obs.put("<xmlattr>.name", "run_" + to_string(i));
				obs.put_value(vec[i]);
				sorted_map[e.first].add_child("observation", obs);
			}
		}

		ptree out_cat_pt;
		for (const auto& e : sorted_map) {
			ptree c_pt;
			c_pt.put("<xmlattr>.name", e.first);
			c_pt.put("<xmlattr>.scale", "seconds");
			c_pt.add_child("observations", e.second);
			out_cat_pt.add_child("category", c_pt);
		}

		ptree out_pt;
		out_pt.add_child("categories", out_cat_pt);
		write_xml(file, out_pt, std::locale(), XmlWriterSettings::GetTab());

		cout << "counted : " << count   << " category items." << endl;
		cout << "counted : " << ccount  << " observations items." << endl;
		cout << "counted : " << cccount << " observation items." << endl <<endl;
		cout << "Exiting at: " << TimeStamp().ToString() << endl << endl;
	}
	catch (Exception& e) {
		exit_status = EXIT_FAILURE;
		cerr << e.what() << endl;
	}
	catch (exception& e) {
		exit_status = EXIT_FAILURE;
		cerr << e.what() << endl;
	}
	catch (...) {
		exit_status = EXIT_FAILURE;
		std::cerr << "Unknown exception thrown." << endl;
	}

	return exit_status;
}
