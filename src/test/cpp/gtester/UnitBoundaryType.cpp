/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of UnitBoundaryType.
 */

#include "bio/BoundaryType.h"

#include <gtest/gtest.h>
#include <string>

using namespace std;
using namespace SimPT_Sim::Util;
using SimPT_Sim::BoundaryType;

namespace SimPT_Shell {
namespace Tests {

TEST( UnitBoundaryType, Types )
{
	ASSERT_EQ( true, BoundaryType::None
		== FromString<BoundaryType>(ToString(BoundaryType::None)) );
	ASSERT_EQ( true, BoundaryType::NoFlux
		== FromString<BoundaryType>(ToString(BoundaryType::NoFlux)) );
	ASSERT_EQ( true, BoundaryType::SourceSink
		== FromString<BoundaryType>(ToString(BoundaryType::SourceSink)) );
	ASSERT_EQ( true, BoundaryType::SAM
		== FromString<BoundaryType>(ToString(BoundaryType::SAM)) );
}

TEST( UnitBoundaryType, Names )
{
	ASSERT_EQ( true, string("None")
		== ToString(FromString<BoundaryType>("None")) );
	ASSERT_EQ( true, string("NoFlux")
		== ToString(FromString<BoundaryType>("NoFlux")) );
	ASSERT_EQ( true, string("SourceSink")
		== ToString(FromString<BoundaryType>("SourceSink")) );
	ASSERT_EQ( true, string("SAM")
		== ToString(FromString<BoundaryType>("SAM")) );
}

} // namespace
} // namespace
