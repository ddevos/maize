#ifndef PROJECT_MASSAGE_HANDLER_H_INCLUDED
#define PROJECT_MASSAGE_HANDLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface (and implementation) of ProjectMessageHandler.
 */

#include <QEventLoop>
#include <QObject>
#include "session/ISession.h"

namespace SimPT_Shell {
namespace Tests {

/**
 * Utility base class with an event loop to catch signal messages
 * from project, which is possibly in another thread.
 */
class ProjectMessageHandler : public QObject
{
	Q_OBJECT
public:
	ProjectMessageHandler()
		: QObject(), m_stepped(false), m_stepping_event_loop(),
		  m_stopped(false), m_stopping_event_loop()
	{
		connect(this, SIGNAL(Stopped()), &m_stopping_event_loop, SLOT(quit()));
		connect(this, SIGNAL(Stepped()), &m_stepping_event_loop, SLOT(quit()));
	}

	virtual ~ProjectMessageHandler() {}

	/// Clears the stepped flag
	void ClearStepped() { m_stepped = false; }

	/// Clears the stopped flag
	void ClearStopped() { m_stopped = false; }

	/// Wait until the project has taken a step
	void WaitUntilStepped()
	{
		if (m_stepped) {
			return;
		}
		m_stepping_event_loop.exec();
		m_stepping_event_loop.processEvents();
	}

	/// Wait until the project has stopped
	void WaitUntilStopped()
	{
		if (m_stopped) {
			return;
		}
		m_stopping_event_loop.exec();
		m_stopping_event_loop.processEvents();
	}

private:
	// Because of Qt wanting to match parameter types of signals and slots by string comparison.
	typedef SimShell::Session::ISession::InfoMessageReason InfoMessageReason;

public slots:
	/// Slot handling simulation info messages
	void SimulationInfo(const std::string& , const InfoMessageReason& reason)
	{
		if (reason == InfoMessageReason::Stopped || reason == InfoMessageReason::Terminated) {
			m_stopped = true;
			emit Stopped();
		} else if (reason == InfoMessageReason::Stepped) {
			m_stepped = true;
			emit Stepped();
		}
	}

	/// Slot handling simulation error messages
	void SimulationError(const std::string&)
	{
		m_stopped = true;
		emit Stopped();
	}

signals:
	void Stepped();

	void Stopped();

private:
	bool         m_stepped;
	QEventLoop   m_stepping_event_loop;
	bool         m_stopped;
	QEventLoop   m_stopping_event_loop;
};

} // namespace
} // namespace

#endif // end_of_include_guard
