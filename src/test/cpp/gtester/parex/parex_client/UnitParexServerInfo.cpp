/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of test of parameter exploration Server.
 */

#include "parex_protocol/ServerInfo.h"

#include <gtest/gtest.h>

namespace SimPT_Parex {
namespace Tests {

TEST( UnitParexServerInfo, CreateServer)
{
	ServerInfo* server = new ServerInfo(QString("Server"), QString("1.1.1.1"), 1234);
	ASSERT_EQ( true, server->GetName() 	== 	"Server");
	ASSERT_EQ( true, server->GetAddress()	== 	"1.1.1.1");
	ASSERT_EQ( true, server->GetPort() 	== 	1234);
	delete server;
}

TEST( UnitParexServerInfo, UpdateServer)
{
	ServerInfo* server = new ServerInfo(std::string("Server"), "1.1.1.1", 1234);
	server->Update("2.2.2.2", 2345);
	ASSERT_EQ( true, server->GetAddress() == 	"2.2.2.2");
	ASSERT_EQ( true, server->GetPort() 	== 	2345);
	delete server;
}

} // namespace
} // namespace
