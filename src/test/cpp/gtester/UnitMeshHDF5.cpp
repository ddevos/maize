/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of UnitMeshHDF5.
 */

#include "common/InstallDirs.h"
#include "bio/Mesh.h"
#include "bio/MeshState.h"
#include "exporters/Hdf5Exporter.h"
#include "fileformats/Hdf5File.h"
#include "sim/Sim.h"
#include "sim/SimState.h"
#include "workspace/CliWorkspace.h"
#include "workspace/StartupFilePtree.h"

#include <QString>
#include <QDir>
#include <boost/property_tree/xml_parser.hpp>
#include <gtest/gtest.h>

#include <iostream>

namespace SimPT_Sim {
namespace Tests {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Shell;
using namespace SimPT_Shell::Ws;
using namespace SimShell;

class UnitMeshHDF5 : public ::testing::TestWithParam<string>
{
protected:
	virtual ~UnitMeshHDF5() {}

	virtual void SetUp()
	{
		m_sim            = make_shared<Sim>();
		m_output_prefix  = InstallDirs::GetDataDir() + "/UnitMeshHDF5_";
	}

	virtual void TearDown()
	{
	}

	string            m_output_prefix;
	shared_ptr<Sim>   m_sim;
};

TEST_P(UnitMeshHDF5, MeshToHDF5ToMesh)
{
	if (!Hdf5File::HDF5Available()) {
		return;
	}

	// Assert that work space exists and define its workspace_model.
	QDir dir(QString::fromStdString(InstallDirs::GetDataDir()));
	ASSERT_EQ(true, dir.cd("simPT_Default_workspace"));
	CliWorkspace ws_model(dir.absolutePath().toStdString());

	// Prelim stuff.
	const string project = GetParam();
	
	// 1. Read the project file into a Mesh
	const auto file_ptr = --ws_model.Get(project)->end();
	const string path = file_ptr->second->GetPath();
	ASSERT_EQ(true, dir.exists(QString::fromStdString(file_ptr->second->GetPath())));
	auto sim_pt = static_pointer_cast<StartupFilePtree>(file_ptr->second)->ToPtree();
	m_sim->Initialize(sim_pt);

	// 2. Get SimState
	SimState sstate   = m_sim->GetState();
	MeshState mstate = sstate.GetMeshState();

	// 3. Export Mesh to HDF5
	const string out_file_name = m_output_prefix + project + string(".h5");
	Hdf5Exporter::Export(m_sim, out_file_name);

	// 4. Read HDF5 to MeshState using a HDF5MeshReader
	Hdf5File mreader(out_file_name);
	SimState sstate_new = mreader.Read(0);
	MeshState mstate_new = sstate_new.GetMeshState();

	// 5. Compare SimStates (parts of SimState: easier to track failures).
	// Project name doesn't get saved in HDF5, hence no test for that as of now.
	ASSERT_EQ(true, sstate.GetTimeStep()              == sstate_new.GetTimeStep());
	ASSERT_EQ(true, sstate.GetTime()                  == sstate_new.GetTime());
	ASSERT_EQ(true, sstate.GetParameters()            == sstate_new.GetParameters());
	ASSERT_EQ(true, sstate.GetRandomEngineState()     == sstate_new.GetRandomEngineState());

	// 6. Compare MeshStates (parts of MeshState: easier to track failures).
	// Nodes:
	ASSERT_EQ(true, mstate.GetNumNodes()              == mstate_new.GetNumNodes());
	ASSERT_EQ(true, mstate.GetNodesID()               == mstate_new.GetNodesID());
	ASSERT_EQ(true, mstate.GetNodesX()                == mstate_new.GetNodesX());
	ASSERT_EQ(true, mstate.GetNodesY()                == mstate_new.GetNodesY());
	// TODO: attributes

	// Cells:
	ASSERT_EQ(true, mstate.GetNumCells()              == mstate_new.GetNumCells());
	ASSERT_EQ(true, mstate.GetCellsID()               == mstate_new.GetCellsID());
	ASSERT_EQ(true, mstate.GetCellsNumNodes()         == mstate_new.GetCellsNumNodes());
	ASSERT_EQ(true, mstate.GetCellsNumWalls()         == mstate_new.GetCellsNumWalls());
	// TODO: GetCellNodes, GetCellWalls, attributes

	// Boundary polygon:
	ASSERT_EQ(true, mstate.GetBoundaryPolygonNodes()  == mstate_new.GetBoundaryPolygonNodes());
	ASSERT_EQ(true, mstate.GetBoundaryPolygonWalls()  == mstate_new.GetBoundaryPolygonWalls());
	// Walls:
	ASSERT_EQ(true, mstate.GetNumWalls()              == mstate_new.GetNumWalls());
	ASSERT_EQ(true, mstate.GetWallsID()               == mstate_new.GetWallsID());
	// TODO: GetWallNodes, GetWallCells, attributes
}

namespace {

string scenarios[] {
	"AuxinGrowth",
	"Geometric",
	"Meinhardt",
	"NoGrowth",
	"TipGrowth",
	"Wortel"
	};

}

INSTANTIATE_TEST_CASE_P(UnitMeshHDF5_a, UnitMeshHDF5, ::testing::ValuesIn(scenarios));

} // namespace
} // namespace

