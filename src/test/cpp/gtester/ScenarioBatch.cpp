/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of scenario test running demo cases in batch mode.
 */

#include "common/InstallDirs.h"
#include "cli/CliController.h"
#include "cli/CliWorkspaceManager.h"
#include "session/SimSession.h"
#include "util/misc/StringUtils.h"
#include "workspace/Preferences.h"

#include <boost/property_tree/ptree.hpp>
#include <gtest/gtest.h>
#include <memory>
#include <string>
#include <tuple>

using namespace std;
using namespace boost::property_tree;
using namespace SimShell::Ws;
using namespace ::testing;

namespace SimPT_Shell {
namespace Tests {

class ScenarioBatch: public TestWithParam<tuple<string, unsigned int>>
{
public:
	/// TestCase set up.
	static void SetUpTestCase()
	{
		// Create the workspace.
		g_ws_name = "simPT_Default_workspace";
		g_ws_path = InstallDirs::GetDataDir() + "/" + g_ws_name;
		g_ws_ptr = CliWorkspaceManager::OpenWorkspace(InstallDirs::GetDataDir(), g_ws_name, false);
		ASSERT_NE( nullptr, g_ws_ptr );
	}

	/// Tearing down TestCase
	static void TearDownTestCase()
	{
	}

protected:
	/// Destructor has to be virtual.
	virtual ~ScenarioBatch() {}

	/// Set up for the test fixture
	virtual void SetUp() {}

	/// Tearing down the test fixture
	virtual void TearDown() {}

	// Data member for test case
	static string                           g_ws_name;
	static string                           g_ws_path;
	static std::shared_ptr<CliWorkspace>    g_ws_ptr;
};

string                               ScenarioBatch::g_ws_name;
string                               ScenarioBatch::g_ws_path;
std::shared_ptr<CliWorkspace>   ScenarioBatch::g_ws_ptr;

TEST_P( ScenarioBatch, main )
{
	// --------------------------------------------------------------------------
	// Preliminaries.
	// --------------------------------------------------------------------------
	tuple<string, unsigned int> tup(GetParam());
	const string file_name = "leaf_000000.xml";
	const string project = string("scenario_batch_threading_") + get<0>(tup);

	// --------------------------------------------------------------------------
	// Set up new project and sim data file by copying template.
	// --------------------------------------------------------------------------
	auto project_ptr = CliWorkspaceManager::OpenProject(g_ws_ptr, project)->second.Project();
	ASSERT_NE( nullptr, project_ptr );

	// --------------------------------------------------------------------------
	// Set the sim.threaded preference
	// --------------------------------------------------------------------------
	auto pt = project_ptr->GetPreferences();
	project_ptr->SetPreferences(pt);

	// --------------------------------------------------------------------------
	// Make sure there's a sim data file.
	// --------------------------------------------------------------------------
	CliWorkspaceManager::OpenLeaf(project_ptr, file_name, true, g_ws_path , get<0>(tup), file_name);

	// --------------------------------------------------------------------------
	// Set up controller and task and execute.
	// --------------------------------------------------------------------------
	auto      controller = CliController::Create(g_ws_ptr, true);
	ASSERT_NE( nullptr, controller );
	CliSimulationTask   task(project, true, file_name, true, get<1>(tup));
	const int status = controller->Execute(task);
	ASSERT_EQ( EXIT_SUCCESS, status ) << "CliController::Execute failed for: " << task;
}

namespace {

tuple<string, unsigned int> scenarios[] {
	 make_tuple("AuxinGrowth",  10U)
	,make_tuple("Geometric",    20U)
	,make_tuple("Meinhardt",    20U)
        ,make_tuple("NoGrowth",     20U)
	,make_tuple("TipGrowth",    10U)
	,make_tuple("Wortel",       1U)
	};

}

INSTANTIATE_TEST_CASE_P(RunBatch, ScenarioBatch, ValuesIn(scenarios));

} // namespace
} // namespace

