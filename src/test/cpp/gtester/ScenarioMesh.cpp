/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of scenario test running demo cases in batch mode.
 */

#include "common/InstallDirs.h"
#include "bio/MeshCheck.h"
#include "cli/CliController.h"
#include "cli/CliWorkspaceManager.h"
#include "session/SimSession.h"
#include "sim/Sim.h"
#include "util/misc/StringUtils.h"
#include "workspace/StartupFilePtree.h"

#include <gtest/gtest.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace ::testing;
using namespace SimPT_Sim;

namespace SimPT_Shell {
namespace Tests {

class ScenarioMesh: public TestWithParam<tuple<string, unsigned int>>
{
public:
	/// TestCase set up.
	static void SetUpTestCase()
	{
		g_ws_path = InstallDirs::GetDataDir() + "/simPT_Default_workspace";
	}

	/// Tearing down TestCase
	static void TearDownTestCase()
	{
	}

protected:
	/// Destructor has to be virtual.
	virtual ~ScenarioMesh() {}

	/// Set up for the test fixture
	virtual void SetUp() {}

	/// Tearing down the test fixture
	virtual void TearDown() {}

	// Data member for test case
	static string                                g_ws_path;
};

string                               ScenarioMesh::g_ws_path;

TEST_P( ScenarioMesh, MeshConsistency )
{
	tuple<string, unsigned int> tup(GetParam());
	const string input_path = g_ws_path + "/" + get<0>(tup) + "/" + "leaf_000000.xml";
	const unsigned int num_iter = get<1>(tup);

	Sim sim;
	auto f  = Ws::StartupFileBase::Constructor(input_path, input_path);
	auto pt = static_pointer_cast<Ws::StartupFilePtree>(f)->ToPtree();

	sim.Initialize(pt);
	const Mesh& mesh = *sim.GetCoreData().m_mesh;

	EXPECT_TRUE( MeshCheck(mesh).CheckAreas() );
	EXPECT_TRUE( MeshCheck(mesh).CheckAtBoundaryNodes() );
	EXPECT_TRUE( MeshCheck(mesh).CheckCellBoundaryWallNodes() );
	EXPECT_TRUE( MeshCheck(mesh).CheckCellBoundaryWalls() );
	EXPECT_TRUE( MeshCheck(mesh).CheckCellIdsSequence() );
	EXPECT_TRUE( MeshCheck(mesh).CheckCellIdsUnique() );
	EXPECT_TRUE( MeshCheck(mesh).CheckEdgeOwners() );
	EXPECT_TRUE( MeshCheck(mesh).CheckMutuallyNeighbors() );
	EXPECT_TRUE( MeshCheck(mesh).CheckNodeIdsSequence() );
	EXPECT_TRUE( MeshCheck(mesh).CheckNodeIdsUnique() );
	EXPECT_TRUE( MeshCheck(mesh).CheckNodeOwningNeighbors() );
	EXPECT_TRUE( MeshCheck(mesh).CheckNodeOwningWalls() );
	EXPECT_TRUE( MeshCheck(mesh).CheckWallIdsSequence() );
	EXPECT_TRUE( MeshCheck(mesh).CheckWallIdsUnique() );
	EXPECT_TRUE( MeshCheck(mesh).CheckWallNeighborsList() );

	for(unsigned int i = 0; i < num_iter; ++i) {
		sim.TimeStep();

		EXPECT_TRUE( MeshCheck(mesh).CheckAreas() );
		EXPECT_TRUE( MeshCheck(mesh).CheckAtBoundaryNodes() );
		EXPECT_TRUE( MeshCheck(mesh).CheckCellBoundaryWallNodes() );
		EXPECT_TRUE( MeshCheck(mesh).CheckCellBoundaryWalls() );
		EXPECT_TRUE( MeshCheck(mesh).CheckCellIdsSequence() );
		EXPECT_TRUE( MeshCheck(mesh).CheckCellIdsUnique() );
		EXPECT_TRUE( MeshCheck(mesh).CheckEdgeOwners() );
		EXPECT_TRUE( MeshCheck(mesh).CheckMutuallyNeighbors() );
		EXPECT_TRUE( MeshCheck(mesh).CheckNodeIdsSequence() );
		EXPECT_TRUE( MeshCheck(mesh).CheckNodeIdsUnique() );
		EXPECT_TRUE( MeshCheck(mesh).CheckNodeOwningNeighbors() );
		EXPECT_TRUE( MeshCheck(mesh).CheckNodeOwningWalls() );
		EXPECT_TRUE( MeshCheck(mesh).CheckWallIdsSequence() );
		EXPECT_TRUE( MeshCheck(mesh).CheckWallIdsUnique() );
		EXPECT_TRUE( MeshCheck(mesh).CheckWallNeighborsList() );
	}
}

namespace {

tuple<string, unsigned int> scenarios[] {
	make_tuple("AuxinGrowth",  500U),
	make_tuple("Geometric",    500U),
	make_tuple("Meinhardt",    500U),
	make_tuple("TipGrowth",    500U),
	make_tuple("Wortel",         1U),
        make_tuple("NoGrowth",     400U),
	};

}

INSTANTIATE_TEST_CASE_P(RunMeshCheck, ScenarioMesh, ValuesIn(scenarios));

} // namespace
} // namespace
