/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package be.ac.ua.simPT.test;

import org.junit.*;
import static org.junit.Assert.*;

import be.ac.ua.simPT.*;
import java.io.StringReader;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class UnitSimWrapperXML {
	private SimWrapper sim;
	private String path;

	@Before
	public void setUp() {
		// Adding the libsimPT_sim library is only required on windows
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
			System.loadLibrary("libsimPTlib_sim");
			
		System.loadLibrary("simPT_sim_jni");
		sim = new SimWrapper();
		path = "../data/simPT_Default_workspace";
	}

	@Test
	public void testXMLState() throws Exception {
		{
			ResultVoid r = sim.Initialize(path + "/AuxinGrowth/leaf_000000.xml");
			assertTrue(r.getStatus() == SimWrapperStatus.SUCCESS);
		}
		{
			XPath xpath = XPathFactory.newInstance().newXPath();
			DocumentBuilder b = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			ResultString r = sim.GetXMLState();
			InputSource s = new InputSource(new StringReader(r.getValue()));
			Document xml = b.parse(s);
			assertEquals("AuxinGrowth", xpath.evaluate("//project", xml));
			assertEquals(Float.parseFloat("385.5570683183"), 
				Float.parseFloat(xpath.evaluate("//mesh//cell[2]/area", xml)), 1.0e-7);
			assertEquals(Float.parseFloat("3.09017"), 
				Float.parseFloat(xpath.evaluate("//parameters/cell_mechanics/target_node_distance", xml)), 1.0e-7);
			assertEquals(Float.parseFloat("16.962"),
				Float.parseFloat(xpath.evaluate("//mesh//node[5]/y", xml)), 1.0e-7);
		}
	}
}
