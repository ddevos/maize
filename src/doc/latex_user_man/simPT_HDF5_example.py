import h5py
import argparse
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection

#==============================================================================
# Check for command line options
parser = argparse.ArgumentParser(description='Plot simPT HDF5 results')
parser.add_argument(
    'filename',
    action='store',
    help='simPT HDF5 file'
)
parser.add_argument(
    '--step', '-s',
    action='store',
    dest='step_idx',
    help='Index of simulation step to load'
)
args = parser.parse_args()

#==============================================================================
# Open & read the HDF5 file in read-only mode
tissue_file = h5py.File(args.filename, 'r')

if args.step_idx != None:
    step_idx = args.step_idx
else:
    # Take the last step if none was given
    step_idx = tissue_file['time_steps_idx'][-1]

# Read a subset of the tissue data from a particular step
# (after checking it actually exists)
step_grp_name = ('/step_{0}').format(step_idx)
if step_grp_name not in tissue_file:
    print ('Step with index {0} not found!').format(step_idx)
    exit(1)
else:
    step_grp = tissue_file[step_grp_name]

nodes_id = step_grp['nodes_id'][...]
nodes_xy = step_grp['nodes_xy'][...]

cells_id = step_grp['cells_id'][...]
cells_num_nodes = step_grp['cells_num_nodes'][...]
cells_nodes = step_grp['cells_nodes'][...]
cells_chem = step_grp['cells_attr_chem_0'][...]

# Close HDF5 file
tissue_file.close()

#==============================================================================
# Some minor processing of the data
num_nodes = nodes_id.shape[0]
num_cells = cells_id.shape[0]

print ('Number of nodes: {0}').format(num_nodes)
print ('Number of cells: {0}').format(num_cells)

# Dictionary that translates from node IDs to node index values
nodes_idx = {nodes_id[node_idx]: node_idx for node_idx in xrange(num_nodes)}

#==============================================================================
# Create the figure
fix,ax = plt.subplots()

# Create a list of polygons that represent the cells
polygons = []
for cell_idx in np.arange(num_cells):
    # IDs of the nodes that define the current cell ...
    cell_nodes_id = cells_nodes[cell_idx, :cells_num_nodes[cell_idx]]
    # ... are translated to node index values ...
    cell_nodes_idx = [nodes_idx[node_id] for node_id in cell_nodes_id]
    # ... and then used to extract appropriate node coordinates for this cell
    cell_nodes_xy = nodes_xy[cell_nodes_idx]

    # Create the cell's polygon and add it to the list of polygons to draw
    cell_poly = Polygon(cell_nodes_xy)
    polygons.append(cell_poly)

# Collection of patches to draw is created from the list of polygons
patch_coll = PatchCollection(polygons, cmap=cm.viridis)

# Set the array of per-polygon (cell) values used to color the cells
# (in this case, values of the first cell-based chemical saved as
# a cell-based attribute with the name 'chem_0')
patch_coll.set_array(cells_chem)

# Add the collection to draw to the axis
ax.add_collection(patch_coll)

# Create a legend-like color bar to show the range of cell based values
plt.colorbar(patch_coll)

# Set figure properties
# Adapt figure ranges to the tissue size
ax.set_xlim(np.min(nodes_xy[:, 0]) - 10, np.max(nodes_xy[:, 0]) + 10)
ax.set_ylim(np.min(nodes_xy[:, 1]) - 10, np.max(nodes_xy[:, 1]) + 10)
# Use a square grid
ax.set_aspect(1.)
ax.grid()
# Optional: In simPT the y-axis faces downwards, to make plots look
# similar to these from simPT you can just flip the y-axis with:
ax.invert_yaxis()

# Show the figure on screen
plt.show()

