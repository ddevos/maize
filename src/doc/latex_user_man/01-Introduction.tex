%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################

\chapter{Introduction}
\label{chap:Introduction}
  
This manual provides a brief description of the 
Virtual Plant Tissue a.k.a. VPTissue. 
VPTissue is a cell based computer modeling framework for plant 
tissue morphogenesis. It provides a means for plant researchers 
to analyze the biophysics of growth and patterning.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The VPTissue toolset}
\label{section:toolset}

The Virtual Plant Tissue toolset consists of:
\begin{description}
	\item[Simulator (\texttt{simPT\_sim})] \ \\
		The actual simulator that evolves the organ using the model 
		specified in the input file. It includes capability for conversion 
		between output file formats and for post-processing the simulation output.
		It can be used interactively, with a graphical user interface, or non-interactively
		with a command line interface.
 	\item[Tissue Editor (\texttt{simPT\_editor})] \ \\
		A graphical editor capable of editing the geometry of the mesh representing 
		the plant organ. It can also be used to edit the attributes of cells and walls.
		The Editor can only be used interactively with a graphical user interface.
	\item[Parameter Explorer (\texttt{simPT\_parex})] \ \\
		Facilitates the study of the parameter dependence of the simulation 
		results by distributing calculations over multiple systems. It consists of a client
		program that functions through a graphical user interface and a server and a worker
		programs that operate via a command line interface.
\end{description}
Each of these tools is discussed in some detail in the following chapters.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Project background}
The Simulation of Plant Tissue project grew out out of extensive contacts between the 
Antwerp group of VPTissue authors and the authors of  VirtualLeaf (\cite{Merks2007}, \cite{Merks2011}, \cite{Vos2012}). In many ways, VPTissue is an offspring of VitualLeaf, in other ways it is completely new and state-of-the-art. It has a totally new code base, takes advantage of the multi-core architecture of present day systems and is current in its use of libraries. More importantly, it introduces new features that are biologically relevant: new models, dynamic models, coupled models.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Documentation}

The VPTissue documentation consists of (a) a user manual in \texttt{pdf} format (i.e. this document), and (b) a developer's reference manual in \texttt{html} format and (c) inline comments in the source code. The user manual has been written in latex (see \url{www.latex-project.org}) and is generated with hyperlinks for easy navigation. The Application Programmer Interface (API) documentation in the reference manual is generated automatically from documentation instructions embedded in the code using the Doxygen tool (see \url{www.doxygen.org}). Additional developer documentation has been written in the doxygen syntax and is included in the reference manual. Figure \ref{fig:doxygenapi} presents the starting page of the API documentation. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Installation}

For instructions on how to install VPTissue, see the file INSTALL.txt in the root directory. 
To check dependence on external resources, see DEPENDENCIES.txt in the root directory. 
To check issues specific to your platform, see PLATFORMS.txt in the root directory.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.35]{images/ScreenShotDoxygenAPI.png}}
	\end{center}
	\caption{Screen shot of the main page of the API documentation. The tabs at the top of page provide access to documentation for namespaces, classes and files.}
	\label{fig:doxygenapi}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Known Issues}
As with almost any software application the size of VPTissue there are a number of known issues where the combination of operating systems and third party library and application software has an issue that cannot be addressed. There are a few such issues in VPTissue and they are listed in the file KNOWN\_ISSUES.txt in the top level directory of the projects. When it is available a fix or workaround is suggested.