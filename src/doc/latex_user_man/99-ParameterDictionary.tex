%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################


\chapter{Parameter Dictionary}
\label{chap:Parameter Dictionary}

An overview of all parameters used in the simulator, specifying the defaults (if any), the 
choices that are currently available and a (very) brief indication of the role the parameter plays.

The parameter are subdivided in sections. These correspond to sub-nodes in the xml tree representation of the parameter set with the same name. Most of the parameters in the model section (cell\_chemical\_count and time\_step being the exceptions) refer to model components. 

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in model}
\begin{compactdesc}
%
\item[group]\ \\
	The name of the model group that this model belongs to.
	\emph{choices:} Default, plus any groups you yourself have developed.
%
\item[name] \ \\
   The name of the model to be simulated. \\
	\emph{choices:} AuxinGrowth, Geometric, Meinhardt, SmithPyllotaxis, NoGrowth, TipGrowth, 
	TestCoupling, TestCoupling\_I, TestCoupling\_II, Wortel, WrapperModel
%
\item[cell\_chemical\_count] \ \\
	The number of chemicals in each cell. \\
	\emph{choices:} any positive integer number
%
\item[cell\_chemistry] \ \\
	The method defining the dynamics of  chemicals inside cells (e.g. auxin, cytokinin, ...). \\
	\emph{choices:} AuxinGrowth, Meinhardt, NoOp, PINFlux, PINFlux2, SmithPyllotaxis, Source, 
	TestCoupling, Wortel, WrapperModel
%
\item[cell\_daughters] \ \\
	The method defining the partitioning of chemicals between parent and daughter cells upon division. \\
	\emph{choices:} Auxin, BasicPIN, NoOp, Perimeter, PIN, SmithPyllotaxis, Wortel, WrapperModel
%
\item[cell\_housekeep] \ \\
	The method for updating parameters related to cell and cell wall growth. \\
	\emph{choices:} Auxin, AuxinGrowth, BasicAuxin, Geometric, Meinhardt, NoOp,
	SmithPyllotaxis, Wortel, WrapperModel
%
\item[cell\_split] \ \\
	The method defining the cell division rules (e.g. cell divides, when its area is two times larger than the base area of the cell). \\
	\emph{choices:}  AreaThresholdBased, AuxinGrowth, Geometric, 
	NoOp, Wortel, WrapperModel
%
\item[cell2cell\_transport] \ \\
	The method defining transport dynamics (influx/efflux) across from cell to cell across cell walls. \\
	\emph{choices:} AuxinGrowth, Basic, Meinhardt, NoOp, Plain, SmithPyllotaxis, Source, 
	TestCoupling, Wortel, WrapperModel
%
\item[cell2cell\_transport\_boundary] \ \\
	Boundary conditions for cell to cell transport in coupled systems. \\
	\emph{choices:}  TestCoupling\_I, TestCoupling\_II 
%
\item[mc\_delta\_hamiltonian] \ \\
	The DeltaHamiltonian used in the Metropolis method for the equilibration of cell and wall mechanics. \\
	\emph{choices:} ElasticWall, Maxwell, ModifiedGC, PlainGC
%
\item[mc\_hamiltonian] \ \\
		The Hamiltonian used in the Metropolis method for the equilibration of cell and wall mechanics. \\
	\emph{choices:} ElasticWall, Maxwell, ModifiedGC, PlainGC
%
\item[mc\_move\_generator] \ \\
	Algorithm to generate random displacements for nodes in the Metropolis sweep. \\
	\emph{choices:} directed\_normal, directed\_uniform, standard\_normal, standard\_uniform
%
\item[time\_evolver] \ \\
	The name of the time evolution scheme to be used. \\
	\emph{choices:} Grow, Housekeep, HousekeepGrow, VPTissue, Transport, VLeaf
%
\item[time\_step] \ \\
 	The time step used in the time evolution in the simulator. \\
	\emph{choices:} any real number
%
\item[wall\_chemistry] \ \\
	The method defining the dynamics of chemicals in cell walls (e.g. PIN transporters). \\
	\emph{choices:} AuxinGrowth, Basic, Meinhardt, NoOp
%
\end{compactdesc}
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUXIN_TRANSPORT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in auxin\_transport}
\begin{compactdesc}
%
	\item[aux\_breakdown] \ \\
		Rate constant of auxin breakdown. \\
		\emph{choices:} any real number
%
	\item[aux\_cons] \ \\
		Auxin production rate outside shoot apical meristem. \\
		\emph{choices:} any real number
%
	\item[aux1prod] \ \\
		Rate of auxin production. \\
		\emph{choices:} any real number
%
	\item[D] \ \\
		Vector of diffusion coefficients. \\
		\emph{choices:} any real number
%
	\item[initval] \ \\
		Vector specifying standard starting values for cellular levels of chemicals after, for instance, division. \\
		\emph{choices:} any real number
%
	\item[k1] \ \\
		Rate constant for PIN translocation to the cell membrane (cfr.~\cite{Merks2007}). \\
		\emph{choices:} any real number
%
	\item[k2] \ \\
		Rate constant for return from cell membrane to endosome (cfr.~\cite{Merks2007}). \\
		\emph{choices:} any real number
%
	\item[ka] \ \\
		Saturation constant for PIN mediated transport of auxin (cfr.~\cite{Merks2007}). \\
		\emph{choices:} any real number
%
	\item[km] \ \\
		Saturation constant for exocytosis (cfr.~\cite{Merks2007}. \\
		\emph{choices:} any real number
%
	\item[kr] \ \\
		Saturation constant for auxin dependence of PIN translocation (cfr.~\cite{Merks2007}. \\
		\emph{choices:} any real number
%
	\item[leaf\_tip\_source] \ \\
		Auxin flux at the tissue boundary. \\
		\emph{choices:} any real number
%
	\item[pin\_breakdown] \ \\
		Intracellular PIN breakdown rate constant. \\
		\emph{choices:} any real number
%
	\item[pin\_production] \ \\
		Cellular PIN production rate. \\
		\emph{choices:} any real number
%
	\item[pin\_production\_in\_epidermis] \ \\
		Cellular PIN production rate in epidermal cells (at boundary). \\
		\emph{choices:} any real number
%
	\item[r] \ \\
		Total level of surface auxin receptors. \\
		\emph{choices:} any real number
%
	\item[sam\_auxin] \ \\
		Auxin level at the boundary with the apical meristem (sink). \\
		\emph{choices:} any real number
%
	\item[sam\_auxin\_breakdown] \ \\
		Rate constant for auxin breakdown in shoot apical meristem. \\
		\emph{choices:} any real number
%
	\item[sam\_efflux] \ \\
		Maximum auxin efflux rate to shoot apical meristem. \\
		\emph{choices:} any real number
%
	\item[transport] \ \\
		Active (mediated) transport rate constant. \\
		\emph{choices:} any real number
%
\end{compactdesc}
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BLAD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in Blad0032, Blad0128, Blad0512 (leaf models)}

These parameters are applicable only in the Blad (leaf) type models. \\

\begin{compactdesc}
%
	\item[grid\_size] \ \\
	  Number of cells in the starting grid. \\
  	%\emph{default:} no limit \\
		\emph{choices:} integer: 32, 128, or 512.
%
	\item[M\_degradation] \ \\
	  Morphogen degradation rate constant. \\
		\emph{choices:} real number
%
	\item[M\_diffusion\_constant] \ \\
	  Morphogen diffusion constant. \\
		\emph{choices:} real number
%
	\item[M\_duration] \ \\
	  Duration of morphogen production. \\
		\emph{choices:} real number
%
	\item[M\_production] \ \\
	  Morphogen production rate. \\
		\emph{choices:} real number
%
	\item[M\_threshold\_expansion] \ \\
	  Minimum morphogen concentration for cell expansion. \\
		\emph{choices:} real number
%
	\item[M\_threshold\_division] \ \\
	  Minimum morphogen concentration for cell division. \\
		\emph{choices:} real number
%
	\item[relative\_growth] \ \\
	  Rate of target area change (per housekeeping step). \\
		\emph{choices:} real number
%
	\item[size\_threshold\_division] \ \\
	  Minimum cell area for division. \\
		\emph{choices:} real number
%
\end{compactdesc}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CELL_MECHANICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in cell\_mechanics}
\begin{compactdesc}
%
	\item[auxin\_dependent\_growth] \ \\
		Dependence of growth on auxin. \\
		\emph{choices:} true, false
%
	\item[base\_area] \ \\
		Base area of the cell used in the cell division condition. \\
		\emph{default:} 293.893 \\
		\emph{choices:} any positive real number
%
	\item[cell\_expansion\_rate] \ \\
		Rate of target area change (per housekeeping step). \\
		\emph{choices:} any real number
%
	\item[collapse\_node\_threshold] \ \\
		Parameter defining the minimal distance between existing node and new node: if the new node is far enough (e.g. 5\% of edge length) from one of the two existing nodes, the new node is inserted, else the existing node is used. \\
		\emph{default:} 0.05 \\
		\emph{choices:} any real number
%
	\item[copy\_wall] \ \\
		Copying wall elements to appropriate cells during cell division. \\
		\emph{choices:} true, false
%
	\item[division\_ratio] \ \\
		Cell division factor: multiplier of base area. \\
		\emph{default:} 2.0 \\
		\emph{choices:} any real number
%
	\item[elastic\_modulus] \ \\
		Young elastic modulus in Elastic Wall and Maxwell models. \\
		\emph{choices:} any real number
%
	\item[lambda\_alignment] \ \\
		Strength of cell alignment along given direction. \\
		\emph{choices:} any real number
%
	\item[lambda\_bend] \ \\
		Strength of vertex bending in cell polygon. \\
		\emph{choices:} any real number
%
	\item[lambda\_celllength] \ \\
		Strength of the cell length constraint. \\
		\emph{choices:} any real number
%
	\item[lambda\_length] \ \\
		Spring constant of the edge (wall element): strength of the edge length constraint. \\
		\emph{choices:} any real number
%
	\item[mc\_abs\_tolerance] \ \\
  	Absolute tolerance used in decision to continue Metropolis Monte Carlo sweeping: once 
  		below this tolerance we stop. \\
		\emph{choices:} any real number 
%
	\item[mc\_do\_not\_use\_delta] \ \\
  	Parameter for using the calculation of total energy (hamiltonian) or just energy difference (delta\_hamiltonian). \\
		\emph{choices:} false, true 
%
	\item[mc\_rel\_tolerance] \ \\
		Relative tolerance used in Metropolis Monte Carlo sweeping: once below tolerance (relative 
  		to total mesh energy) we stop. \\
		\emph{choices:} any real number
%
	\item[mc\_retry\_limit] \ \\
		How many times we do additional Metropolis Monte Carlo sweeps when the current sweep increases energy instead of lowering it. \\
		\emph{choices:} unsigned integer
%
	\item[mc\_sliding\_window] \ \\
		Metropolis Monte Carlo sweeping averages energy changes over a number of mc\_sliding\_window 
  		sweeps to decide to stop or continue. \\
		\emph{choices:} unsigned integer
%
	\item[mc\_stepsize] \ \\
		Maximum node displacement distance: multiplier of mc\_move\_generator(). It is used in the displacements of the nodes in the Metropolis Monte Carlo algorithm for finding the equilibrated state. \\
		\emph{choices:} any real number 
%
	\item[mc\_step\_x\_scale] \ \\
		For move generator of directed type, this number scales steps in 
  		the x direction. \\
		\emph{choices:} any  real number
%
	\item[mc\_step\_y\_scale] \ \\
		For move generator of directed type, this number scales steps in 
  		the y direction. \\
		\emph{choices:} any real number
%
	\item[mc\_sweep\_limit] \ \\
		Maximum number of sweeps that can be executed in a single time step: to avoid infinite sweeps. \\
		\emph{choices:} unsigned integer
%
	\item[mc\_temperature] \ \\
	  Amount of noise; extent to which the system accepts energetically unfavourable moves. \\
%		Temperature used in the Metropolis Monte Carlo sweeps to decide acceptance or rejection of	a node displacement. \\
		\emph{choices:} any positive real number
%
	\item[relative\_perimeter\_stiffness] \ \\
		Ratio between spring constants (lambda\_length) of perimeter edges (wall elements) and internal edges (wall elements). \\
		\emph{choices:} any positive real number
%
	\item[response\_time] \ \\
		Response time needed for turgor regulation used in Maxwell model. \\
		\emph{default:} 0.1 \\
		\emph{choices:} any positive real number
%
	\item[target\_node\_distance] \ \\
		Rest length of edges (wall elements): constant and the same for all edges. \\
		\emph{default:} 3.09017 \\
		\emph{choices:} any positive real number
%
	\item[viscosity\_const] \ \\
		Viscosity used in Maxwell model. \\
		\emph{choices:} any positive real number
%
	\item[yielding\_threshold] \ \\
		Parameter used in edge (wall element) yielding threshold: multiplier of target\_node\_distance. \\
		\emph{choices:} any positive real number
%
\end{compactdesc}
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ODE_INTEGRATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
\section{Parameters in ode\_integration}
\begin{compactdesc}
%
	\item[abs\_tolerance] \ \\
		Absolute tolerance used in the ODE solver algorithms.\\
		\emph{choices:} any positive real number
%
	\item[ode\_solver] \ \\
		ODE solver algorithm to be used for reaction, diffusion and active transport equations. \\
		\emph{default:} runge\_kutta\_dopri5 \\
		\emph{choices:} adams\_bashforth, bulirsch\_stoer, euler, modified\_midpoint, runge\_kutta4, 
  						runge\_kutta\_cash\_karp54, runge\_kutta\_dopri5, runge\_kutta\_fehlberg78 
%
	\item[rel\_tolerance] \ \\
		Relative tolerance used in de ODE solver algorithms. \\
		\emph{choices:} any positive real number
%
	\item[small\_time\_increment] \ \\
		Time increment used in ode\_solver. \\
		\emph{choices:} any positive real number
%
\end{compactdesc}
  
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RANDOM_ENGINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in random\_engine}
\begin{compactdesc}
%
	\item[type] \ \\
		The random number engines available for use (see the C++11 \\Standard Library documentation). \\
		\emph{default:} mt19937\_64
		\emph{choices:} minstd\_rand0, minstd\_rand, mt19937, mt19937\_64, ranlux24\_base, ranlux48\_base, knuth\_b
%
	\item[seed] \ \\
		The seed for the random engine (the default makes the simulation results non-reproducible).\\
		\emph{default:} time reading generates random seed \\
		\emph{choices:} unsigned integer
%
\end{compactdesc}
  
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SMITH_PHYLLOTAXIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in smith\_phyllotaxis}

These parameters are applicable only in the SmithPhyllotaxis model. \\

\begin{compactdesc}
%
	\item[k\_IAA] \ \\
		 IAA production limiting coefficient (saturation). \\
		\emph{choices:} any real number
%
	\item[k\_PIN] \ \\
		 PIN1 production limiting coefficient (saturation). \\
		\emph{choices:} any real number
%
	\item[k\_T] \ \\
		 Coefficient controlling the saturation of IAA transport. \\
		\emph{choices:} any real number
%
	\item[mu\_IAA] \ \\
		 IAA decay. \\
		\emph{choices:} any real number
%
	\item[mu\_PIN] \ \\
		 PIN1 decay. \\
		\emph{choices:} any real number
%
	\item[rho\_IAA] \ \\
		  IAA production. \\
		\emph{choices:} any real number
%
	\item[rho\_PIN0] \ \\
		 Auxin independent PIN1 production. \\
		\emph{choices:} any real number
%
	\item[rho\_PIN] \ \\
		 PIN1 production. \\
		\emph{choices:} any real number
%
	\item[c] \ \\
		 Controls PIN1 distribution. \\
		\emph{choices:} any real number
%
	\item[D] \ \\
		 IAA diffusion coefficient. \\
		\emph{choices:} any real number
%
	\item[T] \ \\
		 IAA active transport coefficient. \\
		\emph{choices:} any real number
%
\end{compactdesc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TERMINATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in termination}
\begin{compactdesc}
%
	\item[max\_cell\_count] \ \\
	  Maximal number of cells in the mesh to be reached for the pause (in GUI mode) or termination (in CLI mode) of the simulator. \\
%		Pause (in GUI mode) or terminate (in CLI mode) the simulator when the mesh has many cells. \\
		\emph{default:} no limit \\
		\emph{choices:} unsigned integer
%
	\item[max\_sim\_time] \ \\
	  Maximal number of time steps to be reached for the pause (in GUI mode) or termination (in CLI mode) of the simulator. \\
%		Pause (in GUI mode) or terminate (in CLI mode) the simulator after this many time steps. \\
  	\emph{default:} no limit \\
		\emph{choices:} unsigned integer
%
	\item[stationarity\_check] \ \\
		Stationarity of ODE solution within abs\_tolerance to be reached for the termination of the simulator. \\
		\emph{choices:} true, false
%
\end{compactdesc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TESTCOUPLING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in TestCoupling, TestCoupling\_I, TestCoupling\_II}
%\section{Parameters in TestCoupling}
These parameters are applicable only in the TestCoupling type models. \\

\begin{compactdesc}
%
	\item[ch0\_breakdown] \ \\
	  Chemical 0 degradation rate constant. \\
		\emph{choices:} real number
%
	\item[ch0\_production] \ \\
	  Chemical 0 production rate. \\
		\emph{choices:} real number
%
	\item[ch1\_breakdown] \ \\
	  Chemical 1 degradation rate constant. \\
		\emph{choices:} real number
%
	\item[ch1\_production] \ \\
	  Chemical 1 production rate. \\
		\emph{choices:} real number
%
	\item[D] \ \\
		Vector of transport coefficients for individual models. \\
		\emph{choices:} any real number
%
	\item[diffusion] \ \\
		Transport coefficient for coupled cells in coupled simulation (one value per cell couple). \\
		\emph{choices:} any real number
%
	\item[sim\_ODE\_coupling\_steps] \ \\
	  Number of coupling steps per simulation time step. \\
		\emph{choices:} real number
%
\end{compactdesc}

				
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WORTEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in Wortel (root)}

These parameters are applicable only in the Wortel (root) model. \\

\begin{compactdesc}
%
	\item[apoplast\_thickness] \ \\
	  Assumed thickness of the apoplast compartment separating two cells. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[aux\_breakdown] \ \\
	  Cellular auxin breakdown rate constant. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[aux\_production] \ \\
	  Cellular auxin production rate constant. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[aux\_shy2\_breakdown] \ \\
	  Auxin-dependent Shy2 breakdown rate constant. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[aux\_sink] \ \\
	  Rate constant for outgoing auxin flux. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[aux\_source] \ \\
	  Rate constant for incoming auxin flux. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[ck\_breakdown] \ \\
	  Cellular cytokinin breakdown rate constant. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[ck\_shy2\_production] \ \\
	  Cytokinin dependent production rate constant. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[ck\_sink] \ \\
	  Rate constant for outgoing cytokinin flux. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[ck\_source] \ \\
	  Rate constant for incoming cytokinin flux. \\
  	%\emph{default:} no limit \\
		\emph{choices:} real number
%
	\item[D] \ \\
		Vector of diffusion coefficients (index '0' for auxin; index '1' for cytokinin). \\
		\emph{choices:} any real number
%
	\item[ga\_breakdown] \ \\
		Cellular gibberellin breakdown rate constant. \\
		\emph{choices:} any real number
%
	\item[ga\_production] \ \\
		Cellular gibberellin production rate constant. \\
		\emph{choices:} any real number
%
	\item[ga\_threshold] \ \\
		Minimum gibberellin concentration for cell division and growth. \\
		\emph{choices:} any real number
%
	\item[k\_export] \ \\
		Auxin export permeability. \\
		\emph{choices:} any real number
%
	\item[k\_import] \ \\
		Auxin import permeability. \\
		\emph{choices:} any real number
%
	\item[km\_aux\_ck] \ \\
		Auxin-dependent cytokinin inhibition constant. \\
		\emph{choices:} any real number
%
	\item[km\_aux\_shy2] \ \\
		$S_{0.5}$ for auxin-dependent Shy2 breakdown . \\
		\emph{choices:} any real number
%
	\item[km\_shy] \ \\
		Shy2-dependent auxin export inhibition constant. \\
		\emph{choices:} any real number
%
	\item[shy2\_breakdown] \ \\
		Cellular Shy2 breakdown rate constant. \\
		\emph{choices:} any real number
%
	\item[shy2\_production] \ \\
		Cellular Shy2 production rate constant. \\
		\emph{choices:} any real number
%
	\item[shy2\_threshold] \ \\
		Maximum Shy2 concentration for cell division. \\
		\emph{choices:} any real number
%
	\item[vm\_aux\_ck] \ \\
		Auxin-dependent cytokinin production rate constant. \\
		\emph{choices:} any real number
%
\end{compactdesc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRAPPERMODEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameters in WrapperModel}
These parameters are applicable only in the WrapperModel model. \\

\begin{compactdesc}
%
	\item[cell\_division\_threshold] \ \\
	  Minimum cell area for division. \\
		\emph{choices:} real number
%	
    \item[ch0\_breakdown] \ \\
	  Chemical 0 degradation rate constant. \\
		\emph{choices:} real number
%
	\item[ch0\_production] \ \\
	  Chemical 0 production rate constant. \\
		\emph{choices:} real number
%
	\item[ch0\_threshold] \ \\
	  Minimal chemical 0 concentration for cell expansion. \\
		\emph{choices:} real number
%
	\item[ch1\_breakdown] \ \\
	  Chemical 1 degradation rate constant. \\
		\emph{choices:} real number
%
	\item[ch1\_production] \ \\
	  Chemical 1 production rate constant. \\
		\emph{choices:} real number
%
	\item[D] \ \\
		Vector of transport coefficients. \\
		\emph{choices:} any real number
%
	\item[expansion\_rate] \ \\
	  Rate of target area change (per housekeeping step). \\
		\emph{choices:} real number
%
\end{compactdesc}

