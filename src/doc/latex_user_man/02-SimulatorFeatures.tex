%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################


\chapter{VPTissue features}

The design of Virtual Plant Tissue intends to make it a rich, flexible and extensible environment for developing simulations of Plat Tissue processes. In this chapter we present a brief overview of of the key feature of VPTissue that serve to realize those objectives.

A number of those features relate to the overall design of the interaction between the simulation work shell and have already been discussed in the previous chapter.

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File formats 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{File formats}
The use of the MVC pattern in the design of the work shell makes it straightforward to extend the work shell with viewers or post-processors for new file formats. At present recognizes a number of file formats for input and output of the simulation data and a number of graphical formats for the post-processing of simulation data. 
 
\subsection{Formats for input-output}
The  simulator supports three file formats for input and output of full simulation data (i.e. a dataset that can be used to restart and extend the simulation): 
\begin{description}
	\item[XML] a well-known human readable or text based markup format (see \url{http://en.wikipedia.org/wiki/XML})
	\item[XML.GZ] compressed file of XML format format (see \url{http://en.wikipedia.org/wiki/Gzip}); provides typically 80-90 percent reduction in file size
	\item[HDF5] a machine readable or binary hierarchical data format often used in data-intensive scientific applications (see \url{www.hdfgroup.org/HDF5/})
\end{description}

HDF5 is a widely used file format in scientific visualisation. It is a 
portable file format that comes with a high-performance software library 
that is available across platforms from laptops to supercomputers, 
with API a.o. for C/C++. It is a free, open source software. 
On \url{www.hdfgroup.org/HDF5/doc/index.html} one finds documentation, 
specifications and examples. HDF5 provides a significant enhancement in 
functionality because it enables the use of Paraview (see \url{www.paraview.org}), 
a state-of-the-art scientific visualisation tool. This requires the use 
of a Paraview plug-in to make the VPTissue HDF5 file structure available to Paraview. 
This plug-in has been developed.  The specification of the HDF5 file can be 
found in appendix \ref{chap:SimPT HDF5 file specification}.


\subsection{Formats for post-processing}
VPTissue supports the following formats for numeric or graphical (both vector and bitmap) output:
\begin{description}
	\item[BMP] a graphics format that provides a bitmap graphics representation 
	of the mesh (see \url{http://en.wikipedia.org/wiki/BMP_file_format}).
	\item[CSV] a human readable format for storing data in a 
	table (see \url{http://en.wikipedia.org/wiki/Comma-separated_values}); used to provide numeric output .
	\item[JPEG] a graphics format (see \url{http://en.wikipedia.org/wiki/JPEG});  
	used to provide a bitmap graphic representation of the mesh.
	\item[PLY] a human readable format for storing graphical objects that are 
	described as a collection of polygons (see \url{http://paulbourke.net/dataformats/ply/})
	\item[PDF] a graphics format  (see \url{http://en.wikipedia.org/wiki/Portable_Document_Format});  
	used to provide a vector graphic representation of the mesh.
	\item[PNG] a graphics format (see \url{http://en.wikipedia.org/wiki/Portable_Network_Graphics});  
	used to provide a bitmap graphic representation of the mesh.
\end{description}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Customizability
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Customizability}
A number of choices, represented by parameters in the input data file, can be made to customize aspects of the simulation. 
	\begin{itemize}
%
	\item The set of ODE solvers for the transport equations includes fixed step and adaptive step solvers. The full set of solvers provided by the Odeint package of the Boost library is available ( see \url{http://www.boost.org/doc/libs/1_61_0/libs/numeric/odeint/doc/html/index.html}. The choice of ODE solver is specified in the input file in the section \texttt{parameters.ode\_integration} with the parameter \texttt{ode\_solver}. The tolerances and solver time increments are specified in the same section. 
%
		\item Tina's Random Number Generators Library developed by Heiko 
Bauke (see \url{http://numbercrunch.de/trng/} are available. Tina's Random Number 
Generator Library (TRNG) is a state of the art C++ pseudo-random number 
generator library for sequential and parallel Monte Carlo simulations. Its 
design principles are based on a proposal for an extensible random number 
generator facility, that has become part of the C++11 standard. Contary to the standard C++ RNG's, the TRNG implementation allows for use in the context of parallel calculations. The simulator correctly tracks the state of random generators across restarts to have consistent time evolution, irrespective of the number of restarts. The choice of random generator and seed is specified in section \texttt{parameters.random\_engine} in the input file.
%
	\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dynamic parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dynamic parameters}
\label{sec:DynParam}
The use of event mechanisms in the interaction between the graphical user interface and the simulation core make it possible that all parameters in the simulation input file are dynamic. That is: if a used edits parameters via the parameter edit panel in the GUI, the an event is triggered signalling this to the simulation core. The core in turn will take those changed values into account starting at the next time step and in turn trigger an event that causes the viewers to be aware of these changes and store the changed parameters on file (to be available for a simulation restart or post processing analysis). A parameter change can also be a computed change, i.e., effected by the simulator when certain conditions are met such as number of cells exceeds a threshold or simulated time reaches a pre-set value. 

All simulation parameters can be changed dynamically. This includes the selection of the model or of model components and of the time evolution algorithm. 


\myCppInput[FullSource]{./}{ComponentInterfaces.h}{``Code excerpt (edited for inclusion in this manual) of the component interface definitions.''}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithmic 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Algorithmic components}
The code for biological processes such as cell division or cell-to-cell transport or the time evolution scheme, has a well-defined set of variation points. These are point in the code where different simulation models require different algorithmic steps. 

We use algorithmic components, implemented as function objects (see \cite{Barton1994,Stroustrup2013}) to insert code at these points (see more on this in the section on Models).This approach creates a lot possibilities and flexibility to customize and extend the simulator without having convoluted code full of control statements to distinguish the execution flow for each model. The algorithmic components also have well-defined interfaces, make it straightforward for third parties to write their own components. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Models and model families}
Models are defined by the attributes that are assigned to the cell tissue, to cells, walls, and so on, and by the algorithmic steps taken in the biological processes. The former are data members in the corresponding classes (see section \ref{sec:Attribute} for details. The latter are encapsulated in algorithmic components (see the section above and section \ref{sec:Components}). These components deal with cell splitting, cell to cell transport of chemicals, wall chemistry, the time evolution scheme and so forth. A full list of model components in the current VPTissue version with a short description and the range of choices for each component type is provided in the Parameter Dictionary in \ref{chap:ParameterExploration}. 

VPTissue organizes models into model families. Models within the same family may share components with one another, so as to avoid code duplication. Otherwise models have no component code in common. The model family called ''\texttt{Default}'' plays a special role. It is always built into the VPTissue distribution and if a model definition specifies the use of a component e.g. for the cell\_split that is not present within its family of components, then the simulator will look for a cell\_split component of that name in the default family. This makes sense because for some component type e.g. the cell\_split type or the time\_evolver type, many models will use the fairly generic component that is available in the \texttt{Default} family. Again we want to avoid copy-paste code duplication with this mechanism.
 
Model definition is simply a specification of all the names of the components of each type that are to be used by the simulator in executing the model. The names are listed in xml format in the model section of the VPTissue input file. Figure \ref{fig:ModelDefinition} shows an example.
The section contains the name of the model family and model, the number of chemicals the model deals with, names for the model components of each of the types and the time step to be used. 

\begin{figure}
%
\begin{verbatim}
    <model>
        <group>Default</group>
        <name>Wortel</name>
        <cell_chemical_count>9</cell_chemical_count>
        <cell_chemistry>Wortel</cell_chemistry>
        <cell_daughters>Wortel</cell_daughters>
        <cell_housekeep>Wortel</cell_housekeep>
        <cell_split>Wortel</cell_split>
        <cell2cell_transport>Wortel</cell2cell_transport>
        <mc_hamiltonian>ModifiedGC</mc_hamiltonian>
        <mc_move_generator>directed_uniform</mc_move_generator>
        <time_evolver>VPTissue</time_evolver>
        <time_step>30</time_step>
        <wall_chemistry>NoOp</wall_chemistry>
    </model>
\end{verbatim}
%
\caption{Model definition for the \texttt{Wortel} model of the \texttt{Default} model family.}
\label{fig:ModelDefinition}
\end{figure}		
		
As the names in the \texttt{Wortel} model definition \ref{fig:ModelDefinition} suggests, it uses a fair number of components specifically written for this model, but it reuses the \texttt{ModifiedGC}, \texttt{directed\_uniform} and \texttt{VPTissue} components that it shares with other models. Though there are two components related the hamiltonian used in the MonteCarlo algorithm (mc\_hamiltonian and delta\_hamiltonian), these come in pairs and are defined by a single common name.

When one introduces a new model it is a judgement call as to whether it is best categorized as new model of an existing family or whether it warrants defining a new model family. This depends on the amount of component reuse within the family at present and in the future development of the new model.

All of the model specifications are dynamic parameters in the sense of section \ref{sec:DynParam}. If for instance tissue growth proceeds in distinct phases, then modularity in the model definition allows one to simply specify the change of the relevant component(s) during the simulation.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-defined models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pre-defined models}
Some of the models in the Default model family serve only for demonstration and have been borrowed from VirtualLeaf (\cite{Merks2011,Merks2013}) while others have been used in the context of research. We review the main models briefly:
\begin{description}
\item[Geometric] \ \\
This primitive model involves cell growth and division and simulates a simple model of callus growth, i.e. an isotropic growth with rapid cell division. The simulation starts with a single cell and cells expand at a constant rate. Cells divide in two equal parts according to a division axis perpendicular to the long axis of the cell (axis of inertia). 
\item[TipGrowth] \ \\
This model illustrates the interaction between a diffusive morphogen and tissue growth. One of the cells is a continuous morphogen production source. The morphogen diffuses passively through Fick's law. If its concentration exceeds some threshold, cells expand and divide once their area has doubled. At lower concentrations only expansion is possible, at even lower concentrations growth stops altogether. 
\item[AuxinGrowth] \ \\
This model illustrates an interaction of tissue growth with auxin-driven patterning. Auxin is produced in this model along the perimeter of the tissue. Cell to cell transport is based on the auxin transport model (\cite{Merks2007}) and auxin concentrations drive cell expansion inducing localized growth.
\item[Meinhardt] \ \\
This model is based on the Meinhardt reaction-diffusion model (\cite{Meinhardt1976}) and demonstrates leaf venation patterning in growing tissue. Cells differentiate into vascular tissue in response to activator, which is formed by autocatalysis and lateral inhibition. 
\item[SmithPhyllotaxis] \ \\
This model is derived from the Smith phyllotaxis model (\cite{Smith2006}) but based on a 2D tissue that is similar to the Geometric model above. Details can be found in \cite{Draelants2016}.
\item[Blad] \ \\
This is a family of three leaf models which differ in the number of starting cells (32, 128, and 512 cells, resp.). A diffusive morphogen produced in the static leaf stem is crucial for regulating cell proliferation and cell expansion phases. Output data of these models have been fitted to experimental leaf growth data of \textit{Arabidopsis} (\cite{DeVos2015}).
\item[Wortel] \ \\
This model describes primary root growth of \textit{Arabidopsis}. The interaction between morphogens auxin and cytokinin is central to formation and regulation of a stable growth zone (\cite{DeVos2014}). 
\end{description}
%
\ \\
The next cases are rather model components or combinations thereof. They specify the cell wall mechanics and as such can be used in other models.
\begin{description}
%
\item[PlainGC] \ \\
As it was based on some 
geometric constraints, e.g. the cell area constraint, the edge length constraint, 
etc, it is now called ``PlainGC'' (Plain Geometric Constraint) model. In PlainGC for the constraint expressions the absolute difference of 
parameters (areas, lengths) is used and it causes the roles of larger 
cells to be more dominant than the smaller ones. As a result the smaller 
cells become less and less significant during equilibration and growth cycle.
%
\item[ModifiedGC] \ \\
In the Modified Geometric Constraint (``ModifiedGC'') model the relative 
difference of cell areas is used in the cell area constraint expression, 
which makes for the equal contribution of both large and small cells.
\item[ElasticWall] \ \\
The ``ElasticWall'' model avoids the edge length constraints in Hamiltonian, 
replacing them by elastic wall term making it additive at wall splitting. 
Additionally, in this model each wall has its individual and variable rest 
length given in the XML file ($``rest\_length''$ in wall attributes), 
instead of the common and constant rest length of 
edges ($``target\_node\_distance''$) in ``PlainGC'' and ``ModifiedGC''.
\item[Maxwell] \ \\
The ``Maxwell'' model represents the viscoelasticity of the cells 
and cell walls. In this model the turgor pressure term and the elastic 
wall term are used in Hamiltonian instead of cell area and edge length 
constraints. The turgor pressure in each cell is represented by the 
quantity of solute given in XML file ($``solute''$ in cell attributes). 
Contrary to all three previous models, this model is time-dependent: at 
each time step the quantity of solute in each cell and the rest 
length of the wall are updated.
%
\end{description}

Two new models were added to the wall relaxation/yielding model. 
The first of them is based on the wall length threshold. In this model 
if the wall length exceeds the threshold value during the wall extension, its rest length is updated by the some rate at each time step until this rest length reaches some value. In fact, this model represents the case when an external force is applied to the tissue. The second wall yielding model is based on the pressure threshold. In this model if the pressure in the cell exceeds the threshold, the rest lengths of cell walls are updated by the some 
rate at each time step until these rest lengths reach some values. This is a more advanced model for wall yielding in plant cells during their growth.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interoperability
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Language interoperability}
The core simulator has been isolated into a single package. In addition to 
its internal interface, an adapter has been used to define a clean external 
interface for the simulator with just five methods, see \myListing{SimWrapper.h:FullSource}. 
This has made it possible to build wrappers for the simulator in Java and Python. 
These wrappers, as the name suggests, are native Java or Python classes that can be 
used in regular Java or Python programming. A wrapper object has the same five methods, but now the methods will forward the calls to the similarly named methods of a corresponding C++ object. 
Thus, the VPTissue core simulator can be accessed from within Java or Python programs. This makes it possible to develop coupled simulations with VPTissue where one (or more) instance of a VPTissue simulator is coupled to simulators written in Java or in Python.

\myCppInput[FullSource]{./}{SimWrapper.h}{``External simulator interface.''}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Coupled
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Coupled simulations}

Multiple instances of simulations can be run in parallel via an internal (coupler) interface thanks to the isolation of the core simulator into a single class (cf. Figure \ref{fig:coupledcppsims}). Via the ExchangeCoupler class information exchange is restricted to modification of the boundary conditions of a specified set of (boundary) cells. This proceeds after the coupled models have evolved individually in terms of the respective local chemical processes. In the coupling step the chemical concentration of the boundary cells is exchanged (to serve as the boundary conditions in the following simulation step) and finally the mechanical equilibration is executed for the respective models. To run coupled simulations with VPTissue the interactive (gui) mode has to be used to set up the individual model simulations (projects). A separate project is then initialized which refers to the individual project names and precisely defines the coupling: i.e. which are the boundary cells and how they communicate (coupler type, pairing, transfer kinetics). The parameter \texttt{sim\_ODE\_coupling\_steps} determines how tight the coupling is by specifying the number of coupling steps per simulation time step. A working version of such a data file is included in the source code (see 'TestCoupling' in the resources directory of the Default models) and defines the coupling between models TestCoupling\_I and TestCoupling\_II.

It is also possible to couple models implemented in a different modelling framework. By means of the Simplified Wrapper and Interface Generator tool (SWIG: see www.swig.org) an interface is created which allows for models coded in other languages such as Python or Java to interact with VPTissue. For that purpose a wrapper class (SimWrapper) is integrated into VPTissue which contains methods that can be called from an external program in order to exchange information as well as coordinate a VPTissue simulation.  Figure \ref{fig:coupledcppsims2} shows an exampe of the interaction of a model defined in Python using the PyPTS toolbox (https://pypi.python.org/pypi/PyPTS) with a model defined in VPTissue. This coupled simulation runs via the command line interface driven by a Python scripts. Precise instructions are given in the source code (src/main/swig\_sim/Py\_WrapperModel/README.md).
%Figure \ref{fig:coupledcppsims} shows the flow of execution for %two coupled SimPT simulations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.3]{images/CoupledSims_v3.png}}
	\end{center}
	\caption{Execution flow of coupled VPTissue simulations.}
	\label{fig:coupledcppsims}
\end{figure}

\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.5]{images/simPT_pypts_coupling.pdf}}
	\end{center}
	\caption{Execution flow of coupled VPTissue ('SimPT') and PyPTS simulations.}
	\label{fig:coupledcppsims2}
\end{figure}
