%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################

\chapter{Parameter Exploration}
\label{chap:ParameterExploration}



The VPTissue parameter exploration tool allows you to start and monitor 
a  parameter sweep calculation on a compute server. 
Before starting a parameter exploration, make sure a parameter 
exploration server and a sufficient amount of nodes are running.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLIENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Client}

Before an exploration can be sent or its progress can be shown, 
it is necessary to first connect to a server.
This can be done by clicking ``Connect'', after which you can 
enter a name, ip address and port number. To make this process more 
convenient, it is possible to save a server (by clicking ``Save Server''), 
which means you can reconnect to a previously configured server. When the 
right information has been submitted, a connection will be set up.

The central part of the client is the exploration overview, which shows 
the status of a running exploration to which the user is subscribed. 
Subscribing to an exploration can be done by clicking ``Subscribe'' and 
choosing an exploration you want updates about. Note that when a new 
exploration is created, you are automatically subscribed to it. 
You can only be subscribed to one exploration at a time.

A more detailed overview for all tasks that are part of an exploration 
can be found under ``Task Overview''. Here, you can see how many tasks there are, 
what their status is, the run time for running tasks and the total time taken 
to finish for completed tasks. It is also possible to cancel a task by clicking ``Stop''. 
In case you want to resend a cancelled task, you can click ``Restart'' to 
add this task to the queue. \textbf{Note that cancelled tasks are executed \emph{completely} 
again when they are resent.}

Via ``Start Exploration'' it is possible to send a new exploration to the server. 
The wizard that pops up, gives you the choice to start an exploration based 
on parameters or based on tissue files. You can also retrieve and edit the 
last created exploration. By clicking ``Delete Exploration'', it is possible 
to remove an exploration from the server, which consequently also removes all 
its results. This action can not be undone.


\subsection{Choosing exploration options}
There are several types of parameter explorations that can be started.

\textbf{Sweep based exploration} \ \\
In a sweep based exploration, a single parameter is varied. You can either 
choose to vary a parameter based on a range of values (with a 'from' value, 
a 'to' value and a stepsize) or you can specify the values over which to iterate in a list.

\textbf{Template based exploration} \ \\
When you want to vary several parameters at once, it can be useful to 
create an 'experiment design', to avoid superfluous or redundant calculations. 
When starting a template based exploration (see figure \ref{fig:templateexploration}), you can specify a tissue file (see listing \ref{templateexample} for an example) where 
the parameters that should be varied are marked and a csv file (see table \ref{tab:csvparameters} ) in which the 
combinations of parameters that should be simulated are laid out.



\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.6]{images/ScreenShotExplorationTemplate.png}}
	\end{center}
	\caption{The Parex dialog which can be used to start a template based exploration. See listing \ref{templateexample} and table \ref{tab:csvparameters} for examples on how to design the ``Template'' and ``Params'' file.}
	\label{fig:templateexploration}
\end{figure}


\begin{lstlisting}[caption=An example of how to incorporate the parameters in the tissue XML file. Each parameter name should be enclosed by dollar signs., label=templateexample, float]
...
<parameters>
	<model>
	</model>
	<auxin_transport>
		<aux_breakdown>$param1$</aux_breakdown>
		<k1>$param2$</k1>
		<k2>$param3$</k2>
		<aux_cons>0</aux_cons>
		....
\end{lstlisting}

\begin {table}
 \label{tab:csvparameters} 
\begin{center}
  \begin{tabular}{ | l | l | l | }
    \hline
    param1 & param2 & param3  \\ \hline
    0.0001 & 1 & 0.1 \\ \hline 
    0.0002 & 1 & 0.2 \\ \hline 
    0.0003 & 1 & 0.3 \\ \hline 
	... & ... & ... \\ 
    \hline
  \end{tabular}
\end{center}
\caption {An example of how to design the ``params'' csv file. The header row should contain the names of the parameters that were defined between the dollar signs in the template file. Each row is a separate experiment that will be distributed to the nodes.}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NODE AND SERVER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Node and server}
You can start a server by executing \texttt{./SimPT\_parex -server}.
By providing the extra arguments \texttt{-n} and \texttt{-p}, you can 
configure the server. \texttt{-n} defines the minimum number of nodes 
the server should have (which by default is 0). Every 30 seconds, the 
server will check whether this condition is fulfilled. If not, the 
server will start up a couple of nodes until the condition is satisfied.
The argument \texttt{-p} allows you to choose a port to communicate 
with clients (by default this is 8888).

Nodes can be started by running \texttt{./SimPT\_parex -node}. The node will 
automatically connect to the server. Nodes write the results of a simulation 
to the path ``vleafspace\emph{NAME}\emph{yyyy-MM-dd-HH:mm:ss:zzz}/Simulations\emph{X}/'', 
where \emph{NAME} is the name of the exploration, \emph{yyyy-MM-ddTHH:mm:ss:zzz} 
the date and time, and \emph{X} the id of the task.