%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################

\chapter{Tissue Editor}
\label{chap:Editor}

The VPTissue Tissue editor is a graphical editor for the VPTissue mesh geometry and the cell, 
wall and node attributes. The application constructs, reads and writes a full XML file 
that includes simulation parameters and mesh data. The parameters are stored on 
input, can be edited and written on output. 
The Tissue Editor lets you edit 
the mesh: geometry and attributes of nodes, cells and walls.\ \\
 
\noindent \emph{WARNING: 
the tissue editor cannot process single cell meshes due to a data structure issue forced by 
backward compatibility. The current implementation of the tissue editor does not have any 
saveguards against reading a single cell file or making a mesh single cell by deleting 
all cells but one.}
%THE TISSUE EDITOR CANNOT PROCESS SINGLE CELL MESHES 
%DUE TO A DATA STRUCTURE ISSUE FORCED BY BACKWARD COMPATIBILITY. THE CURRENT 
%IMPLEMENTATION OF THE EDITOR DOES NOT HAVE ANY SAVEGUARDS AGAINST READING
%A SINGLE CELL FILE OR MAKING A MESH SINGLE CELL BY DELETING ALL CELLS BUT ONE.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview}

 When the editor starts up you will notice a menu bar with 
 three pull down menus: \emph{Project}, \emph{Edit} en \emph{View}.
 
The \emph{Project} menu provides following actions:
\begin{compactitem}
	\item \emph{\textbf{New}} lets you initialize a new configuration. 
	It creates a new tissue borrowing the tissue preamble, the parameters 
	and (in the current implementation) the mesh.cells.chemical\_count from 
	a template file that you specify in a file dialog. The 
	mesh (taking chemical\_count into account) is generated (in the current 
	implementation it is a mesh of two square cells with all nodes, cells and walls 
	having default values for the attributes) and substituted into the tissue. 
	That tissue is then available for editing.
	\item \emph{\textbf{Open}} lets you read sim data (preamble,  parameters and mesh) 
	of an existing XML sim dat file. The simulation parameters are stored and included 
	in the output when the edited mesh is written to file. 
	\item \emph{\textbf{Save}} lets you write the current tissue to an XML sim data file.	
	\item \emph{\textbf{Close}} lets you close the mesh you are currently editing. 
	If you have not saved the sim data data yet, the application will ask you whether it needs to do so.
\end{compactitem}
Depending on your operating system, you will have a \emph{\textbf{Quit}} option 
in the \emph{Project} menu or in the menu with the application name.

The actions accessible through the \emph{Edit} and \emph{View} menus will be addressed in the following sections.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Modes of the editor}

\subsection{Selection Modes}

The mesh display area allows you to edit the mesh graphically. 
An important feature are the graphical selection modes:  \emph{Cell}, 
\emph{Edge} and \emph{Node}. Note that the mode relates to the type of 
item you can select to define an operation, rather than the type of item you 
will operate on. For example, to split a cell you need to select two nodes 
that define the division axis. Thus the ``split cell'' action is available 
in the \emph{Node} selection mode.
 
One activates a particular selection mode by clicking the appropriate icon in the 
top left corner of the mesh display area or using the \emph{Mode} option 
of the \emph{Edit} pull down menu. The icon of the current mode is 
highlighted in the top left corner. The effect of a mode is that it 
enables the subset of operations that pertain to that particular type of entity.
For more information on the actions available in each mode, see section \ref{sec:GraphicalEditing}.

\subsection{Display Mode}

When you initialize or open a data file, you start in the \emph{Display} mode. 
If you deactivate the current selection mode by clicking its icon in the top 
left corner or deselect it in the \emph{Mode} option of the \emph{Edit} pull 
down menu, you will also revert to \emph{Display} mode. In this mode the mesh 
is displayed using one of the pre-defined color schemes. A different color 
scheme (the default is size dependent coloring) can be selected with 
the \emph{Set color scheme ...} option of the \emph{View} pull down menu. These are the same color scheme options as available in the simulator.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Selecting items}

\begin{compactitem}
	\item You can select an item (node, cell, wall - depending on 
	the current selection mode) by clicking it.
	\item You can also perform a \emph{bulk selection} by holding 
	down the Shift key and selecting multiple items. 
	%\item You can also use the \emph{Find and select} option of 
	%the \emph{Edit} pull down menu. This brings up a dialog 
 		%lets you specify the id of the item to be selected. You 
 		%can keep this dialog open and specify multiple id's one 
 		%after the other, each time hitting the \emph{Select} button, 
 		%to select multiple items.TODO:removed since not possible?
 	\item Finally, you can use the search box present in the toolbar. 
 	Specifying a combination (separated by a comma) of ranges of the 
 	format `from-to:step' (with `to' and `step' optional) allows the 
 	selection of multiple items based on their ids.
	\item It is also possible to combine the above selection methods.
\end{compactitem}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Panels}

After you have initialized a new tissue or opened an existing one, you 
will notice that the application window has an area displaying the mesh 
and three panels named \emph{Parameters panel}, \emph{Attribute panel} and \emph{Geometric panel}.
You can zoom in or out in the mesh display area, for instance by scrolling.
\begin{compactitem}
	\item The first panel allows editing the model and  simulation parameters. 	
	\item When no item (node, cell, wall) has been selected, the latter two panels display the mesh data.
	\item When an item has been selected, the \emph{Attribute panel} 
	lets you view and edit the attributes of that item, while 
	the \emph{Geometric panel} lets you view its geometric data. 
	The x, y coordinates of a node can also be edited in the panel; other geometric data cannot. 
	\item  When multiple items have been selected, the attribute 
	panel will show all the attributes with the same value for all 
	selected items. Attributes with a different value across the 
	selected items have a question mark in the value area in the panel. 
	\item  If you edit any of the attributes in the panel, the new value 
	will apply to all selected items. 
 \end{compactitem}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Graphical editing}
\label{sec:GraphicalEditing}

The first two actions (repositioning nodes and slicing the mesh) are entirely graphical. 
The other actions are available by using an option in the \emph{Edit} pull down menu.
 
\textbf{Reposition node [Node]} \ \\
A node can be repositioned by selecting it and dragging it to its new position or by 
editing its coordinates in the geometric panel. The new position is only accepted if 
it is allowed, i.e., if it does not cause two cells to overlap.
  
\textbf{Slicing mesh [Cell]} \ \\
One can slice the mesh, i.e., draw a straight line and eliminate all cells 
completely on one side and cut the cells intersected by the line. When 
in \emph{Cell} selection mode, right click outside the mesh, draw the line by 
moving the mouse to another point outside the mesh and right click again. 
Select the part of the mesh you wish to keep. The part on the other side 
of the line will disappear.
 
\textbf{Split edge [Edge]} \ \\
This action, available in \emph{Edge} selection mode, splits an edge in two halves. 
This is the method you use to insert additional nodes or edges in a wall: you split 
an edge of the wall and then reposition the node to where you need it to be.
 
\textbf{Split cell [Node]} \ \\
This action, available in \emph{Node} selection mode, splits a cell. Select two 
nodes that belong to the cell and that are positioned such that a straight line 
through the nodes divides the cell in two parts. Choosing the \emph{Split cell} 
option in the \emph{Edit} menu executes the split. It is of course perfectly all 
right to first insert nodes with the \emph{Split edge} action for the specific 
purpose of defining the axis of division.
 
\textbf{Create cell [Node]} \ \\
This action, available in \emph{Node} selection mode, creates a new cell 
at the boundary of the mesh. Select two nodes at the boundary of the mesh, 
choose the \emph{Create cell} option of the \emph{Edit} menu and position 
the third node of the new cell by clicking outside the mesh. If you position 
the third node where it would lead to an illegal construction of the new cell, 
that position is disregarded. The application forces you to retry until you position 
that third node at an appropriate position or cancel the action. Of course, you can 
afterwards format the shape of the new cell by adding a new 
node (the "Split Edge" action) and repositioning them.
  
\textbf{Delete item [Node / Cell]} \ \\
This action removes the selected node or cell.
\begin{compactitem}
	\item When deleting a node, this node has to have a degree of two 
	and can only belong to cells that have at least three other nodes.
	\item When deleting a cell, this cell has to be located at the 
	boundary of the cell complex and cannot violate the consistency of 
	the mesh upon deletion (e.g., the mesh cannot be split into two separate parts).
\end{compactitem}
 
\textbf{Copy attributes [Node / Edge / Cell]} \ \\
This action allows you to copy attributes from a node, edge or cell to another one. 
First, all target items must be selected. Next, the \emph{Copy attributes} action 
can be executed, after which the source item must be selected (this can also be 
an item among the targets). Finally, a dialog will appear where you can specify 
which attributes should be copied from the source to the targets.

%\textbf{Generate pattern [Cell]} \ \\
%This action can split a selected cell based on a pattern. Two %types of patterns 
%can be selected and configured: a regular pattern or a Voronoi %tessellation. 
%Selecting the former one brings up a dialog with possible %regular patterns, 
%which you can configure by rotating, translating or scaling %them. 
%The patterns available within this category are triangles, %rectangles, 
%rhombi and hexagons. The Voronoi tessellation allows you to %specify points 
%within the selected cell and shows you the resulting pattern of %the tessellation. 
%You can place those points by double clicking at the desired %location within the 
%cell, while deleting them can be done by right clicking on an %existing point.
 
\textbf{Undo [Node / Edge / Cell]} \ \\
This option, available in all three selection modes, allows you to undo previous actions.
 
\textbf{Redo [Node / Edge / Cell]} \ \\
This option, available in all three selection modes, allows you to redo actions you have undone.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Graphical Settings}

In the \emph{View} menu, you will find four graphical settings. 
The first three actions toggle the visibility of the \emph{Parameters panel}, the \emph{Attribute panel} 
and \emph{Geometric panel}, allowing a better view on the cell complex. 
It also will increase performance when handling a large number of nodes, 
edges or cells, as these panels will not be updated when invisible. 
Next, \emph{Transparent cells} allows you to make the cells transparent. 
The functionality to create a background for the cell complex is also available which allows to draw cell meshes with microscopic images of plant tissues as a template.
You can specify an image-file and rotate, scale or translate it as you wish. 
Of course, this background can be hidden whenever desired. Finally, you can 
also alter the color model of the \emph{Display} mode.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Toolbar}
A toolbar has been added to allow fast transitions between modes (first 
three pictograms, respectively modes \emph{Node}, \emph{Edge} and \emph{Cell}) 
and to quickly cancel an action (fourth pictogram), e.g., splitting a cell. 
Next to that, the toolbar contains a search option to select items based 
on their identifiers, by entering a combination of ranges of the 
form `from-to:step'. Toggling the lock button right of this search box 
prevents discarding previously selected items.