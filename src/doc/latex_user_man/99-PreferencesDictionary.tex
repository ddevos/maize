%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################


\chapter{Preferences Dictionary}
\label{chap:PreferencesDictionary}

\section{Introduction}
In this section we give an overview of all preferences that apply to VPTissue
 as a command line or graphical application. At the present level of implementation, 
 all preferences deal  with viewers: graphical viewers, output viewers and log viewers. \\
  
Preference definitions come in two distinct flavors, each in a separate file 
namely \texttt{.simPT-cli-preferences.xml} and \texttt{.simPT-gui-preferences.xml}. 
The first, with keyword \texttt{cli} (i.e. Command-Line Interface) in the name, 
applies to the command line application,  the second, with the keyword \texttt{gui} 
(i.e. Graphical User Interface) in the name, applies to the graphical application. \\
  
The workspace preferences act as defaults for those project preferences that do not have 
an actual value, but instead have a \texttt{\$WORKSPACE\$} label as a value. 
An actual value for a project preference overrides the workspace default for that preference. \\
  
The installed application defines a template for the \texttt{cli} 
and \texttt{gui} preferences that are used when creating a new workspace or when 
opening a workspace where the preferences are missing. When creating a new project 
or opening a project where the preferences are missing, the workspace preferences 
are copied to define the initial project preferences. \\

\section{Preferences for graphics: colors\_sizes}
 Options for the color and size of text of mesh objects.  \\
\begin{compactdesc}
%
 	\item[arrow\_color] \ \\
 		\emph{choices:} any color name acceptable to Qt4 
%
 	\item[arrow\_size] \ \\
 		\emph{choices:} unsigned integer 
%
 	\item[background\_color] \ \\
 		\emph{default:} white  \\
		\emph{choices:} any color name acceptable to Qt4
%
 	\item[cell\_color] \ \\
 	Cell corloring scheme to be used when generating images of the tissue. \\
 		\emph{choices:} AuxinPIN1, ChemBlue, ChemGreen, Meinhardt, Size, Wortel
%
 	\item[cell\_number\_size] \ \\
 		\emph{choices:} unsigned integer 
%
 	\item[cell\_outline\_color] \ \\
 		\emph{choices:} any color name acceptable to Qt4
%
 	\item[node\_magnification] \ \\
 		\emph{choices:} unsigned integer 
%
 	\item[node\_number\_size] \ \\
 		\emph{choices:} unsigned integer 
%
 	\item[outline\_width] \ \\
 		\emph{choices:} unsigned integer
%
 	\item[resize\_stride] \ \\
 		\emph{choices:} unsigned integer 
%
 	\item[text\_color] \ \\
 		\emph{choices:} any color name acceptable to Qt4 
%
 \end{compactdesc}



\section{Preferences for graphics: visualization}
 Options for the view of mesh objects. \\
\begin{compactdesc} 
%
 	\item[border\_cells] \ \\
 		\emph{choices:} false, true 
%
 	\item[cells] \ \\
 		\emph{choices:} false, true 
%
 	\item[cell\_axes] \ \\
 		\emph{choices:} false, true 
%
 	\item[cell\_centers] \ \\
 		\emph{choices:} false, true 
%
 	\item[cell\_numbers] \ \\
 		\emph{choices:} false, true 
%
 	\item[cell\_strain] \ \\
 		\emph{choices:} false, true (currently no model implementation)
%
 	\item[fluxes] \ \\
 		\emph{choices:} false, true 
%
 	\item[nodes] \ \\
 		\emph{choices:} false, true 
%
 	\item[node\_numbers] \ \\
 		\emph{choices:} false, true 
%
 	\item[only\_leaf\_boundary] \ \\
 		\emph{choices:} false, true 
%
 	\item[tooltips] \ \\
	  View of cell data (number, type, chemicals, area, etc.)  by mouse pointer. \\
 		\emph{choices:} false, true 
%
 	\item[walls] \ \\
 		\emph{choices:} false, true 
%
 \end{compactdesc}




\section{Preferences for file format: bitmap\_graphics}
Options for output of bitmap-files. These options are used for BMP, JPEG 
and PNG export or post-processing.
\begin{compactdesc}
%
 	\item[size] \ \\
	  Size of generated bitmaps.
	  This node is optional. \\
	  \textbf{x} \\
 		Width of generated bitmaps. \\
		\emph{default:} 800 \\
		\emph{choices:} unsigned integer \\
	  \textbf{y} \\
 		Height of generated bitmaps. \\
		\emph{default:} 600 \\
		\emph{choices:} unsigned integer
%
 	\item[source\_window] \ \\
 		Rectangular part of mesh to export. Coordinates in mesh-domain. \\
		This node is optional. If it isn't specified, the source window will be the bounding box of the mesh. \\
	  \textbf{min\_x} \\
		X-coordinate of top-left corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number \\
	  \textbf{min\_y} \\
		Y-coordinate of top-left corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number \\
	  \textbf{max\_x} \\
		X-coordinate of bottom-right corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number \\
	  \textbf{max\_y} \\
		Y-coordinate of bottom-right corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number
%
 \end{compactdesc}
 
 
\section{Preferences for viewer: hdf5}
Option for output of hdf5-files.
\begin{compactdesc}
%
 	\item[enabled\_at\_startup] \ \\
 		The viewer is disabled (inactive) or enabled (active). \\
 		\emph{default:} - \\
 		\emph{choices:} false, true 
%
 	\item[stride] \ \\
 		The viewer skips time steps and takes action at steps corresponding to multiples of the stride. \\
 		\emph{default:} -  \\
		\emph{choices:} unsigned integer
%
	\item[file] \ \\
		The name of the hdf5 output file containing the history of the simulation for that project. Note
  		that each project lives in its own subdirectory, so it is perfectly fine to have the same filename,
  		e.g., \texttt{leaf.h5} for all projects. \\
		\emph{default:} - \\
		\emph{choices:} any allowed filename with .h5 extension
%
\end{compactdesc}		
 
 
\section{Preferences for viewer: log}
\begin{compactdesc}
%
 	\item[enabled\_at\_startup] \ \\
 		The viewer is disabled (inactive) or enabled (active). \\
 		\emph{default:} - \\
 		\emph{choices:} false, true 
%
\end{compactdesc}		
 
 
\section{Preferences for viewer: logwindow}
\begin{compactdesc}
%
 	\item[enabled\_at\_startup] \ \\
 		The viewer is disabled (inactive) or enabled (active). \\
 		\emph{default:} - \\
 		\emph{choices:} false, true 
%
 	\item[position] \ \\
	  \textbf{x} \\
 		Position of logwindow in x-axis. \\
		\emph{choices:} unsigned integer \\
	  \textbf{y} \\
 		Position of logwindow in y-axis. \\
		\emph{choices:} unsigned integer
%
 	\item[size] \ \\
	  \textbf{x} \\
 		Size of logwindow in x-axis. \\
		\emph{choices:} unsigned integer \\
	  \textbf{y} \\
 		Size of logwindow in y-axis. \\
		\emph{choices:} unsigned integer
%
\end{compactdesc}
  
 
 
\section{Preferences for viewer: qt}
\begin{compactdesc}
%
 	\item[enabled\_at\_startup] \ \\
 		The viewer is disabled (inactive) or enabled (active). \\
 		\emph{default:} - \\
 		\emph{choices:} false, true 
%
 	\item[stride] \ \\
 		The viewer skips time steps and takes action at steps corresponding to multiples of the stride. \\
 		\emph{default:} -  \\
		\emph{choices:} unsigned integer
%
 	\item[position] \ \\
	  \textbf{x} \\
 		Position of logwindow in x-axis. \\
		\emph{choices:} unsigned integer \\
	  \textbf{y} \\
 		Position of logwindow in y-axis. \\
		\emph{choices:} unsigned integer
%
 	\item[size] \ \\
	  \textbf{x} \\
 		Size of logwindow in x-axis. \\
		\emph{choices:} unsigned integer \\
	  \textbf{y} \\
 		Size of logwindow in y-axis. \\
		\emph{choices:} unsigned integer
%
\end{compactdesc}
 

\section{Preferences for file format: vector\_graphics}
Options for output of vector graphics-files. These options are used for PDF export or post-processing.
\begin{compactdesc}
%
 	\item[source\_window] \ \\
 		Rectangular part of mesh to export. Coordinates in mesh-domain. \\
		This node is optional. If it isn't specified, the source window will be the bounding box of the mesh. \\
	  \textbf{min\_x} \\
		X-coordinate of top-left corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number \\
	  \textbf{min\_y} \\
		Y-coordinate of top-left corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number \\
	  \textbf{max\_x} \\
		X-coordinate of bottom-right corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number \\
	  \textbf{max\_y} \\
		Y-coordinate of bottom-right corner of source window. \\
 		\emph{default:} -  \\
		\emph{choices:} any real number

%
 \end{compactdesc}
 
 
\section{Preferences for viewer: xml}
Option for output of xml-files.
\begin{compactdesc}
%
 	\item[enabled\_at\_startup] \ \\
 		The viewer is disabled (inactive) or enabled (active). \\
 		\emph{default:} - \\
 		\emph{choices:} false, true 
%
 	\item[stride] \ \\
 		The viewer skips time steps and takes action at steps corresponding to multiples of the stride. \\
 		\emph{default:} -  \\
		\emph{choices:} unsigned integer
%
 	\item[gzip] \ \\
 		The viewer is disabled (inactive) or enabled (active) for packing the xml-files. \\
 		\emph{default:} -  \\
		\emph{choices:} false, true
%
\end{compactdesc}

