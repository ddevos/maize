%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################


\chapter{The VPTissue Software}
\label{chap:Software}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centering
\begin{verbatim}
------------------------------------------------------------
Language           files       blank     comment        code
------------------------------------------------------------
XML                   48           4          25      160507
C++                  339        7593        9330       33702
C/C++ Header         419        6179       15141       15609
CMake                 56         535        1906        2786
Python                 6         117         184         430
Java                   4          57          60         245
Bourne Shell           4          39         154         134
make                   2          41          27          72
XSLT                   1           8           0          36
HTML                   2           0           2          26
DOS Batch              1           0           0           2
------------------------------------------------------------
SUM:                 883       14578       26829      213562
------------------------------------------------------------
\end{verbatim}
\caption{Line count of source text, documentation, build files, etc. in the
{\tt src} directory (excluding external software). Situation of late March 2016. }
\label{fig:src_cloc}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Code base}
At present (spring of 2016) the software stands at approximately 49K lines of C++ application 
code plus 25K lines of comment (see figure \ref{fig:src_cloc}). About 3.K lines of C++ code
are test code in the {\tt src$/$test} directory. The line 
counts were gathered with the \texttt{cloc} tool (see \url{http://cloc.sourceforge.net/}).

In addition to the source code there are a significant number of other artefacts 
that are involved in building, testing and executing VPTissue. Foremost among them 
are a number of simulation input files (sometimes referred to as ``tissue'' files in jargon). 
These are used for test runs and for populating a workspace template that provides the default 
project to the users. Other artefacts are configuration files and input files 
involved in the generation of documentation.

Concerning language conformance, we have taken a forward perspective, aiming 
to make the code base last for as long as possible. We have used C++11 language 
constructs (in particular lambdas, range based for loop, auto 
keyword (see \cite{Stroustrup2013, Lippman2013}) wherever possible and have 
continuously refactored the code to use them. 

We have taken great care in designing classes, using familiar design 
patterns (\cite{Gamma1995}) and using the cppcheck tool (see \url{cppcheck.sourceforge.net}) 
to analyse the code and flag design deficiencies. We have also maximized code reuse 
by using libraries. A major example is the use 
of Boost's (see section \ref{sec:build}) \texttt{ptree} container. This container 
is tailor made to hold configuration data with a hierarchical structure and provides 
easy access and input-output to a number of file formats, among others xml.

At the level of program design we have taken great pains to represent domain 
concepts in well-defined classes. Not only biological concepts (e.g., mesh, cell, 
wall, edge, node, \ldots) but also algorithmic entities such as CellDivider, 
NodeInserter or the various time evolution scheme have been moulded into 
classes. The use of such algorithmic objects represents current practice in 
computational programming (\cite{Barton1994}).

\begin{float-verbatim}
%\begin{figure}[ht]
{\scriptsize
\begin{verbatim}


main
    |-cpp_execs              <--- Main programs to build executables
    |---modes                <--- Modes (GUI, command line, ...) for executables
    |-cpp_parex              <--- Parameter exploration tool
    |---parex_client         <--- Client side of the parex tool
    |---parex_node           <--- Compute node for the parex tool
    |---parex_protocol       <--- Client-server protocol used with parex
    |---parex_server         <--- Server for parex
    |-cpp_sim                <--- Core simulator: biological concepts, algorithms, time evolution
    |---algo                 <--- Algorithms for expansion, division, node insertion, ...
    |---bio                  <--- Biological concepts: mesh, cell, wall, edge, node, ...
    |---coupler              <--- Coupled simulations code
    |---fileformats          <--- File formats for use by simulator
    |---math                 <--- Miscellaneous mathematical constructs
    |---model                <--- Interfaces to model dependent extension points of the algorithms
    |---sim                  <--- Simulator proper i.e. the simulation driver
    |---util                 <--- Miscellaneous utilities   
    |-cpp_simptshell         <--- Components that build the user interface
    |---cli                  <--- Command line interface components
    |---converter            <--- Converter between file formats
    |---exporters            <--- Exporters to formatted files
    |---gui                  <--- Graphical user interface
    |---mesh_drawer          <--- Drawing of tissue image
    |---session              <--- SimPT specific features of session creation
    |---viewer               <--- Root viewer construct
    |---viewers              <--- The viewers that are available
    |---workspace            <--- VPTissue specific features of workspace construction
    |-cpp_simshell           <--- Components that build the simulator user interface
    |---common               <--- Common code for user interface
    |---gui                  <--- Graphical user interface for the simulator
    |---ptree                <--- Utilities for dealing with ptrees
    |---session              <--- Simulator session management
    |---viewer               <--- Viewers for simulator
    |---workspace            <--- Simulation workspace management
    |-cpp_tissue_edit        <--- Gui tissue editor tool
    |---generator            <--- Generator for meshes (regular, Voronoi)
    |---editor               <--- Core editor components
    |---slicer               <--- Component with slicing capability
    |-models                 <--- Components and resources for each of the model groups
    |---Blad                 <--- Components and resources for each of the model groups  
    |---Default              <--- Components and resources the Default model group  
    |--- ........            <--- Components and resources for model groups you are developing
    |-resources              <--- Resources i.e. non source code artefacts used in build
    |---cmake                <--- CMake modules used for building
    |---data                 <--- Third party icons
    |---icons                <--- Icons for desktop use
    |---lib                  <--- External software included at source level in build
    |---make                 <--- Makefile template
    |---paraview             <--- Integration of HDF5 data files with Paraview
    |---txt                  <--- Some text files
    |-swig_sim               <--- Java and Python wrapper sources
\end{verbatim}
}
\caption{The layout of the VPTissue main 
directory (some directories have been elided to their 
first level subdirectories). Situation of late March 2016.}
\label{list:main_dir}
%\end{figure}
\end{float-verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Directory layout}
The project directory structure is very systematic and is represented in list \ref{list:main_dir}.

Everything used to generate project artefacts is placed in directory \texttt{src}:
\begin{compactitem}
    \item code related files (sources, third party libraries and headers, ...)
      in directory \texttt{src/main}
      	\begin{compactitem}
        		\item for each language the sources in \texttt{src/main/"language"\ldots}
        		\item third party resources in \texttt{src/main/resources}.
        \end{compactitem}
    \item documentation files (api, manual, html, pdf and text ...)
      in directory \texttt{src/doc}
      	\begin{compactitem}
        		\item for each document processing tool a sub directory \texttt{src/doc/"tool"\ldots}
        \end{compactitem}
    \item test related files (description, scripts, regression files, ...)
      in directory \texttt{src/test}
\end{compactitem}
Every artefact is generated in directory \texttt{target} or its sub directories 
during the build procedure (see section \ref{sec:build}). This directory is 
completely removed when the project is cleaned.


The directory structure is reflective of the large-scale package structure of the VPTissue software, outlined in \ref{fig:framework}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Building and testing in Continuous Integration}
\label{sec:build}
The build system for VPTissue has been engineered with the CMake tool 
that describes the build steps and build and install artefacts at a 
high level of abstraction. This leads to build files that are to a high degree 
platform independent (see \url{http://www.cmake.org/}). For those 
users that do not have a working knowledge of CMake, a front end 
Makefile has been provided that invokes the appropriate CMake commands.
VPTissue builds on Linux/UNIX platforms, on Mac OSX platforms 
and Windows/MinGW platforms in a number of configuration (see the list in the next section).
 
From the outset automated tests, including unit tests and scenario tests, have been part of the VPTissue software. We use both ctest, companion to cmake (see
\url{https://cmake.org/}), and Google's googletest (see \url{https://github.com/google/googletest}).
 
The tests are part of the Continuous Integration develop-build-test cycle (\cite{Aiello2011}, \cite{Humble2011}).
It is executed with the Jenkins CI server (see \url{jenkins-ci.org} ). Figure (\ref{fig:jenkins}) 
shows the status of various CI jobs managed by the jenkins server. Only when all build activities (including generation of documentation artefacts) and tests have been successful, does the new revision of the software get pushed to the project's public repository.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Platforms}
\label{sec:platforms}

The reference platforms on which the software has been 
developed and tested through continuous integration are:
\begin{compactitem}
	\item Linux Ubuntu 
	\item Mac OSX 
	\item Windows 7 with MinGW 4.8 (4.8.1-posix-sjlj-rev5.7 available at 
	\url{http://sourceforge.net/projects/mingwbuilds/files/host-windows/releases/4.8.1/32-bit/threads-posix/sjlj/}) or MinGW 4.9 embedded in the qt5 distribution.
\end{compactitem}
The compilers and libraries are:
\begin{compactdesc}
	\item[required] GCC compiler C++ 4.8 or higher or Clang C++ compiler 3.5 or higher.
	\item[required] Boost library release 1.53 or higher; only header components are required, libraries are optional
	\item[required] Qt4  or Qt5 library, specifically Core, GUI and Network components
	 \item[optional] HDF5 1.8 library or higher to have the HDF5 functionality
	 \item[optional] SWIG 2.0 interface compiler or higher to generate the Java and Python wrappers for the core simulator 
\end{compactdesc}
These are all highly regarded and active C++ library projects. These libraries provide 
support in a number of domains (input-output, data structures, math, templates, \ldots).
All other third party software has been included in source form and is injected in the 
build process at source level. 

\begin{figure}
\centering
{\footnotesize
\begin{verbatim}
MAC OSX 10.9   CMake 3.3   GCC 5.2         Boost 1.59   Qt 5.4   HDF5 1.8   swig 3.0                                %speedy
MAC OSX 10.9   CMake 3.3   Clang 3.9       Boost 1.59   Qt 5.4   HDF5 1.8   swig 3.0                               %speedy
MAC OSX 10.11  CMake 3.4   GCC 5.3         Boost 1.59   Qt 5.5   HDF5 1.8   swig 3.0                              %hermes
MAC OSX 10.11  CMake 3.4   Clang 3.9       Boost 1.59   Qt 5.5   HDF5 1.8   swig 3.0                             %hermes
MAC OSX 10.11  CMake 3.5   GCC 6.1         Boost 1.59   Qt 5.5   HDF5 1.10  swig 3.0                             %cyclops
MAC OSX 10.11  CMake 3.5   Clang 3.9       Boost 1.59   Qt 5.5   HDF5 1.10  swig 3.0                             %cyclops
MAC OSX 10.11  CMake 3.5   AppleClang 7.3  Boost 1.59   Qt 5.5   HDF5 1.10  swig 3.0                             %cyclops
Ubuntu 12.04   CMake 2.8   GCC 4.8         Boost 1.54   Qt 4.8   HDF5 1.8   swig 2.0                                %jperf-ubuntu
Ubuntu 12.04   CMake 2.8   GCC 5.1         Boost 1.53   Qt 4.8   HDF5 1.8   swig 2.0                                %radiko
Ubuntu 14.04   CMake 2.8   GCC 5.1         Boost 1.55   Qt 5.2   HDF5 1.8   swig 2.0                                %octo
Ubuntu 14.04   CMake 2.8   Clang 3.6       Boost 1.55   Qt 5.2   HDF5 1.8   swig 2.0                               %octo
Ubuntu 14.04   CMake 2.8   GCC 5.1         Boost 1.54   Qt 5.2   HDF5 1.8   swig 2.0                                %studento
Ubuntu 16.04   CMake 3.5   GCC 5.3         Boost 1.58   Qt 5.5   HDF5 1.8   swig 3.0                                %VM
Win7/MinGW 4.8 CMake 2.8   GCC 4.8         Boost 1.53   Qt 4.8   HDF5 1.8   swig 2.0                              %jslave-win7
Win7/MinGW 4.9 CMake 2.8   GCC 4.9         Boost 1.53   Qt 5.6   HDF5 1.8   swig 2.0                             %jslave-win7-qt5
\end{verbatim}
}
\caption{Full list of build-and-test platforms. Situation of late June 2016.}
\label{fig:platforms}
\end{figure}

The build-and-test process takes from a few minutes to up to an hour depending on the hardware platform. It benefits significantly from the parallel Make features if executed on a multi-core platform.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
	\begin{center}
		\frame{\includegraphics[scale=0.40]{images/simPT_framework.png}}
	\end{center}
	\caption{VPTissue framework.}
	\label{fig:framework}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
		\centering
		\frame{\includegraphics[scale=0.45]{images/ScreenShotJenkins.png}}

		\caption{Screen shot of the overview page of the Jenkins server showing (in this instance)  
		perfect status for all jenkins jobs.}
		\label{fig:jenkins}
\end{figure}

