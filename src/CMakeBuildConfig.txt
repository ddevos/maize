#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#  Macros related to build & install procedures
#
#############################################################################

#============================================================================
# Tweaking advanced / non-advanced:
#============================================================================
mark_as_advanced( CMAKE_OSX_ARCHITECTURES CMAKE_OSX_DEPLOYMENT_TARGET CMAKE_OSX_SYSROOT )
mark_as_advanced( QT_QMAKE_EXECUTABLE )
mark_as_advanced( Boost_DIR  HDF5_DIR )
mark_as_advanced( SIMPT_FORCE_NO_HDF5 )
mark_as_advanced( SIMPT_FORCE_NO_BOOST_GZIP )
mark_as_advanced( SIMPT_FORCE_NO_ZLIB )
mark_as_advanced( CLEAR CMAKE_C_COMPILER   )
mark_as_advanced( CLEAR CMAKE_CXX_COMPILER   )

#============================================================================
# User defined options:
#============================================================================
if ( (NOT SIMPT_PARALLEL_MAKE) OR (SIMPT_PARALLEL_MAKE STREQUAL "") )
    set( SIMPT_PARALLEL_MAKE "1")
endif()

#============================================================================
# User defined options:
#============================================================================
option( SIMPT_MAKE_DOC  
	"Include doc in build and install."  OFF 
)
option( SIMPT_MAKE_JAVA_WRAPPER  
	"Include java wrapper in build and install."  OFF 
)
option( SIMPT_MAKE_PYTHON_WRAPPER  
	"Include python wrapper in build and install"  OFF 
)
option( SIMPT_VERBOSE_TESTING  
	"Run tests in verbose mode."  OFF 
)
option( SIMPT_FORCE_NO_HDF5  
	"Force CMake to act as if HDF5 had not been found."  OFF 
)
option( SIMPT_FORCE_NO_BOOST_GZIP  
	"Force CMake to act as if Boost iostreams+compression had not been found."  OFF 
)
option( SIMPT_FORCE_NO_ZLIB  
	"Force CMake to act as if ZLIB had not been found."  OFF 
)

#============================================================================
# Handle instances of binary incompatibility with system zlib:
#============================================================================
if ((CMAKE_HOST_UNIX AND CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
OR (CMAKE_HOST_APPLE AND CMAKE_CXX_COMPILER_ID STREQUAL "GNU"))
	set( SIMPT_FORCE_NO_BOOST_GZIP   ON )
    set( SIMPT_FORCE_NO_ZLIB         ON )
endif()

#============================================================================
# INSTALL LOCATION for bin, doc etc.
#============================================================================
set( BIN_INSTALL_LOCATION	bin    )
set( LIB_INSTALL_LOCATION	bin    )
set( DATA_INSTALL_LOCATION	data   )
set( DOC_INSTALL_LOCATION	doc    )
set( TESTS_INSTALL_LOCATION	tests  )

#############################################################################
