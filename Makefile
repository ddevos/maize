#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#   Meta makefile calls cmake to do the heavy lifting. It first
#   includes the file MakeLocalConfig (if it exists) for local
#   configuration. Copy src/main/resources/make/MakeLocalConfig
#   to this directory and customize it as you see fit.
#   This Makefile is tracked by the mercurial repository so do not
#   change this for personal customization. That should be done in
#   the file MakeLocalConfig.
#
#############################################################################

#============================================================================
#   Load MakeLocalConfig to set macro values
#============================================================================
MakeLocalConfig = $(wildcard MakeLocalConfig)
ifeq ($(MakeLocalConfig),MakeLocalConfig)
	include MakeLocalConfig
endif

#============================================================================
#   CMake command
#============================================================================
ifeq ($(CMAKE),)
	CMAKE = cmake
endif
ifeq ($(CTEST),)
	CTEST = ctest
endif

#============================================================================
#   Platform info: Windows, Mac, Linux/UNIX
#============================================================================
ifdef SystemRoot
	# Compiling for MinGW on Windows
	ifeq ($(CMAKE_GENERATOR),)
		CMAKE_GENERATOR = "MinGW Makefiles"
	endif
else
	# Compiling for Mac OSX or *nix
	ifeq ($(CMAKE_GENERATOR),)
		CMAKE_GENERATOR = "Unix Makefiles"
	endif
endif

#============================================================================
#   MACRO definitions to pass on to cmake
#============================================================================
ifeq ($(SIMPT_PARALLEL_MAKE),)
        SIMPT_PARALLEL_MAKE=1
endif

CMAKE_ARGS += -DSIMPT_PARALLEL_MAKE=$(SIMPT_PARALLEL_MAKE)

ifneq ($(CMAKE_GENERATOR),)
	CMAKE_ARGS += -DCMAKE_GENERATOR=$(CMAKE_GENERATOR)
endif
ifneq ($(CMAKE_BUILD_TYPE),)
	CMAKE_ARGS += -DCMAKE_BUILD_TYPE:STRING=$(CMAKE_BUILD_TYPE)
endif
ifneq ($(CMAKE_PREFIX_PATH),)
	CMAKE_ARGS += -DCMAKE_PREFIX_PATH:PATH=$(CMAKE_PREFIX_PATH)
endif
ifneq ($(CMAKE_INSTALL_PREFIX),)
	CMAKE_ARGS += -DCMAKE_INSTALL_PREFIX:PATH=$(CMAKE_INSTALL_PREFIX)
endif
ifneq ($(CPACK_OUTPUT_FILE_PREFIX),)
	CMAKE_ARGS += -DCPACK_OUTPUT_FILE_PREFIX:PATH=$(CPACK_OUTPUT_FILE_PREFIX)
endif
ifneq ($(CMAKE_C_COMPILER),)
	CMAKE_ARGS += -DCMAKE_C_COMPILER:FILEPATH=$(CMAKE_C_COMPILER)
endif
ifneq ($(CMAKE_CXX_COMPILER),)
	CMAKE_ARGS += -DCMAKE_CXX_COMPILER:FILEPATH=$(CMAKE_CXX_COMPILER)
endif
ifneq ($(CMAKE_CXX_FLAGS),)
	CMAKE_ARGS += -DCMAKE_CXX_FLAGS:STRING=$(CMAKE_CXX_FLAGS)
endif
ifneq ($(CMAKE_CXX_FLAGS_DEBUG),)
	CMAKE_ARGS += -DCMAKE_CXX_FLAGS_DEBUG:STRING=$(CMAKE_CXX_FLAGS_DEBUG)
endif
ifneq ($(SIMPT_FORCE_NO_HDF5),)
	CMAKE_ARGS += -DSIMPT_FORCE_NO_HDF5:BOOL=${SIMPT_FORCE_NO_HDF5}
endif
ifneq ($(SIMPT_FORCE_NO_BOOST_GZIP),)
	CMAKE_ARGS += -DSIMPT_FORCE_NO_BOOST_GZIP:BOOL=${SIMPT_FORCE_NO_BOOST_GZIP}
endif
ifneq ($(SIMPT_FORCE_NO_ZLIB),)
	CMAKE_ARGS += -DSIMPT_FORCE_NO_ZLIB:BOOL=${SIMPT_FORCE_NO_ZLIB}
endif
ifneq ($(SIMPT_MAKE_DOC),)
	CMAKE_ARGS += -DSIMPT_MAKE_DOC:BOOL=$(SIMPT_MAKE_DOC)
endif
ifneq ($(SIMPT_MAKE_JAVA_WRAPPER),)
	CMAKE_ARGS += -DSIMPT_MAKE_JAVA_WRAPPER:BOOL=$(SIMPT_MAKE_JAVA_WRAPPER)
endif
ifneq ($(SIMPT_MAKE_PYTHON_WRAPPER),)
	CMAKE_ARGS += -DSIMPT_MAKE_PYTHON_WRAPPER:BOOL=$(SIMPT_MAKE_PYTHON_WRAPPER)
endif
ifneq ($(SIMPT_VERBOSE_TESTING),)
	CMAKE_ARGS += -DSIMPT_VERBOSE_TESTING:BOOL=$(SIMPT_VERBOSE_TESTING)
endif
ifeq ($(BUILD_DIR),)
	BUILD_DIR = ./build
endif

#============================================================================
#   Targets
#============================================================================
.PHONY: help configure all install install_main install_test package   
.PHONY: test installcheck clean distclean 

help:
	@ $(CMAKE) -E echo " "
	@ $(CMAKE) -E echo " Targets are:"
	@ $(CMAKE) -E echo "    configure"
	@ $(CMAKE) -E echo "    all"
	@ $(CMAKE) -E echo "    install"
	@ $(CMAKE) -E echo "    test"
	@ $(CMAKE) -E echo "    clean"
	@ $(CMAKE) -E echo " Read INSTALL.txt in this directory for a brief overview."
	@ $(CMAKE) -E echo "    "
	@ $(CMAKE) -E echo " Current macro values are (CMake will use an appropriate"
	@ $(CMAKE) -E echo " default for any macro that has not been set):"
	@ $(CMAKE) -E echo "   BUILD_DIR                 : " $(BUILD_DIR)
	@ $(CMAKE) -E echo "   CMAKE_GENERATOR           : " $(CMAKE_GENERATOR)
	@ $(CMAKE) -E echo "   CMAKE_C_COMPILER          : " $(CMAKE_C_COMPILER)
	@ $(CMAKE) -E echo "   CMAKE_CXX_COMPILER        : " $(CMAKE_CXX_COMPILER)
	@ $(CMAKE) -E echo "   CMAKE_CXX_FLAGS           : " $(CMAKE_CXX_FLAGS)
	@ $(CMAKE) -E echo "   CMAKE_BUILD_TYPE          : " $(CMAKE_BUILD_TYPE)
	@ $(CMAKE) -E echo "   CMAKE_PREFIX_PATH         : " $(CMAKE_PREFIX_PATH)
	@ $(CMAKE) -E echo "   CMAKE_INSTALL_PREFIX      : " $(CMAKE_INSTALL_PREFIX)
	@ $(CMAKE) -E echo "   CPACK_OUTPUT_FILE_PREFIX  : " $(CPACK_OUTPUT_FILE_PREFIX)
	@ $(CMAKE) -E echo "   SIMPT_MAKE_DOC            : " $(SIMPT_MAKE_DOC)
	@ $(CMAKE) -E echo "   SIMPT_MAKE_JAVA_WRAPPER   : " $(SIMPT_MAKE_JAVA_WRAPPER)
	@ $(CMAKE) -E echo "   SIMPT_MAKE_PYTHON_WRAPPER : " $(SIMPT_MAKE_PYTHON_WRAPPER)
	@ $(CMAKE) -E echo "   SIMPT_FORCE_NO_HDF5       : " $(SIMPT_FORCE_NO_HDF5)
	@ $(CMAKE) -E echo "   SIMPT_FORCE_NO_OPENMP     : " $(SIMPT_FORCE_NO_OPENMP)
	@ $(CMAKE) -E echo "   SIMPT_FORCE_NO_BOOST_GZIP : " $(SIMPT_FORCE_NO_BOOST_GZIP)
	@ $(CMAKE) -E echo "   SIMPT_FORCE_NO_ZLIB       : " $(SIMPT_FORCE_NO_ZLIB)
	@ $(CMAKE) -E echo "   SIMPT_VERBOSE_TESTING     : " $(SIMPT_VERBOSE_TESTING)
	@ $(CMAKE) -E echo " "
				
configure:
	$(CMAKE) -E make_directory $(BUILD_DIR)
	$(CMAKE) -E chdir $(BUILD_DIR) $(CMAKE) $(CMAKE_ARGS) ../src

all: configure
	$(MAKE) -j $(SIMPT_PARALLEL_MAKE) -C $(BUILD_DIR) all

install: configure
	$(MAKE) -j $(SIMPT_PARALLEL_MAKE) -C $(BUILD_DIR) --no-print-directory install   
	
install_main: configure
	$(MAKE) -j $(SIMPT_PARALLEL_MAKE) -C $(BUILD_DIR)/main --no-print-directory install

install_test: install_main
	$(MAKE) -j $(SIMPT_PARALLEL_MAKE) -C $(BUILD_DIR)/test --no-print-directory install

distclean clean:
	$(CMAKE) -E remove_directory $(BUILD_DIR)

test installcheck: install_test
	cd $(BUILD_DIR) && $(CTEST) -V -R 'start_basic|py|java|unit'
	
#############################################################################
