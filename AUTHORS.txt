#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#    Authors and contributors (in alphabetical order):
#    
#############################################################################
At UA, Antwerpen (VirtualLeaf 2):
Jan Broeckhove           <Jan.Broeckhove@ua.ac.be>
Dirk De Vos              <Dirk.DeVos@ua.ac.be>
Abdiravuf Dzhurakhalov   <Abdiravuf.Dzhurakhalov@ua.ac.be>
Joeri Exelmans           <Joeri.Exelmans@student.ua.ac.be>
Przemyslaw Klosiewicz    <Przemyslaw.Klosiewicz@ua.ac.be>
Sean Stijven             <Sean.Stijven@ua.ac.be>
Elise Kuylen             <Elise.Kuyle@student.ua.ac.be>

Coding with(out) Discipline (Bachelor thesis):
Tom De Schepper          <Tom.DeSchepper@student.ua.ac.be>
Yannick Jadoul           <Yannick.Jadoul@student.ua.ac.be>
Stephen Pauwels          <Stephen.Pauwels@student.ua.ac.be>
Tom Van hamme            <Tom.Vanhamme@student.ua.ac.be>
Timothy Verstraeten      <Timothy.Verstraeten@student.ua.ac.be

#############################################################################
